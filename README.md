# README #

Within this repository you will find:

1. The dataset used during the project (Drug_Dataset_v2.xml.gz); which is an updated version of the *in silico* dataset for drug discovery which can be found at [http://www.ondex.org/ ](http://www.ondex.org/ ).
2. The implemented algorithm as described in "Semantic Subgraph Searching in Integrated Biological Graphs: An approach for mining Drug Repositioning Networks".

### What is this repository for? ###

* The repository shows an implementation for an algorithm that allows for the searching and extraction of semantic subgraphs from a larger target graph.

### How do I get set up? ###

* Clone into the repository
* Ensure your local machine has Java version 1.7 and Maven

Build the project using:
```
#!terminal

mvn clean install
```

Validate the results shown in the accompanying paper using:
```
#!terminal

mvn exec:java -Dexec.mainClass=uk.ac.ncl.semsubgraphalgorithm.testing.DrugBankWorkPaper
```


### Contribution guidelines ###

* Code is written in Java
* Downloaded as a Maven project

### Who do I talk to? ###

* Any problems or questions feel free to contact the repository owner at j.mullen[at]ncl.ac.uk
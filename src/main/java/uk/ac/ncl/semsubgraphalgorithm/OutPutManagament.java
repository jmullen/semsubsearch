/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

/**
 *
 * @author joemullen
 */
public class OutPutManagament {

  
    /**
     * Empties the temp folder, this should be done before and after every usage 
     * of the temp file store
     */
    public void emptyTempFolder() {
        String dir = "Results\\Temp";
        String sys = System.getProperty("os.name");
        if (sys.startsWith("Mac") || sys.startsWith("Ubun")) {
            dir = "Results/Temp";
        }
        File folder = new File(sys);
        if (folder.isDirectory()) {
            for (File child : folder.listFiles()) {
                child.delete();
            }
        }
    }
    
      public void emptySearchesFolder() {
        String dir = "Results\\Searches";
        String sys = System.getProperty("os.name");
        if (sys.startsWith("Mac") || sys.startsWith("Ubun")) {
            dir = "Results/Searches";
        }
        File folder = new File(sys);
        if (folder.isDirectory()) {
            for (File child : folder.listFiles()) {
                child.delete();
            }
        }
    }
    
  
    
     /**
     * Empties the searches folder, this should be done before and after every usage 
     * of the search file store
     */
    public void emptySearchFolder() {
        String dir = "Results\\Searches";
        String sys = System.getProperty("os.name");
        if (sys.startsWith("Mac") || sys.startsWith("Ubun")) {
            dir = "Results/Searches";
        }
        File folder = new File(sys);
        if (folder.isDirectory()) {
            for (File child : folder.listFiles()) {
                child.delete();
            }
        }
    }


    /**
     * Returns the string name of the results directory depending on the OS
     * @return 
     */
    public String getResultsName() {

        String dir = "Results\\";
        String sys = System.getProperty("os.name");
        if (sys.startsWith("Mac") || sys.startsWith("Ubun")) {
            dir = "Results/";
        }

        return dir;

    }
    
    /**
     * Returns the string name of the results directory depending on the OS
     * @return 
     */
    public String getScalabilityName() {

        String dir = "Results\\Scalability\\";
        String sys = System.getProperty("os.name");
        if (sys.startsWith("Mac") || sys.startsWith("Ubun")) {
            dir = "Results/Scalabilty/";
        }

        return dir;

    }
    
    /**
     * Returns the string name of the temp directory depending on the OS
     * @return 
     */
    public String getTempDirectoryName() {

        String dir = "Results\\Temp";
        String sys = System.getProperty("os.name");
        if (sys.startsWith("Mac") || sys.startsWith("Ubun")) {
            dir = "Results/Temp";
        }

        return dir;

    }
    
    /**
     * Returns the path to the DrugBank work directory
     * @return 
     */
      public String getDrugBankDirectoryPath() {

        String dir = "Results\\DB3WORK\\";
        String sys = System.getProperty("os.name");
        if (sys.startsWith("Mac") || sys.startsWith("Ubun")) {
            dir = "Results/DB3WORK/";
        }

        return dir;

    }
      
      
      /**
       * Returns the path to the searches directory
       * @return 
       */
       public String getSearhchesDirectoryPath() {

        String dir = "Results\\Searches\\";
        String sys = System.getProperty("os.name");
        if (sys.startsWith("Mac") || sys.startsWith("Ubun")) {
            dir = "Results/Searches/";
        }

        return dir;

    }
       
       /**
       * Returns the path to the unique pairs directory
       * @return 
       */
       public String getuniquePairsDirectoryPath() {

        String dir = "Results\\DB3WORK\\UniquePairs\\";
        String sys = System.getProperty("os.name");
        if (sys.startsWith("Mac") || sys.startsWith("Ubun")) {
            dir = "Results/Db3WORK/UniquePairs/";
        }

        return dir;

    }
       
       /**
       * Returns the path to all the pairs directory
       * @return 
       */
       public String getALLPairsDirectoryPath() {

        String dir = "Results\\DB3WORK\\AllPairs\\";
        String sys = System.getProperty("os.name");
        if (sys.startsWith("Mac") || sys.startsWith("Ubun")) {
            dir = "Results/Db3WORK/AllPairs/";
        }

        return dir;

    }
       
              /**
       * Returns the path to the visualization directory
       * @return 
       */
       public String getVisualisationDirectoryPath() {

        String dir = "Results\\Visualisation\\";
        String sys = System.getProperty("os.name");
        if (sys.startsWith("Mac") || sys.startsWith("Ubun")) {
            dir = "Results/Visualisation/";
        }

        return dir;

    }
       
       
}

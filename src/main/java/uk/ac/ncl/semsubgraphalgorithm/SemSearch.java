/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm;

/**
 * @author joemullen
 */
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jgrapht.DirectedGraph;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import uk.ac.ncl.semsubgraphalgorithm.semanticDistanceSubgraphExclusion.SemanticDistanceCalculator;
import uk.ac.ncl.semsubgraphalgorithm.semanticGraphPruning.GraphPruning;

public class SemSearch extends SourceGraph {

    public static void main(String[] args) throws IOException, Exception {
        SerialisedGraph ser = new SerialisedGraph();
        SourceGraph source = ser.useSerialized("graph.data");
        QueryGraph q3 = new QueryGraph(QueryGraph.SemanticSub.CTCT);
        GraphPruning gp4 = new GraphPruning(source, q3, 0.8);
        SemSearch sem = new SemSearch(gp4.getPrunedSource(), q3, null, null, 8, "Compound", true, 0.8, true, true, true);
        sem.completeSearch();


    }

    public String getSummary() {
        return summary;
    }

    /**
     * Class performs a search
     *
     * @param s
     * @param q
     * @param original
     * @param initialNodes
     * @param semDepth
     * @param startClass
     * @param SDC
     * @param threshold
     * @param closedWorld
     * @param outPut
     * @param noElementsBelowST
     * @throws IOException
     */
    public SemSearch(SourceGraph s, QueryGraph q, QueryGraph original, ArrayList<String> initialNodes, int semDepth, String startClass, boolean SDC, double threshold, boolean closedWorld, boolean outPut, boolean noElementsBelowST) throws IOException {


        this.dupecount = 0;
        this.duplication = new HashSet<String>();
        this.closedWorld = closedWorld;
        this.threshold = threshold;
        this.useSDC = SDC;
        this.writeToFile = outPut;
        this.semDepth = semDepth;
        this.source = s;
        this.query = q;
        this.inferredFrom = q.getInferredFROM();
        this.inferredTo = q.getInferredTO();
        this.inferredTYPE = q.getInferredTYPE();
        this.motif = q.getSub();
        this.targ = s.getSourceGraph();
        this.subMap = q.nameEdgeset(motif);
        this.targeyMap = s.nameEdgeset(targ);
        this.subGraphSize = getGraphSize(motif);
        this.tarGraphSize = getGraphSize(targ);
        this.startClass = startClass;
        this.succesfulInitialNodes = new HashSet<String>();
        this.qname = q.getMotifName();
        this.noElementsBelowST = noElementsBelowST;

        //if original = null we are running a non-split search
        if (original != null) {
            this.originalQuery = original;
            this.originalSub = original.getSub();
            this.origisubMap = original.nameEdgeset(originalSub);
            this.original_concept_classes = original.getQueryNodeInfo();
            this.allQuerySubMatches = new HashMap<String, Set<DirectedGraph<String, DefaultEdge>>>();

            if (initialNodes != null) {
                this.initialNodes = initialNodes;

            } else {
                //our initial nodes are those that match the highest connected node in the original subgraph
                this.initialNodes = startingNodes(mostConnectedSem(origisubMap, "any", original.getQueryNodeInfo()), origisubMap, targeyMap);
            }


        } //else we are running a normal search
        else {

            this.mostConnectedSemNode = mostConnectedSem(subMap, startClass, q.getQueryNodeInfo());
            this.mostConnectedTopNode = mostConnectedTop(subMap);

            if (threshold == 0.0) {//just do it topologically
                this.initialNodes = s.startingNodes(mostConnectedTopNode,
                        subMap, targeyMap);
            } else { //use the starting CC
                this.initialNodes = s.firstSemanticMatch(s.startingNodes(mostConnectedSemNode,
                        subMap, targeyMap), getStartingCC());
            }

        }

        this.sub_core = new String[subGraphSize];
        this.sub_order = new String[subGraphSize];
        this.tar_core = new String[subGraphSize];
        this.sub_out = new String[subGraphSize];
        this.sub_in = new String[subGraphSize];
        this.tar_out = new String[tarGraphSize];
        this.tar_in = new String[tarGraphSize];
        this.potentialNodesPairsP = new String[tarGraphSize];
        this.allMatches = new ArrayList<String>();
        this.conceptInfo = s.getAllCOnceptInfo();
        this.sub_concept_classes = q.getQueryNodeInfo();
        this.op = new OutPutManagament();

        if (useSDC == true) {
            this.sdc = new SemanticDistanceCalculator();
            this.allCCsINSDC = sdc.getAllConceptClasses();

        }

        if (outPut == true) {
            this.fstream = new FileWriter(new File(op.getSearhchesDirectoryPath() + "[" + qname
                    + "]_ST" + threshold + "_" + System.currentTimeMillis() / 100 + ".txt"));
            this.out = new BufferedWriter(fstream);
        }


    }
    Map<String, Set<DirectedGraph<String, DefaultEdge>>> allQuerySubMatches;
    private Map<String, String> original_concept_classes;
    private HashMap<String, int[]> origisubMap;
    private DirectedGraph<String, DefaultEdge> originalSub;
    private QueryGraph originalQuery;
    private String inferredFrom;
    private String inferredTo;
    private String inferredTYPE;
    private String qname;
    private Set<String> allCCsINSDC;
    private double threshold;
    private SemanticDistanceCalculator sdc;
    private boolean useSDC;
    private boolean writeToFile;
    public String summary = "";
    private int semDepth;
    private SourceGraph source;
    private QueryGraph query;
    int tempCount = 0;
    private String mostConnectedTopNode;
    private String mostConnectedSemNode;
    private DirectedGraph<String, DefaultEdge> targ;
    private DirectedGraph<String, DefaultEdge> motif;
    private HashMap<String, int[]> subMap;
    private HashMap<String, int[]> targeyMap;
    private int subGraphSize;
    private int tarGraphSize;
    private String[] sub_core;
    private String[] tar_core;
    private String[] UID_core;
    private String[] sub_order;
    private Map<String, String> sub_concept_classes;
    private String[] temp_new_additions;
    private int subOrderCount = 0;
    private int recursionCount = 0;
    private String[] sub_out;
    private String[] sub_in;
    private String[] tar_out;
    private String[] tar_in;
    private String[] nextResukst;
    private int UID = 0;
    private int STATE = 0;
    private int allMatchesExclSmallWorldAss = 0;
    private String[] potentialNodesPairsP;
    private ArrayList<String> allMatches;
    private ArrayList<String> initialNodes;
    private Set<String> succesfulInitialNodes;
    private DirectedGraph<String, DefaultEdge> subTree = new DefaultDirectedGraph<String, DefaultEdge>(
            DefaultEdge.class);
    private DirectedGraph<String, DefaultEdge> UIDTree = new DefaultDirectedGraph<String, DefaultEdge>(
            DefaultEdge.class);
    private String rootNodeSub = "";
    private String rootNodeUID = "";
    private String[][] conceptInfo;
    private boolean closedWorld;
    private boolean noElementsBelowST;
    FileWriter fstream;
    BufferedWriter out;
    long startTime2;
    long endTime2;
    private String startClass;
    private Set<String> duplication;
    private int dupecount;
    private OutPutManagament op;

    /**
     * Performs a semantic search using parameters passed to the constructor
     *
     * @throws IOException
     */
    public void completeSearch() throws IOException {

        startTime2 = System.currentTimeMillis();
        try {
            recursiveVF2(initialNodes);
        } catch (Exception ex) {
            Logger.getLogger(SemSearch.class.getName()).log(Level.SEVERE, null, ex);
        }
        endTime2 = System.currentTimeMillis();


        summary = "-----------------------------------------------------" + "\n" + "[SemSearch Took " + getTime() + " seconds]" + "\n" + "[Found: " + allMatches.size() + " matches with ST: " + threshold + "]" + "\n" + "[Matches Failing Closed World: " + (allMatchesExclSmallWorldAss - allMatches.size()) + "]" + "\n" + "[Initial Cand Size: " + initialNodes.size() + "]" + "\n" + "[Subgraph size= " + subGraphSize + "/" + query.getSub().edgeSet().size() + "]" + "\n" + "[TarGraphSize= " + tarGraphSize + "/" + targ.edgeSet().size() + "]" + "\n" + "[Sub_ORDER:" + Arrays.toString(sub_order) + "]";

        System.out.println(summary);

        if (writeToFile == true) {
            out.close();
        }
        //System.out.println("Duplication: " + dupecount);

    }

    /**
     * Method performs a search for each initial pair identified
     *
     * @param initialPairs
     * @throws Exception
     */
    public void recursiveVF2(ArrayList<String> initialPairs) throws Exception {

        prepareDataStructures();

        for (String pairs : initialPairs) {
            long checktime = System.currentTimeMillis();
            recursionCount = 0;
            subTree = new DefaultDirectedGraph<String, DefaultEdge>(
                    DefaultEdge.class);
            UIDTree = new DefaultDirectedGraph<String, DefaultEdge>(
                    DefaultEdge.class);

            resetDataStructures();

            STATE = 0;
            String[] split = pairs.split(",,,");
            String sub = split[0];
            String tar = split[1];
            rootNodeSub = sub.trim();
            rootNodeUID = tar.trim();
            subTree.addVertex(rootNodeSub);
            UIDTree.addVertex(rootNodeUID);

            String[] resultsSTATE1 = getNextNodes(sub, tar);

            recursionCount++;
            nextResukst = getNextNodes(resultsSTATE1);

        }

    }

    /**
     * Method returns all possible nodes that may be included in a match in the
     * next state of the search, if the search has reached the final state it
     * returns all matches
     *
     * @param subNode
     * @param tarNode
     * @return
     * @throws Exception
     */
    public String[] getNextNodes(String subNode, String tarNode) throws Exception {

        resetDataStructures();
        String[][] info = popUIDCore(rootNodeUID, tarNode);
        sub_core = popSubCore(rootNodeSub, subNode);
        UID_core = info[0];
        tar_core = info[1];

        STATE = getState();
        String allNodesAdded = "";
        populateArrays(sub_core, tar_core);
        calculateP();

        for (int x = 0; x < potentialNodesPairsP.length; x++) {

            String pairs = potentialNodesPairsP[x];
            if (pairs.equals("NULL_NODE")) {
                break;
            } else {
                String[] split = pairs.split(",,,");
                String sub = split[0].trim();
                String tart = split[1].trim();
                String[] target = removeUID(tart);
                String tar = target[0].trim();

                int count = 0;
                double check = 0.0;
                String[] tempmatch = new String[subGraphSize];


                boolean addedLatest = false;
                if (subOrderCount > 0) {

                    for (int f = 0; f < tempmatch.length; f++) {
                        if (tar_core[f].equals("NULL_NODE") && addedLatest == false) {
                            tempmatch[f] = tar;
                            addedLatest = true;
                        } else {
                            tempmatch[f] = tar_core[f];
                        }
                    }

                }



                if ((feas2(sub, tar) == true)
                        && (repeatabiltyCHECK(tar, tar_core, getState()) == 0 && repeatabiltyCHECK(
                        sub, sub_core, getState()) == 0)
                        && (feas1(sub, tar) == true)
                        && (closedWorldCHECK(tar_core, sub_order, targ, motif, closedWorld, subOrderCount) == true)
                        && (scoreMatchBooleanCHECK(query,
                        source,
                        sub_order,
                        tar_core,
                        sub_concept_classes,
                        sdc,
                        allCCsINSDC,
                        useSDC,
                        noElementsBelowST,
                        threshold,
                        subOrderCount) == true)) {

                    allNodesAdded = allNodesAdded + pairs + "\t";
                    UIDTree.addVertex(tart);

                    if (count == 0) {
                        subTree.addVertex(sub.trim());
                    }

                    STATE = getState();

                    UIDTree.addEdge(UID_core[STATE - 1].trim(), tart);
                    if (count == 0) {
                        subTree.addEdge(sub_core[STATE - 1].trim(), sub.trim());

                    }

                    if (STATE == subGraphSize - 1) {
                        String[] match1 = new String[subGraphSize];
                        String match = "";

                        for (int f = 0; f < match1.length; f++) {
                            if (f < subGraphSize - 1) {
                                match1[f] = tar_core[f];
                                match = match + tar_core[f] + "\t";
                                if (subOrderCount == 0) {
                                    sub_order[f] = sub_core[f];
                                }

                            } else {
                                match1[f] = tar;
                                match = match + tar;
                                if (subOrderCount == 0) {
                                    sub_order[f] = sub;

                                }
                            }

                        }


                        subOrderCount++;
                        String matchToString = orderMatch(match1);

                        if (closedWorldCHECK(match1, sub_order, targ, motif, closedWorld, subOrderCount) == true && !duplication.contains(matchToString) && (scoreMatchBooleanCHECK(query,
                                source,
                                sub_order,
                                match1,
                                sub_concept_classes,
                                sdc,
                                allCCsINSDC,
                                useSDC,
                                noElementsBelowST,
                                threshold,
                                subOrderCount) == true)) {
                            if (allMatches.contains(match)) {
                                tempCount++;
                            } else if (duplication.contains(matchToString)) {
                                dupecount++;
                            } else {
                                duplication.add(matchToString);
                                allMatches.add(match);


                                allMatchesExclSmallWorldAss++;
                                succesfulInitialNodes.add(sub_order[0] + ",,," + match1[0]);



                                try {
                                    if (writeToFile == true) {
                                        addtoFile(match1);
                                        if (allMatches.size() % 10000 == 0) {
                                        }

                                    }
                                } catch (IOException ex) {
                                    Logger.getLogger(SemSearch.class.getName()).log(Level.SEVERE, null, ex);
                                }

                            }

                        }

                    } else {
                        allMatchesExclSmallWorldAss++;
                    }
                    count++;

                }
            }
        }

        String[] allNodesAddedSplit = allNodesAdded.split("\t");
        if (!allNodesAddedSplit[0].equals("")) {
        } else {
            String[] temp = new String[]{"Nothing"};
            allNodesAddedSplit = temp;
        }

        return allNodesAddedSplit;

    }

    /**
     * Returns the next nodes to be considered during the search
     *
     * @param otherNodes
     * @return
     * @throws Exception
     */
    public String[] getNextNodes(String[] otherNodes) throws Exception {

        String newAdditions = "";

        for (int u = 0; u < otherNodes.length; u++) {

            String local = otherNodes[u];

            String[] split = local.split(",,,");
            String sub = split[0].trim();
            if (sub.equals("Nothing") || sub.isEmpty()) {
                break;
            } else {
                String tar = split[1].trim();
                String[] local2 = getNextNodes(sub, tar);
                for (int t = 0; t < local2.length; t++) {
                    newAdditions = newAdditions + local2[t].trim() + "\t";
                }
            }
        }

        String[] newAdditions2 = newAdditions.split("\t");
        temp_new_additions = newAdditions2;
        recursionCount++;

        while (recursionCount < subGraphSize - 1
                && temp_new_additions.length > 0
                && !temp_new_additions[0].equals("Nothing")) {

            getNextNodes(temp_new_additions);
        }

        return newAdditions2;

    }

    /**
     * Adds a UID to a node name
     *
     * @param node
     * @return
     */
    public String addUID(String node) {
        String unique = node + "===" + UID;
        UID++;
        return unique;
    }

    /**
     * Removes a UID from a node and returns only the node name
     *
     * @param node
     * @return
     */
    public String[] removeUID(String node) {
        String[] split1 = node.split("===");
        String[] split = new String[2];
        for (int p = 0; p < split1.length; p++) {
            split[p] = split1[p].trim();
        }
        return split;

    }

    /**
     * Returns the temp (match) core
     *
     * @param nodeInQuestion
     * @param targetNode
     * @return
     */
    public String[][] popUIDCore(String nodeInQuestion, String targetNode) {
        DijkstraShortestPath tr2 = new DijkstraShortestPath(UIDTree,
                nodeInQuestion, targetNode);

        Object[] why = tr2.getPathEdgeList().toArray();
        String[][] output = new String[2][subGraphSize];
        String[] tempTar = new String[subGraphSize];
        if (why.length > 0) {
            String nodes = "";
            for (int g = 0; g < why.length; g++) {

                String relation1 = why[g].toString();
                String relation2 = relation1.trim();
                String relation = relation2
                        .substring(1, relation2.length() - 1);

                String[] split = relation.split(":");

                for (int y = 0; y < split.length; y++) {
                    String node = split[y].trim();
                    if (g == 0) {
                        nodes = nodes + node + "\t";
                    } else if (y == 0) {
                        nodes = nodes + split[1].trim() + "\t";
                    }
                }

            }

            String[] core = nodes.split("\t");
            for (int h = 0; h < core.length; h++) {
                if (!core[h].equals("")) {
                    tempTar[h] = core[h];
                }
            }
            for (int f = 0; f < tempTar.length; f++) {
                if (tempTar[f] == null) {
                    tempTar[f] = "NULL_NODE";
                }
            }
        } else {
            for (int f = 0; f < tempTar.length; f++) {
                if (f == 0) {
                    tempTar[f] = nodeInQuestion;
                } else {
                    tempTar[f] = "NULL_NODE";
                }

            }
        }

        output[0] = tempTar;
        for (int y = 0; y < tempTar.length; y++) {
            if (output[0][y].equals("NULL_NODE")) {
                output[1][y] = output[0][y];
            } else {
                String[] local = removeUID(output[0][y]);
                output[1][y] = local[0];

            }
        }
        return output;
    }

    /**
     * Populates the sub graph core
     *
     * @param nodeInQuestion
     * @param targetNode
     * @return
     */
    public String[] popSubCore(String nodeInQuestion, String targetNode) {

        DijkstraShortestPath tr2 = new DijkstraShortestPath(subTree,
                nodeInQuestion, targetNode);

        Object[] why = tr2.getPathEdgeList().toArray();

        String[] tempTar = new String[subGraphSize];
        if (why.length > 0) {
            String nodes = "";
            for (int g = 0; g < why.length; g++) {

                String relation1 = why[g].toString();
                String relation2 = relation1.trim();
                String relation = relation2
                        .substring(1, relation2.length() - 1);

                String[] split = relation.split(":");
                for (int y = 0; y < split.length; y++) {
                    String node = split[y].trim();
                    if (g == 0) {
                        nodes = nodes + node + "\t";
                    } else if (y == 0) {
                        nodes = nodes + split[1].trim() + "\t";
                    }
                }

            }

            String[] core = nodes.split("\t");
            for (int h = 0; h < core.length; h++) {
                if (!core[h].equals("")) {
                    tempTar[h] = core[h];
                }
            }
            for (int f = 0; f < tempTar.length; f++) {
                if (tempTar[f] == null) {
                    tempTar[f] = "NULL_NODE";
                }
            }
        } else {
            for (int f = 0; f < tempTar.length; f++) {
                if (f == 0) {
                    tempTar[f] = nodeInQuestion;
                } else {
                    tempTar[f] = "NULL_NODE";
                }

            }
        }

        return tempTar;
    }

    /**
     * Checks to see if a node passes the first feasibility rule
     *
     * @param subNode
     * @param tarNode
     * @return
     */
    public boolean feas1(String subNode, String tarNode) {

        boolean well = false;

        if (STATE == 0) {
            return true;
        } else {
            if ((rPred(subNode, tarNode) == true)
                    && (rSucc(subNode, tarNode) == true)) {
                return true;

            } else {

                return false;
            }
        }

    }

    /**
     * Check if the pairs match in the same way to the previous nodes of the
     * partial mapping
     *
     * @param sub
     * @param tar
     * @return
     */
    public boolean rPred(String sub, String tar) {

        boolean well = true;
        String[] inComingNodes = tIn(motif, sub);

        if (inComingNodes.length > 0) {

            ArrayList<Integer> location_in_mapping = new ArrayList<Integer>();

            for (String node : inComingNodes) {
                //tits deleted -1
                for (int i = 0; i < sub_core.length; i++) {
                    if (sub_core[i].equals(node)) {
                        location_in_mapping.add(i);

                        //tits below commented
//                    } else {
//                        // we have none to look at
//                        well = true;
                    }
                }
            }
            for (int look : location_in_mapping) {
                if (targ.containsEdge(tar_core[look].trim(), tar.trim()) == false) {
                    //tits below commented (also changed the abve boolean from true) 
//                    well = true;
//                } else {
                    well = false;
                }
            }
        }

        return well;

    }

    /**
     * Check if the pairs match in the same way to the next nodes of the partial
     * mapping
     *
     * @param sub
     * @param tar
     * @return
     */
    public boolean rSucc(String sub, String tar) {
        boolean well = true;
        String[] outGoingNodes = tOut(motif, sub);

        if (outGoingNodes.length > 0) {
            ArrayList<Integer> location_in_mapping = new ArrayList<Integer>();
            for (String node : outGoingNodes) {

                for (int i = 0; i < sub_core.length - 1; i++) {
                    if (sub_core[i].equals(node)) {
                        location_in_mapping.add(i);
                    }

                }
            }
            for (int look : location_in_mapping) {
                if (!targ.containsEdge(tar.trim(), tar_core[look].trim()) == true) {

                    return false;
                }

            }
        }

        return well;
    }

    /**
     * Checks to see if a potential addition passes feasibility rule 2
     *
     * @param subNode
     * @param tarNode
     * @return
     */
    public boolean feas2(String subNode, String tarNode) {
        if (STATE == 0) {
            return true;
        } else {
            int[] subEdgeset = subMap.get(subNode);
            int[] tarEdgeset = targeyMap.get(tarNode);
            return (rTermin(subEdgeset, tarEdgeset));
        }
    }

    /**
     * Check the node in the mapping has the same edgeset as its equivalent in
     * the subgraph
     *
     * @param sub
     * @param tar
     * @return
     */
    public boolean rTermin(int[] sub, int[] tar) {
        boolean well = true;
        for (int p = 0; p < sub.length; p++) {
            if (tar[p] < sub[p]) {
                return false;
            }
        }
        return well;
    }

    /**
     * Resets the data structures prior to a new search or a new state
     */
    public void resetDataStructures() {
        for (int i = 0; i < subGraphSize; i++) {
            if (sub_core[i].equals("NULL_NODE")) {
                break;

            } else {
                sub_core[i] = "NULL_NODE";
            }
        }
        for (int i = 0; i < subGraphSize; i++) {
            if (tar_core[i].equals("NULL_NODE")) {
                break;

            } else {

                tar_core[i] = "NULL_NODE";
            }
        }
        for (int i = 0; i < subGraphSize; i++) {
            if (sub_out[i].equals("NULL_NODE")) {
                break;

            } else {
                sub_out[i] = "NULL_NODE";
            }
        }
        for (int i = 0; i < subGraphSize; i++) {
            if (sub_in[i].equals("NULL_NODE")) {
                break;

            } else {
                sub_in[i] = "NULL_NODE";
            }
        }
        for (int i = 0; i < tarGraphSize; i++) {
            if (tar_out[i].equals("NULL_NODE")) {
                break;

            } else {
                tar_out[i] = "NULL_NODE";
            }
        }
        for (int i = 0; i < tarGraphSize; i++) {
            if (tar_in[i].equals("NULL_NODE")) {
                break;

            } else {
                tar_in[i] = "NULL_NODE";
            }
        }
        for (int i = 0; i < tarGraphSize; i++) {
            if (potentialNodesPairsP[i].equals("NULL_NODE")) {
                break;

            } else {
                potentialNodesPairsP[i] = "NULL_NODE";
            }
        }
    }

    /**
     * Populates the data structures
     */
    public void prepareDataStructures() {
        for (int i = 0; i < subGraphSize; i++) {
            sub_core[i] = "NULL_NODE";
            tar_core[i] = "NULL_NODE";
            sub_out[i] = "NULL_NODE";
            sub_in[i] = "NULL_NODE";
        }

        for (int i = 0; i < tarGraphSize; i++) {
            tar_out[i] = "NULL_NODE";
            tar_in[i] = "NULL_NODE";
            potentialNodesPairsP[i] = "NULL_NODE";

        }

    }

    /**
     * Populates the arrays that contain information that is used to see whether
     * or not a node passes the rules and may be considered for the next state
     * of the search
     *
     * @param subnode
     * @param tarnode
     */
    public void populateArrays(String[] subnode, String[] tarnode) {

        String[] tarOut1;
        tarOut1 = tOut(targ, tarnode);

        for (int i = 0; i < tarOut1.length; i++) {

            tar_out[i] = addUID(tarOut1[i]);
        }
        String[] tarIn1;
        tarIn1 = tIn(targ, tarnode);

        for (int i = 0; i < tarIn1.length; i++) {
            tar_in[i] = addUID(tarIn1[i]);
        }

        String[] subOut1;
        subOut1 = tOut(motif, subnode);
        for (int i = 0; i < subOut1.length; i++) {
            sub_out[i] = subOut1[i];
        }

        String[] subIn1;
        subIn1 = tIn(motif, subnode);
        for (int i = 0; i < subIn1.length; i++) {
            sub_in[i] = subIn1[i];
        }
    }

    /**
     * Calculates which set of nodes will be used for the next state of the
     * search
     *
     * @return
     */
    public String calculateP() {
        String mostConnectednode = "";
        int highest = 0;

        if (!tar_out[0].equals("NULL_NODE") && !sub_out[0].equals("NULL_NODE")) {

            for (int y = 0; y < sub_out.length - 1; y++) {
                String local = sub_out[y];
                if (!sub_out[y].equals("NULL_NODE")) {
                    int[] local2 = subMap.get(local);

                    if ((local2[0] >= highest)
                            && (repeatabiltyCHECK(local, sub_core, getState()) == 0)) {
                        highest = local2[0];
                        mostConnectednode = local;

                    }

                }
            }

            for (int i = 0; i < tar_out.length; i++) {
                if (tar_out[i].equals("NULL_NODE")) {

                    break;
                } else if (!mostConnectednode.equals("")) {

                    potentialNodesPairsP[i] = mostConnectednode + ",,,"
                            + tar_out[i];

                }

            }
        }

        if (mostConnectednode.equals("") && !tar_in[0].equals("NULL_NODE")
                && !sub_in[0].equals("NULL_NODE")) {

            String mostConnectednodeIn = "";
            int highestIn = 0;


            for (int y = 0; y < sub_in.length - 1; y++) {
                String local = sub_in[y];
                if (!sub_in[y].equals("NULL_NODE")) {
                    int[] local2 = subMap.get(local);
                    if ((local2[0] >= highestIn)
                            && (repeatabiltyCHECK(local, sub_core, getState()) == 0)) {
                        highestIn = local2[0];
                        mostConnectednodeIn = local;
                    }
                }
            }

            for (int i = 0; i < tar_in.length; i++) {

                if (tar_in[i].equals("NULL_NODE")) {

                    break;
                } else {

                    potentialNodesPairsP[i] = mostConnectednodeIn + ",,,"
                            + tar_in[i];
                    if (tar_in[i].equals("1003")) {
                    }

                }
            }

        }

        return mostConnectednode;

    }

    /**
     * Check to see if the graph we are searching for is a complete match of the
     * target graph
     *
     * @return
     */
    public boolean completeMatch() {
        boolean completeMatch = false;

        if (getGraphSize(targ) == getGraphSize(motif)) {
            completeMatch = true;

        }
        return completeMatch;

    }

    /**
     * Returns the current state of the search for that particular mapping
     *
     * @return
     */
    public int getState() {

        int county = 0;
        for (int y = 0; y < sub_core.length; y++) {
            if (sub_core[y] == "NULL_NODE") {
                county++;
            }

        }
        int myPredictedState = (sub_core.length - county);
        return myPredictedState;
    }

    /**
     * Get the relations of a match ready for scoring
     *
     * @param match
     * @return
     */
    public String[] getMatchRelations(String[] match) {

        String subEdge = motif.edgeSet().toString();
        String[] lomatch = match;

        for (int u = 0; u < sub_order.length; u++) {
            subEdge = subEdge.replaceAll(sub_order[u], lomatch[u]);

        }
        String[] subedge2 = subEdge.substring(1,
                subEdge.length() - 1).split(", ");

        for (int h = 0; h < subedge2.length; h++) {
            subedge2[h] = subedge2[h].trim().substring(1,
                    subedge2[h].length() - 1);

        }
        return subedge2;
    }

    /**
     * Write all of the matches identified to file
     *
     * @param match1
     * @throws IOException
     */
    public void addtoFile(String[] match1)
            throws IOException {

        String[] rel = getMatchRelations(match1);
        String[] relType = source.getMatchRelationsRELTYPE(rel);

        String[] conCl = source.getConceptClass(match1);
        String[] conName = source.getConceptName(match1);


        try {
            out.append(">>> " + (allMatches.size()) + "\n");
            out.append("[SUB_INFO]" + "\n");
            out.append("SS:" + scoreMatchDoubleCHECK(match1, query, sub_order, threshold, source, sdc, useSDC, sub_concept_classes, allCCsINSDC) + "\n");

            String[] localNodes = match1;
            out.append("SN:");
            for (int t = 0; t < localNodes.length; t++) {
                out.append(localNodes[t] + "\t");
            }
            out.append("\n");

            String[] localConcepts = conName;
            out.append("NN:");
            for (int t = 0; t < localConcepts.length; t++) {
                out.append(localConcepts[t] + "\t");
            }
            out.append("\n");

            String[] localClasses = conCl;
            out.append("CC:");
            for (int t = 0; t < localClasses.length; t++) {
                out.append(localClasses[t] + "\t");
            }
            out.append("\n");

            String[] localReltype = rel;
            out.append("SR:");
            for (int t = 0; t < localReltype.length; t++) {
                out.append(localReltype[t] + "\t");
            }
            out.append("\n");

            String[] localRel = relType;
            out.append("RT:");
            for (int t = 0; t < localRel.length; t++) {
                out.append(localRel[t] + "\t");
            }
            out.append("\n");

            out.append("[INFERRED_REL_INFO]" + "\n");
            out.append("IR:" + getInferredRelInfo(match1));
            out.append("\n");


        } catch (Exception e) {
            //System.out.println("Error: " + e.getMessage());
        }

    }

    /**
     * Sorts the nodes of match in ascending order using their id's
     *
     * @param match1
     * @return
     */
    public String orderMatch(String[] match1) {

        ArrayList<Integer> temp = new ArrayList<Integer>();
        for (int y = 0; y < match1.length; y++) {
            temp.add(Integer.parseInt(match1[y]));
        }
        Collections.sort(temp);
        return temp.toString();
    }

    /**
     * Return the set of initial nodes that lead to a match being found during
     * the search
     *
     * @return
     */
    public ArrayList<String> getSuccesfulInitialNodes() {
        ArrayList<String> in = new ArrayList<String>();
        for (String init : succesfulInitialNodes) {
            in.add(init);
        }
        return in;
    }

    /**
     * Returns the semantics for the inferred relation; if one has been
     * specified in the QueryGraph being searched for
     *
     * @param match
     * @return
     */
    public String getInferredRelInfo(String[] match) {

        String from = "";
        String to = "";

        if (inferredFrom.equals("n/a") || inferredTo.equals("n/a")) {
            return "no_info_on_inferred_assigned" + "\n";
        } else {
            for (int i = 0; i < sub_order.length; i++) {
                if (sub_order[i].equals(inferredFrom)) {
                    from = match[i];
                }
                if (sub_order[i].equals(inferredTo)) {
                    to = match[i];
                }
            }
        }
        return from + "\t" + to + "\n" + "IN:" + source.getConceptName(from) + "\t" + source.getConceptName(to) + "\n" + "IC:" + source.getConceptClass(from) + "\t" + source.getConceptClass(to) + "\t" + inferredTYPE + "\n";
    }

    /**
     * Return time taken to complete a search in seconds
     *
     * @return
     */
    public String getTime() {
        String time = (" " + (double) (endTime2 - startTime2) / 1000);
        return time.trim();
    }

    /**
     * Return all the matches that were identified by the search
     *
     * @return
     */
    public ArrayList<String> getAllMatches() {
        return allMatches;

    }

    /**
     * Get the number of matches that were identified by the search
     *
     * @return
     */
    public int numberOfMathces() {

        return allMatches.size();
    }

    /**
     * Set the initial nodeset to be used prior to a search
     *
     * @param nodeset
     */
    public void setInitialNodes(ArrayList<String> nodeset) {
        this.initialNodes = nodeset;

    }

    /**
     * Set the order in which nodes from the subgraph will be searched for
     *
     * @param order
     */
    public void setSubOrder(String[] order) {
        this.sub_order = order;
        this.subOrderCount = 1;
    }

    /**
     * Return the order in which the subgraph nodes were mapped
     *
     * @return
     */
    public String[] getSubOrder() {
        return sub_order;
    }
}

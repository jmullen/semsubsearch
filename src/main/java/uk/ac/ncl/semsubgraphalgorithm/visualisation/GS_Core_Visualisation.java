/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.visualisation;

import java.io.IOException;
import java.util.HashMap;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.graphstream.graph.*;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.stream.file.FileSinkImages;
import org.graphstream.stream.file.FileSinkImages.LayoutPolicy;
import org.graphstream.stream.file.FileSinkImages.OutputType;
import org.graphstream.stream.file.FileSinkImages.RendererType;
import org.graphstream.stream.file.FileSinkImages.Resolutions;
import uk.ac.ncl.semsubgraphalgorithm.OutPutManagament;

/**
 *
 * @author joemullen
 */
public class GS_Core_Visualisation {

    private HashMap<String, Set<String>> nodes;
    private HashMap<String, Set<String>> edges;
    private Graph graph;
    private String name;
    private Set<String> highLight;
    private boolean onlyToFile;
    private OutPutManagament op;

    public GS_Core_Visualisation(HashMap<String, Set<String>> nodes, HashMap<String, Set<String>> edges, String name, boolean onlyToFile) {

        this.name = name;
        graph = new SingleGraph(name);
        this.nodes = nodes;
        this.edges = edges;
        this.onlyToFile = onlyToFile;
        this.op = new OutPutManagament();

    }

    public void visualise() {

        FileSinkImages pic = new FileSinkImages(OutputType.PNG, Resolutions.VGA);
        pic.setRenderer(RendererType.SCALA);

        pic.setLayoutPolicy(LayoutPolicy.COMPUTED_FULLY_AT_NEW_IMAGE);

        for (String type : nodes.keySet()) {
            Set<String> node = nodes.get(type);
            for (String n : node) {
                System.out.println(n);
                String name = n
                        + graph.addNode(n);
                if (type.equals("Target")) {
                    Node node2 = graph.getNode(n);
                    node2.addAttribute("ui.class", "target");
                }
                if (type.equals("Compound")) {
                    Node node2 = graph.getNode(n);
                    node2.addAttribute("ui.class", "compound");
                }

                if (type.equals("MolFunc")) {
                    Node node2 = graph.getNode(n);
                    node2.addAttribute("ui.class", "molfunc");
                }


                if (type.equals("Indications")) {
                    Node node2 = graph.getNode(n);
                    node2.addAttribute("ui.class", "indication");
                }

                if (type.equals("Protein")) {
                    Node node2 = graph.getNode(n);
                    node2.addAttribute("ui.class", "protein");
                }
                
                  if (type.equals("Publication")) {
                    Node node2 = graph.getNode(n);
                    node2.addAttribute("ui.class", "publication");
                }



            }


        }
        for (String edgeType : edges.keySet()) {
            Set<String> edge = edges.get(edgeType);
            for (String n : edge) {
                String[] split = n.split(":");
                graph.addEdge(split[0] + split[1] + "type", split[0], split[1], true);
            }
        }
        for (Node node : graph) {
            node.addAttribute("ui.label", node.getId());
        }
        for (Edge edge : graph.getEdgeSet()) {
            edge.addAttribute("ui.label", edge.getId());
        }

        System.setProperty("org.graphstream.ui.renderer",
                "org.graphstream.ui.j2dviewer.J2DGraphRenderer");


        if (onlyToFile == false) {
            graph.display();
        }


        graph.addAttribute(
                "ui.stylesheet", "url('graphstreamStyle.css')");

        String directory = op.getVisualisationDirectoryPath();
        
        try {
            pic.writeAll(graph, directory + name + System.currentTimeMillis() / 100+ ".png");
        } catch (IOException ex) {
            Logger.getLogger(GS_Core_Visualisation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.visualisation;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import org.jgrapht.graph.DefaultEdge;
import uk.ac.ncl.semsubgraphalgorithm.SerialisedGraph;
import uk.ac.ncl.semsubgraphalgorithm.SourceGraph;


/**
 *
 * @author joemullen
 */
public class ExtractSubgraph {

    private Set<String> initialNodes;
    private SourceGraph source;
    private HashMap<String, Set<String>> extractedNodes;
    private HashMap<String, Set<String>> extractedEdges;
    private String name;
    private Set<String> subNames;
    private boolean neighbours;
    private boolean onlyToFile;

    public static void main(String[] args) throws IOException, FileNotFoundException, ClassNotFoundException {

        SerialisedGraph ser = new SerialisedGraph();
        SourceGraph source = ser.useSerialized("ran_1000_graph.data");//ser.useSerialized("1000SPIKE_100_.data");
        Set<String> nodes = new HashSet<String>() {
            {
            //7750	6547	6938	5829	6777	3391
//7750	6547	6938	5829	6777	4798
                add("2757");
                add("5950");
                add("6526"); 
                add("7421");
                add("7629");
                add("7761");
//[4532, 5950, 6731, 7268, 7378, 7629]
          
//                add("141256");
//                add("123935");
//                add("31503");
//                add("34867");


                //from the large dataset
                //124223     124210     125187     31262     29053
                // 140030	140005	125753	28742	32472
                //141216	141256	123935	31503	34867

                //from the 5_3
                //[1400, 1399, 1398, 1397, 1396]
            }
        };

        ExtractSubgraph es = new ExtractSubgraph(nodes, source, "subgraphNEIGHBOURS", false);
        es.getSemantics();


    }

    public ExtractSubgraph(Set<String> nodes, SourceGraph source, String name, boolean onlyToFile) {
        this.initialNodes = nodes;
        this.source = source;
        this.name = name;
        this.extractedEdges = new HashMap<>();
        this.extractedNodes = new HashMap<>();
        this.subNames = new HashSet<>();
        this.neighbours = neighbours;
        this.onlyToFile = onlyToFile;
        if (neighbours == true) {
            getNearestNeighbours_NODES();
        } else {
            getOnlySubgraph();
        }

        draw();

    }

    public void draw() {

        System.out.println("extracted nodes: " + extractedNodes.toString());

        GS_Core_Visualisation gs = new GS_Core_Visualisation(extractedNodes, extractedEdges, name, onlyToFile);
        gs.visualise();

    }

    public void getNearestNeighbours_NODES() {
        for (String node : initialNodes) {
            subNames.add(source.getConceptName(node));
            //get all the nodes it has edges too
            Set<DefaultEdge> edges = source.getSourceGraph().edgesOf(node);
            for (DefaultEdge edg : edges) {
                String from = source.getSourceGraph().getEdgeSource(edg);
                String to = source.getSourceGraph().getEdgeTarget(edg);
                String fromName = source.getConceptName(from);
                String toName = source.getConceptName(to);
                String ccto = source.getConceptClass(to);
                String ccfrom = source.getConceptClass(from);
                String relType = source.getRelationType(from, to);
                if (!extractedNodes.containsKey(ccto)) {
                    Set<String> temp = new HashSet<String>();
                    temp.add(toName);
                    extractedNodes.put(ccto, temp);
                }

                if (!extractedNodes.containsKey(ccfrom)) {
                    Set<String> temp = new HashSet<String>();
                    temp.add(fromName);
                    extractedNodes.put(ccfrom, temp);
                }

                if (extractedNodes.containsKey(ccto)) {
                    Set<String> temp = extractedNodes.get(ccto);
                    temp.add(toName);
                    extractedNodes.put(ccto, temp);
                }

                if (extractedNodes.containsKey(ccfrom)) {
                    Set<String> temp = extractedNodes.get(ccfrom);
                    temp.add(fromName);
                    extractedNodes.put(ccfrom, temp);
                }


                if (!extractedEdges.containsKey(relType)) {
                    Set<String> temp = new HashSet<String>();
                    temp.add(fromName + ":" + toName);
                    extractedEdges.put(relType, temp);

                }

                if (extractedEdges.containsKey(relType)) {
                    Set<String> temp = extractedEdges.get(relType);
                    temp.add(fromName + ":" + toName);
                    extractedEdges.put(relType, temp);

                }


            }

        }

    }

    public void getOnlySubgraph() {

        for (String node : initialNodes) {
            System.out.println(node);
            System.out.println(source.getConceptName(node));
            System.out.println(source.getConceptClass(node));
            subNames.add(source.getConceptName(node));
            //get all the nodes it has edges too
            Set<DefaultEdge> edges = source.getSourceGraph().edgesOf(node);
            for (DefaultEdge edg : edges) {
                String from = source.getSourceGraph().getEdgeSource(edg);
                String to = source.getSourceGraph().getEdgeTarget(edg);

                if (initialNodes.contains(to) && initialNodes.contains(from)) {

                    String fromName = source.getConceptName(from);
                    String toName = source.getConceptName(to);
                    String ccfrom = source.getConceptClass(from);
                    String ccto = source.getConceptClass(to);
                    String relType = source.getRelationType(from, to);

                    if (!extractedNodes.containsKey(ccto)) {
                        Set<String> temp = new HashSet<String>();
                        temp.add(toName);
                        extractedNodes.put(ccto, temp);
                    }

                    if (!extractedNodes.containsKey(ccfrom)) {
                        Set<String> temp = new HashSet<String>();
                        temp.add(fromName);
                        extractedNodes.put(ccfrom, temp);
                    }

                    if (extractedNodes.containsKey(ccto)) {
                        Set<String> temp = extractedNodes.get(ccto);
                        temp.add(toName);
                        extractedNodes.put(ccto, temp);
                    }
                    
                    

                    if (extractedNodes.containsKey(ccfrom)) {
                        Set<String> temp = extractedNodes.get(ccfrom);
                        temp.add(fromName);
                        extractedNodes.put(ccfrom, temp);
                    }


                    if (!extractedEdges.containsKey(relType)) {
                        Set<String> temp = new HashSet<String>();
                        temp.add(fromName + ":" + toName);
                        extractedEdges.put(relType, temp);

                    }

                    if (extractedEdges.containsKey(relType)) {
                        Set<String> temp = extractedEdges.get(relType);
                        temp.add(fromName + ":" + toName);
                        extractedEdges.put(relType, temp);

                    }
                    
                    


                }
            }
        }
    }

    public void getSemantics() {

        for (String node : initialNodes) {
            System.out.println("NODE:|"+node + "|" + source.getConceptName(node) + "|" + source.getConceptClass(node));
            //get all the nodes it has edges too
            Set<DefaultEdge> edges = source.getSourceGraph().edgesOf(node);
            System.out.println("Edges"+ edges.size());

            for (DefaultEdge edg : edges) {

                String from = source.getSourceGraph().getEdgeSource(edg);
                String to = source.getSourceGraph().getEdgeTarget(edg);
                System.out.println(from+":"+to);

                if (initialNodes.contains(to) && initialNodes.contains(from)) {
                   
                    System.out.println("EDGE:|"+from+":"+to+ "|" + source.getRelationType(from, to));
                }
            }
        }



    }
}

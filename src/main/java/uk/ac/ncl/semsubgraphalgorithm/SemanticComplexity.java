/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.semsubgraphalgorithm.semanticDistanceSubgraphExclusion.SemanticDistanceCalculator;


/**
 *
 * @author joemullen
 */
public class SemanticComplexity {

    private String[][] sourceNodeRules;
    private int elementsToMakeZero;
    private Map<String, Set<String>> sourceRelRules;
    private QueryGraph query;
    private SemanticDistanceCalculator sdc;
    private double newST;
    private double currentST;
    private QueryGraph newQuery;
    private SourceGraph s;
    private final Map<String, String> NODES;
    private final Map<String, String> RELS;
    private Map<String, String> newNODEInfo = new HashMap<String, String>();
    private Map<String, String> newRELInfo = new HashMap<String, String>();
    private Map<String, String> tempRELInfo = new HashMap<String, String>();
    private Set<String> availableCCs;
    private Set<String> availableRTs;
    private boolean allRelsReplaced;
    private String[] nodeOrder;
    private int next;
    private Set<String> allQUERYElements;
    private double newSTAIM;
    private boolean onlyNODES;
    private boolean successfullyChanged;

    public static void main(String args[]) throws IOException, FileNotFoundException, ClassNotFoundException, Exception {
        //return a subgraph whose ST matches that of the one specified.
//        SerialisedGraph ser = new SerialisedGraph();
//        SourceGraph source = ser.useSerialized("graph.data");
//        QueryGraph q = new QueryGraph(QueryGraph.SemanticSub.TEST10ELEMENT);
//
//        SemanticComplexity sc = new SemanticComplexity(q, 0.8, source);
//        sc.createNewQuery();
//
//        SemanticComplexity sc2 = new SemanticComplexity(q, 0.6, source);
//        sc2.createNewQuery();
//
//        SemanticComplexity sc3 = new SemanticComplexity(q, 0.4, source);
//        sc3.createNewQuery();
//
//        SemanticComplexity sc4 = new SemanticComplexity(q, 0.2, source);
//        sc4.createNewQuery();
//
//        SemanticComplexity sc5 = new SemanticComplexity(q, 0.0, source);
//        sc5.createNewQuery();
//        QueryGraph q = new QueryGraph(QueryGraph.SemanticSub.TEST10ELEMENT);
//        SemanticComplexity sc = new SemanticComplexity(q, 1);
//        SemanticComplexity sc2 = new SemanticComplexity(q, 2);
//        SemanticComplexity sc3 = new SemanticComplexity(q, 3);
//        SemanticComplexity sc4 = new SemanticComplexity(q, 4);
//        SemanticComplexity sc5 = new SemanticComplexity(q, 5);
    }

    public SemanticComplexity(QueryGraph q, int elements, double st, boolean onlyNODES, SourceGraph s) throws Exception {

        this.elementsToMakeZero = elements;
        this.query = q;
        this.NODES = q.getQueryNodeInfo();
        this.RELS = q.getQueryRelInfo();
        this.sdc = new SemanticDistanceCalculator();
        this.newNODEInfo.putAll(NODES);
        this.newRELInfo.putAll(RELS);
        this.tempRELInfo.putAll(RELS);
        this.availableCCs = sdc.getAllConceptClasses();
        this.availableRTs = sdc.getAllRelationTypes();
        this.allQUERYElements = populateAllElements();
        this.newSTAIM = st;
        this.onlyNODES = onlyNODES;
        this.nodeOrder = new String[NODES.size()];
        this.sourceRelRules = s.getRelationTypeRules();
        this.successfullyChanged = false;
        changedSpecifiedNumberOfElementsTOZERO(elements);

    }

    public void changedSpecifiedNumberOfElementsTOZERO(int elements) throws Exception {


        if (elements == 0 || newST == 1.0) {
            setNewQuery("NoneChanged_" + elements);

        } else {

            for (int i = 1; i <= elements; i++) {
                while (successfullyChanged == false) {
                    changeSingleElement();
                }
                System.out.println("[INFO] Changed: " + i + " SS of:" + scoreSubs(newRELInfo));

            }
            //  System.out.println("FINISHED");
            setNewQuery("ElementsChanged_" + elements);
        }
        //  System.out.println(getNewQuery().toString());

    }

    public void changeSingleElement() throws Exception {


        String change = getRandomElement();
        //String ccORrt;

        if (onlyNODES == true) {
            while (!NODES.containsKey(change) && !change.equals("NONE")) {
                change = getRandomElement();
            }
        }

        if (NODES.containsKey(change)) {
            String nodeCC = NODES.get(change);
            for (String cc : availableCCs) {
                System.out.println(cc+ " >>> "+sdc.getNodeDistance(cc, nodeCC));
                if (sdc.getNodeDistance(cc, nodeCC) < (newSTAIM + 0.1) && sdc.getNodeDistance(cc, nodeCC) > (newSTAIM - 0.1)) {
                    String newCC = cc;

                    System.out.println("[INFO] DISTANCE: " + sdc.getNodeDistance(cc, nodeCC));
                    newNODEInfo.put(change, cc);
                    successfullyChanged = true;
                    break;
                }

            }

        }

        if (RELS.containsKey(change)) {
            //elements is a relation

            String relRT = RELS.get(change);
            for (String rt : availableRTs) {
                if (sdc.getEdgeDistance(rt, relRT) == newSTAIM) {
                    //we can use it
                    String newRT = rt;
                    newRELInfo.put(change, rt);
                    break;
                }

            }
        }

    }

    public String getRandomElement() {

        String change;// = getRandomNodeOrRelation();
        Object[] elements;
        elements = allQUERYElements.toArray();

        Random ran = new Random();
        System.out.println("[INFO]Elements length: " + elements.length);
        change = (String) elements[ran.nextInt(elements.length)];
        allQUERYElements.remove(change);


        if (elements.length == 0) {
            return "NONE";
        }

        return change;

    }

    public Set<String> populateAllElements() {

        Set<String> allElements = new HashSet<String>();
        allElements.addAll(NODES.keySet());
        allElements.addAll(RELS.keySet());

        return allElements;

    }

//    public SemanticComplexity(QueryGraph q, double newST, SourceGraph s) {
//
//        this.sourceNodeRules = s.getAverageConnectivityOfEachConceptClass();
//        this.sourceRelRules = s.getRelationTypeRules();
//        this.newST = newST;
//        this.query = q;
//        this.sdc = new SemanticDistanceCalculator();
//        this.s = s;
//        this.NODES = q.getQueryNodeInfo();
//        this.RELS = q.getQueryRelInfo();
//        this.newNODEInfo.putAll(NODES);
//        this.newRELInfo.putAll(RELS);
//        this.tempRELInfo.putAll(RELS);
//        this.availableCCs = sdc.getAllConceptClasses();
//        this.availableRTs = sdc.getAllRelationTypes();
//        this.currentST = 1.0;
//        this.allRelsReplaced = false;
//        this.nodeOrder = new String[NODES.size()];
//        this.next = 0;
//        this.newSTAIM = newST;
//        try {
//            changedSpecifiedNumberOfElementsTOZERO(1);
//        } catch (Exception ex) {
//            Logger.getLogger(SemanticComplexity.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//
//    }
    public void createNewQuery() throws Exception {

        populateNodeOrder();
        int count = 0;
        while (currentST != newST + -0.5) {
            count++;
            changeOneNodeCC();
            if (count == 10000) {
                break;
            }
        }


        // System.out.println("[INPUT NODE_INFO: " + NODES.toString() + "]");
        // System.out.println("[INPUT REL_INFO: " + RELS.toString() + "]");
        System.out.println("[INFO] ACHIEVED ST: " + currentST );
        // System.out.println("[NEW NODE_INFO: " + newNODEInfo.toString() + "]");
        // System.out.println("[NEW REL_INFO: " + newRELInfo.toString() + "]");

        // System.out.println("All rts: " + availableRTs.toString());
    }

    public void changeOneNodeCC() throws Exception {

//        Random rand = new Random();
//        int select = rand.nextInt(NODES.size());
//
//        String node = (String) query.getSub().vertexSet().toArray()[select];

        String node = getNextNode();
        // System.out.println("Looking at: " + node);

        String conceptClass = newNODEInfo.get(node);

        Map<String, String> highestRELInfo = new HashMap<String, String>();
        highestRELInfo.putAll(newRELInfo);

        String newConceptClass = conceptClass;

        //should this be put as 1.0 each time we go around??????
        double howClose = scoreSubs(tempRELInfo) - newST;


        for (String newCC : availableCCs) {
            refreshTempRels();
            replaceCC(node, newCC);
            // System.out.println("what???");
            updateRelations(node);
            double score = scoreSubs(tempRELInfo);
            // System.out.println("");
            // System.out.println("SUB_SCORE: " + score);

            if (score < currentST && score >= newST && allRelsReplaced() == true) {
                if ((score - newST) < howClose) {
                    newConceptClass = newCC;
                    howClose = (score - newST);
                    //        System.out.println("---------------------THIS IS IT");
                    //       System.out.println(score);
                    //       System.out.println(newNODEInfo.toString());
                    //       System.out.println(tempRELInfo.toString());
                    //       System.out.println("---------------------");


                    highestRELInfo.putAll(tempRELInfo);
                }
            }

        }

        // System.out.println("NewCC: " + newConceptClass + " CC " + conceptClass);

        if (newConceptClass.equals(conceptClass)) {
            //nothing changes
            replaceCC(node, newConceptClass);
            refreshTempRels();
            //  System.out.println("NOTHING CHANGED: " + newNODEInfo.toString());

        } else {
            replaceCC(node, newConceptClass);
            acceptTempRels(highestRELInfo);
            updateST();
            // System.out.println("UPDATED: " + newNODEInfo.toString());
            // System.out.println("UPDATED new rel: " + newRELInfo.toString());
            // System.out.println("UPDATED temp rel: " + tempRELInfo.toString());
            refreshTempRels();
        }



    }

    public void updateRelations(String node) {

        //System.out.println("Checking rels for: " + node);
        Map<String, String> associatedRels = new HashMap<String, String>();
        //get all relations that the involved the updated node
        for (String relation : newRELInfo.keySet()) {
            String[] split = relation.split(":");
            if (split[0].equals(node)) {
                associatedRels.put(relation, newRELInfo.get(relation));

            }
            if (split[1].equals(node)) {
                associatedRels.put(relation, newRELInfo.get(relation));
            }

        }


        // System.out.println("RELATIONS TO BE CHANGED: " + associatedRels.toString());
        // System.out.println("UPDATED " + newNODEInfo);
        //System.out.println("All associated relations: " + associatedRels.toString());

        //for each of these relations see if there exists a relation that 
        //follows the rules

        setAllRelsReplaced(false);

        boolean ch = true;
        for (String rel : associatedRels.keySet()) {
            //     System.out.println(rel);
            String[] split = rel.split(":");
            String fromCC = newNODEInfo.get(split[0]);
            String toCC = newNODEInfo.get(split[1]);
            String newRelation = fromCC + ":" + toCC;
            //     System.out.println("newRelation: " + newRelation);
            if (sourceRelRules.containsKey(newRelation)) {
                Set<String> possRTs = sourceRelRules.get(newRelation);
                //       System.out.println(possRTs);
                Random r = new Random();
                int select = r.nextInt(possRTs.size());
                String newRT = (String) possRTs.toArray()[select];
                //     System.out.println("Updating " + rel + " with the new RT: " + newRT);
                replaceTempRELATIONTYPE(rel, newRT);

            } else {

                ch = false;
                //          System.out.println("errrrrr NO");
            }

        }

        if (ch == true) {
            setAllRelsReplaced(true);
        }

        //      System.out.println("RELATIONS UPDATED: " + tempRELInfo.toString());
        //      System.out.println(" ");

    }

    public void replaceCC(String node, String nodeCC) {

        newNODEInfo.remove(node);
        newNODEInfo.put(node, nodeCC);
    }

    public void replaceTempRELATIONTYPE(String rel, String nodert) {

        //System.out.println("IN: "+ rel+"  "+ nodert+"  tr: "+ tempRELInfo.toString());
        tempRELInfo.remove(rel);
        tempRELInfo.put(rel, nodert);
        //System.out.println("OUT: "+  tempRELInfo.toString());

    }

    public void refreshTempRels() {

        this.tempRELInfo.putAll(newRELInfo);

    }

    public void acceptTempRels(Map<String, String> up) {

        // System.out.println("WHY ISN;t THIS ADDED to newRELINFO?? " + up.toString());

        this.newRELInfo.putAll(up);
        this.tempRELInfo.putAll(up);

    }

    public boolean checkRelationRules() {
        boolean check = false;


        return check;

    }

    public void updateST() throws Exception {
        this.currentST = scoreSubs(tempRELInfo);

    }

    public double scoreSubs(Map<String, String> relInfo) throws Exception {
        double score = 0.0;
        //score nodes
        for (String node : NODES.keySet()) {
            score += sdc.getNodeDistance(NODES.get(node), newNODEInfo.get(node));
        }

        //score relations
        for (String relation : RELS.keySet()) {
            String queryCC = relInfo.get(relation);
            if (queryCC.equals("ub_by")) {
                queryCC = "ubiquitinated_by";
            }
            if (queryCC.equals("ph_by")) {
                queryCC = "phosphorylated_by";
            }
            if (queryCC.equals("is_p")) {
                queryCC = "is_part";
            }
            if (queryCC.equals("de_by")) {
                queryCC = "dephosphorylated_by";
            }

            score += sdc.getEdgeDistance(RELS.get(relation), queryCC);
        }

        double ss = score / (RELS.size() + NODES.size());
        return ss;
    }

    public void setNewQuery(String name) {
        this.newQuery = new QueryGraph(query.getSub(), name, newNODEInfo, newRELInfo);
    }

    public QueryGraph getNewQuery() {
        return newQuery;
    }

    public boolean allRelsReplaced() {

        return allRelsReplaced;

    }

    public void setAllRelsReplaced(boolean t) {
        this.allRelsReplaced = t;

    }

    public String getNextNode() {
        String node = nodeOrder[next % nodeOrder.length];
        next++;
        return node;
    }

    public void populateNodeOrder() {

        Object[] names = NODES.keySet().toArray();
        for (int i = 0; i < names.length; i++) {
            nodeOrder[i] = (String) names[i];
        }

    }
}

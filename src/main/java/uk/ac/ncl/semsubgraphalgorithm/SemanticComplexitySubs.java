/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import org.jgrapht.DirectedGraph;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import uk.ac.ncl.semsubgraphalgorithm.semanticDistanceSubgraphExclusion.SemanticDistanceCalculator;


/**
 *
 * @author joemullen
 */
public class SemanticComplexitySubs {

    private QueryGraph[] subs;
    private double[] semanticComplexity;
    private Random ran;
    private SemanticDistanceCalculator sdc;
    private Set<String> conceptClasses;
    private Set<String> relationTypes;
    private int[] subSizeOptions;
    private double[] scores;
    private Map<String, Set<String>> relationRules;

    public static void main(String args[]) throws IOException, FileNotFoundException, ClassNotFoundException, Exception {

        SerialisedGraph ser = new SerialisedGraph();
        SourceGraph source = ser.useSerialized("graph.data");
        double[] score = new double[]{0.0, 0.2, 0.4, 0.6, 0.8, 1.0};
        int[] sizes = new int[]{3, 4, 5, 6};
        for (int i = 0; i < 1; i++) {
            SemanticComplexitySubs scs = new SemanticComplexitySubs(score, sizes, source);
            System.out.print("[INFO] FINISHED round: " + i);
        }

    }

    public SemanticComplexitySubs(double[] scores, int[] subSizeOptions, SourceGraph source) throws Exception {

        this.scores = scores;
        this.subs = new QueryGraph[scores.length];
        this.ran = new Random();
        this.sdc = new SemanticDistanceCalculator();
        this.conceptClasses = sdc.getAllConceptClasses();
        this.relationTypes = sdc.getAllRelationTypes();
        this.subSizeOptions = subSizeOptions;
        this.relationRules = source.getRelationTypeRules();
        run();

    }

    public void run() throws Exception {

        boolean allFilled = false;
        while (allFilled == false) {
            QueryGraph ran = createRandomQuery();
            if (ran != null) {
                allocateQuery(ran, 0.05);
                allFilled = checkAllFilled();
            }
        }

    }

    public void allocateQuery(QueryGraph random, double range) throws Exception {
        double scoredSub = scoreSemCom(random);

        for (int i = 0; i < scores.length; i++) {
            if (subs[i] == null) {
                double requiredScore = scores[i];
                if ((scoredSub > (requiredScore - range)) && (scoredSub < (requiredScore + range))) {
                    subs[i] = random;
                    //  System.out.println("Allocated sub score " + scoredSub + "(required: " + requiredScore + ")");
                    // System.out.println("Allocated sub:" + random.toString());
                }
            }
        }
    }

    public QueryGraph createRandomQuery() throws Exception {


        Map<String, String> nodes = new HashMap<String, String>();
        Map<String, String> relations = new HashMap<String, String>();
        DirectedGraph<String, DefaultEdge> semGraph = new DefaultDirectedGraph<String, DefaultEdge>(
                DefaultEdge.class);
        String subName = null;
        QueryGraph randomQuery = null;



        //we want a subgraph of a random size
        int nodeSize = getRandomSize();

        //create the nodes
        for (int i = 0; i < nodeSize; i++) {
            //add a node to the graph
            String name = "SEM_COM_" + i;
            //assign a random ConceptClass  
            String conceptClass = getRandomConceptClass();
            nodes.put(name, conceptClass);
            semGraph.addVertex(name);
        }
        //this will simply contain a while loop that will call

        //System.out.println("Relation_Rules:" + relationRules.toString());

        for (String node : nodes.keySet()) {
            String fromCC = nodes.get(node);
            for (String node2 : nodes.keySet()) {
                String toCC = nodes.get(node2);
                if (node.equals(node2)) {
                    //nothing- don't want self loops
                } else {
                    String relation = fromCC + ":" + toCC;
                    String nameRelation = node + ":" + node2;
                    String REVERSErelation = toCC + ":" + fromCC;
                    String REVERSEnameRelation = node2 + ":" + node;


                    //symmetrical relations??
                    if (relationRules.containsKey(relation)) {
                        Set<String> relTypes = relationRules.get(relation);
                        String rt = pickRandomReltype(relTypes);
                        if (rt != null) {
                            semGraph.addEdge(node, node2);
                            relations.put(nameRelation, rt);
                        }
                    }

                    if (relationRules.containsKey(REVERSErelation)) {
                        Set<String> relTypes = relationRules.get(REVERSErelation);
                        String rt = pickRandomReltype(relTypes);
                        if (rt != null) {
                            semGraph.addEdge(node2, node);
                            relations.put(REVERSEnameRelation, rt);
                        }
                    }


                }

            }


        }

        //delete any nodes for which it was not possible to create any relations for
        //store nodes to be deleted in a Set- avoid concurrent modification error
        Set<String> toDelete = new HashSet<String>();
        for (String node : nodes.keySet()) {
            if (semGraph.edgesOf(node).size() == 0) {
                toDelete.add(node);
            }
        }

        for (String delete : toDelete) {
            semGraph.removeVertex(delete);
            nodes.remove(delete);
        }

        if ((nodes.size() > 3) && (connectedCheck(semGraph) == true)) {
            subName = "SEM_COM_" + nodes.size() + "/" + relations.size();
            randomQuery = new QueryGraph(semGraph, subName, nodes, relations);
            // return randomQuery;
        } else {
        }

        return randomQuery;
    }

    public double scoreSemCom(QueryGraph randomQuery) throws Exception {

        int elementsScored = getNumberOfScorableElements(randomQuery);

        Map<String, String> nodes = randomQuery.getQueryNodeInfo();
        Map<String, String> relations = randomQuery.getQueryRelInfo();

        double nodeScores = getScores(nodes, true);
        double relScores = getScores(relations, false);

        double semanticComplexityScore = (nodeScores + relScores) / elementsScored;

        return semanticComplexityScore;
    }

    public double getScores(Map<String, String> info, boolean nodes) throws Exception {

        double score = 0.0;
        Set<String> element = info.keySet();
        //score every element against everyother element apart from itself
        for (String nameOne : element) {
            String type1 = info.get(nameOne);
            for (String nameTwo : element) {
                String type2 = info.get(nameTwo);
                if (!nameOne.equals(nameTwo)) {
                    if (nodes == true) {
                        score += sdc.getNodeDistance(type1, type2);
                    } else if (nodes == false) {
                        if (relationTypes.contains(type1) && relationTypes.contains(type2)) {
                            score += sdc.getEdgeDistance(type1, type2);
                        }
                    }
                }
            }
        }
        return score;
    }

    //method returns the number of scores that will be produced from a QueryGraph
    public int getNumberOfScorableElements(QueryGraph randomQuery) {

        int nodes = randomQuery.getSub().vertexSet().size();
        int relations = randomQuery.getSub().edgeSet().size();

        int nodeScores = nodes * (nodes - 1);
        int relScores = relations * (relations - 1);

        int elementScored = nodeScores + relScores;

        return elementScored;

    }

    public boolean checkAllFilled() {
        boolean allFilled = true;
        for (int i = 0; i < subs.length; i++) {
            if (subs[i] == null) {
                allFilled = false;
            }
        }
        return allFilled;

    }

    public String pickRandomReltype(Set<String> options) {
        // System.out.println("OPtions: " + options.toString());
        String relationType = null;
        int element = ran.nextInt(options.size());
        // System.out.println("ran int: " + element);
        int count = 0;
        for (String rt : options) {
            if (element == count && relationTypes.contains(rt)) {
                relationType = rt;
                break;

            }
            count++;
        }

        //   System.out.println("returning: " + relationType);
        return relationType;

    }

    public String getRandomConceptClass() {

        int element = ran.nextInt(conceptClasses.size());
        Object[] ccs = conceptClasses.toArray();
        if (element == conceptClasses.size()) {
            element -= element;
        }
        String conceptClass = (String) ccs[element];
        return conceptClass;

    }

    public int getRandomSize() {

        int element = ran.nextInt(subSizeOptions.length);
        if (element == subSizeOptions.length) {
            element -= element;
        }
        int size = subSizeOptions[element];

        return size;
    }

    public QueryGraph[] getQueryGraphs() {
        return subs;
    }

    public double[] getScores() {
        return semanticComplexity;
    }

    public boolean connectedCheck(DirectedGraph<String, DefaultEdge> semGraph) {

        boolean connected = true;

        SimpleGraph<String, DefaultEdge> UDorigi = new SimpleGraph<String, DefaultEdge>(DefaultEdge.class);
        Set<String> allNodes = semGraph.vertexSet();

        for (String no : allNodes) {
            UDorigi.addVertex(no);
        }

        Set<DefaultEdge> allEdges = semGraph.edgeSet();
        for (DefaultEdge ed : allEdges) {
            String from = semGraph.getEdgeSource(ed);
            String to = semGraph.getEdgeTarget(ed);
            if (!from.equals(to)) {
                UDorigi.addEdge(from, to);
            }
        }

        //check the graph is connected

        for (String nodes : allNodes) {
            for (String nodes2 : allNodes) {
                if (getShortestPath(nodes, nodes2, UDorigi) == false) {
                    System.out.println("[INFO] GRAPH FAILED CONNECTIVITY:" + semGraph.toString());

                    return false;
                }

            }
        }

        return connected;

    }

    public boolean getShortestPath(String node1, String node2, SimpleGraph<String, DefaultEdge> UDorigi) {

        boolean check = true;

        if (UDorigi.containsVertex(node1) && UDorigi.containsVertex(node2)) {
            DijkstraShortestPath tr2;
            tr2 = new DijkstraShortestPath(UDorigi, node1, node2);
//            if(tr2.getPath().getEdgeList().isEmpty()){
//                System.out.println("hereherherheherherherherhe??@?!?!?!");
//                return false;
//            }

            if (tr2.getPathEdgeList() == null) {
              //  System.out.println("wearlllly??????");
                return false;

            }
//            if (tr2 == null) {
//                System.out.println("hereherherheherherherherhe??@?!?!?!");
//                return false;
//            }
        }

        return check;
    }
}

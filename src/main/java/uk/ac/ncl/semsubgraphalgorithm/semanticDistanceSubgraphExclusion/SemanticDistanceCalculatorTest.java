/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.semanticDistanceSubgraphExclusion;

/**
 *
 * @author joemullen
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sjcockell
 */
public class SemanticDistanceCalculatorTest {

    /**
     * 
     * @param args 
     */
    
    public static void main(String [] args) {
        SemanticDistanceCalculator sdc = new SemanticDistanceCalculator();
        
        try {    
            //System.out.println("are we getting to here?");
            System.out.println(""+sdc.getEdgeDistance("has_similar_sequence", "sim"));
            System.out.println(""+sdc.getEdgeDistance("has_child","has_parent"));
            System.out.println(""+sdc.getEdgeDistance("has_child","has_child"));
            System.out.println(""+sdc.getEdgeDistance("has_parent","has_parent"));
            System.out.println(""+sdc.getNodeDistance("Indications", "Target"));
            System.out.println(""+sdc.getNodeDistance( "Target", "Indications"));
            System.out.println(""+sdc.getNodeDistance("Target", "MolFunc"));
            System.out.println("All ccs :"+ sdc.getAllConceptClasses().toString());
            System.out.println("All rts :"+ sdc.getAllRelationTypes().toString());
            
        } catch (Exception ex) {
            Logger.getLogger(SemanticDistanceCalculatorTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}



/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm;

/**
 *
 * @author joemullen
 */
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

public class RandomGraph extends SourceGraph {

    //want to make sure that we create relations of the right proportion to the right concept class...
    //then delete that node from the candidate nodes... so as to not create the same relation twice!!!
    //need to improve this !!!!!! it itsn
    private Set<String> nodes;
    private ArrayList<String> relations;
    private DirectedGraph<String, DefaultEdge> random;
    private String[][] ranConInfo;
    private HashMap<String, String> ranRelInfo;
    private SourceGraph source;
    private long startTime;
    private long endTime;
    private String summary;
    private boolean symmetrical = false;
    private int sourceNodeSize;
    private String[][] sourceNodeRules;
    private Map<String, Set<String>> sourceRelRules;
    private int newSize;
    private double connectivity;

    public static void main(String args[]) throws IOException, FileNotFoundException, ClassNotFoundException {

        SerialisedGraph ser = new SerialisedGraph();
        SourceGraph source = ser.useSerialized("graph2.data");
        //RandomGraph rg = new RandomGraph(source, 15000, 1);
        source.getCCFrequency();
        source.getAverageConnectivityOfEachConceptClass();

    }

    public RandomGraph(SourceGraph source, int newSize, double connectivity) throws IOException {

        this.random = new DefaultDirectedGraph<String, DefaultEdge>(
                DefaultEdge.class);
        this.sourceNodeSize = source.getGraphNodeSize();
        this.sourceNodeRules = source.getAverageConnectivityOfEachConceptClass();
        this.sourceRelRules = source.getRelationTypeRules();
        this.newSize = newSize;
        this.connectivity = connectivity;
        this.ranRelInfo = new HashMap<String, String>();
        createSemanticGraph();
        System.out.println(getSummary());
    }

    public void createSemanticGraph() throws IOException {
        long startTime = System.currentTimeMillis();

        createNodes();

        //create the relations following the rules in the semInfo (relations can only go TO the conceptclasses in the array)
        Random rand = new Random();
        Set<String> nodes = random.vertexSet();
        int relcount = 0;
        for (String node : nodes) {



            String cc = ranConInfo[Integer.parseInt(node) - 1][2];
            //System.out.println("CC from: " + cc);
            String from = node;
            String rules = getRules(cc);
            int numberOfOutGoingRels = getNumberOfOutGoingRels(cc);

            if (rules.isEmpty()) {

                //means there is no outgoing relations from this cc
                String[] info = getFromRelationInfo(node, cc);
                random.addEdge(info[1], node);
                ranRelInfo = (HashMap<String, String>) addSemanticRel(info[1], node, info[3], ranRelInfo);


            } else {

                String[][] splitRules = splitRules(rules);

                //for each rule we must create the number of relations expected
                //to that concept class
                // System.out.println("RULIES: " + Arrays.deepToString(splitRules));

                //go through each rulie of the ORIGINAL NODE
                for (int p = 0; p < splitRules.length; p++) {
                    //the first ruleTO
                    String toconclass = splitRules[p][0];



                    Set<String> alreadyrelated = new HashSet<String>();
                    //do we have symmetrical relations??
                    int number = (Integer.parseInt(splitRules[p][1]) / (int) connectivity);
                    String rulesTo = getRules(toconclass);

                    // System.out.println(rulesTo.length());

                    if (rulesTo.length() != 0) {
                        String[][] splitRulesTO = splitRules(rulesTo);
                        //get the rules for the TO conceptclass

                        symmetrical = false;

                        //go through each rule of the TO concept class
                        for (int u = 0; u < splitRulesTO.length; u++) {

                            String toFROMconceptclass = splitRulesTO[u][0];
                            //if the TO conceptClass we are looking at MATCHES one of its rules
                            if (toFROMconceptclass.equals(cc)) {

                                //minus the relations that the node already has to said conceptclass
                                //from the total we must create

                                Set<DefaultEdge> alreadyAdded = random.edgesOf(from);

                                int alreadyadded = 0;
                                for (DefaultEdge de : alreadyAdded) {
                                    String conceptClassTO = "";
                                    if (random.getEdgeSource(de).equals(from)) {
                                        conceptClassTO = ranConInfo[Integer.parseInt(random.getEdgeTarget(de)) - 1][2];
                                        alreadyrelated.add(random.getEdgeTarget(de));
                                    }


                                    if (conceptClassTO.equals(toconclass)) {
                                        number = number - 1;
                                    }

                                }


                                symmetrical = true;
                            }


                        }



                        //first of all we create an array of those nodes that are in 
                        //the graph and match the concept class we are interested in
                        List<String> relevantNodes = new ArrayList<String>();
                        for (int h = 0; h < ranConInfo.length; h++) {
                            if (ranConInfo[h][2].equals(toconclass)) {
                                relevantNodes.add(ranConInfo[h][0]);
                            }
                        }

                        for (String al : alreadyrelated) {
                            if (relevantNodes.contains(al)) {

                                relevantNodes.remove(al);
                                //System.out.println("deleted: " + al + " as they are already related");
                            }

                        }

                        //for loops not allowed
                        relevantNodes.remove(from);

                        //if we have already created all the edges as symmetrical edges no need to create

                        if (number > 0) {
                            for (int f = 0; f < number; f++) {

                                int randomNumber = rand.nextInt(relevantNodes.size());
                                String to = relevantNodes.get(randomNumber);
                                if (random.containsEdge(from, to)) {
                                    while (random.containsEdge(from, to)) {
                                        randomNumber = rand.nextInt(relevantNodes.size());
                                        to = relevantNodes.get(randomNumber);


                                    }


                                } else {

                                    random.addEdge(from, to);
                                    String rt = getRandomRelType(getRandomCC(from), getRandomCC(to));
                                    ranRelInfo = (HashMap<String, String>) addSemanticRel(from, to, rt, ranRelInfo);

                                    //  System.out.println("FROM: " + getRandomCC(from) + "TO: " + getRandomCC(to));
                                    //if symmetrical then add a return edge
                                    if (symmetrical == true) {

                                        random.addEdge(to, from);
                                        //String rt = getRandomRelType(getRandomCC(from), getRandomCC(to));      
                                        ranRelInfo = (HashMap<String, String>) addSemanticRel(to, from, rt, ranRelInfo);
                                        relcount++;


                                    }
                                    //don't want to create the same edge twice
                                    relevantNodes.remove(to);
                                    relcount++;

                                }
                                symmetrical = false;
                            }

                        }

                    }

                }

            }


        }

        //final check to make sure there are no unconnectednodes in the new graph

        source = new SourceGraph(random, ranConInfo, ranRelInfo);

        int count = 0;
        for (String node : source.getSourceGraph().vertexSet()) {
            if (source.getSourceGraph().edgesOf(node).size() == 0) {
                count++;
                //System.out.println(count + " NO_EDGES: " + node + " " + (source.getConceptClass(node)));
                String[] info = getFromRelationInfo(node, source.getConceptClass(node));
                random.addEdge(info[1], node);
                ranRelInfo = (HashMap<String, String>) addSemanticRel(info[1], node, info[3], ranRelInfo);

            }

        }

        source = new SourceGraph(random, ranConInfo, ranRelInfo);


        long endTime = System.currentTimeMillis();




    }

    public void createNodes() {
        String[][] newGraphInfo = new String[newSize][4];

        int nodeMultiply = newSize / 100;
        
        //System.out.println(Arrays.deepToString(sourceNodeRules));

        //System.out.println("SOOURCE NODE RULES:::" + sourceNodeRules.length);

        int nodeName = 0;
        //create nodes
        for (int i = 0; i < sourceNodeRules.length; i++) {

            String cc = sourceNodeRules[i][0];
            int number = Integer.parseInt(sourceNodeRules[i][1]) * nodeMultiply;
            for (int y = 0; y < number; y++) {
                if ((nodeName) < newSize) {
                    random.addVertex(Integer.toString(nodeName + 1));
                    //we do this so that we can use the same methods in Source Graph, which uses nodeIDs from Ondex which start at 
                    newGraphInfo[nodeName][0] = (Integer.toString(nodeName + 1));
                    newGraphInfo[nodeName][1] = "Random";
                    //format- source graph has cc in the [0][2]
                    newGraphInfo[nodeName][2] = cc;
                    newGraphInfo[nodeName][3] = "RANDOM_" + (Integer.toString(nodeName + 1));
                }
                
                else{
                
                System.out.println("Node too much!!>  "+ (nodeName+1) + "  "+ cc);
                
                }

                nodeName++;


            }

        }

        ranConInfo = newGraphInfo;


    }
    //connectivity is 10: if we put 2 in as an argument it is divided by 2 = 5

    public String getRules(String cc) {
        String rules = "";
        for (int i = 0; i < sourceNodeRules.length; i++) {
            if (sourceNodeRules[i][0].equals(cc)) {
                rules = sourceNodeRules[i][3];
                //numberOfOutGoingRels = (int) (((Integer.parseInt(semInfo[i][2]))) / connectivity);
                //System.out.println("Number of outgoring rels of " + cc + " : " + numberOfOutGoingRels);
            }
        }
        return rules;
    }

    public int getNumberOfOutGoingRels(String cc) {

        int rels = 0;
        for (int i = 0; i < sourceNodeRules.length; i++) {
            if (sourceNodeRules[i][0].equals(cc)) {

                rels = (int) (((Integer.parseInt(sourceNodeRules[i][2]))) / connectivity);
                //System.out.println("Number of outgoring rels of " + cc + " : " + numberOfOutGoingRels);
            }
        }
        return rels;

    }

    public String getRandomNodeOfType(String cc) {
        Set<String> possibleNodes = new HashSet<String>();

        for (int i = 0; i < ranConInfo.length; i++) {

            if (ranConInfo[i][2].equals(cc)) {
                possibleNodes.add(Integer.toString(i + 1));
            }
        }

        Random ran = new Random();
        String node = (String) possibleNodes.toArray()[ran.nextInt(possibleNodes.size())];

        return node;

    }

    public String[][] splitRules(String rules) {
        // System.out.println("String rules: "+ rules);
        if (rules.endsWith(",")) {
            rules = rules.substring(0, rules.length() - 1);

        }
        String[] rul = rules.split(",");
        String[][] rulies = new String[rul.length][2];

        for (int i = 0; i < rul.length; i++) {

            String[] split = rul[i].split("=");
            String toconclass = split[0];
            String freq = split[1];
            rulies[i][0] = toconclass;
            rulies[i][1] = freq;
        }

        return rulies;

    }

    public String getRandomRelType(String ccfrom, String ccto) {

        String relType = "";
        String ccRel = ccfrom + ":" + ccto;
        Set<String> relTypes = sourceRelRules.get(ccRel);

        Random rand = new Random();
        relType = (String) relTypes.toArray()[rand.nextInt(relTypes.size())];

        return relType;

    }

    public SourceGraph getRandomSourceGraph() {

        return source;

    }

    public SourceGraph getCloneForEditing() {
        SourceGraph c = (SourceGraph) source.clone();
        return c;
    }

    public DirectedGraph<String, DefaultEdge> getRandomGraph() {

        return random;
    }

    public String[][] getSemInfo() {
        return ranConInfo;

    }

    public String getSummary() {

        return "-----------------------------------------------------" + "\n" + "[RANDOM GRAPH TOOK: " + getTime() + " seconds]" + "\n" + "[NEW GRAPH: " + random.vertexSet().size() + "/" + random.edgeSet().size() + "]";
    }

    public String getRandomCC(String node) {

        return ranConInfo[Integer.parseInt(node) - 1][2];

    }

    public String[] getFromRelationInfo(String node, String cc) {

        Map<String, Set<String>> options = getPossibleRelations(node, cc);
        Random rand = new Random();
        String relation = (String) options.keySet().toArray()[rand.nextInt(options.keySet().size())];

        String[] split = relation.split(":");
        String from = split[0];
        String to = split[1];

        // Set<String> relTypes = options.get(relation);
        // String relationType = (String)relTypes.toArray()[rand.nextInt(relTypes.size())];
        String relationType = getRandomRelType(from, to);
        String[] info = new String[4];
        info[0] = from;
        info[1] = getRandomNodeOfType(info[0]);
        info[2] = to;
        info[3] = relationType;

        return info;

    }

    public Map<String, Set<String>> getPossibleRelations(String node, String cc) {


        Map<String, Set<String>> possibleRelations = new HashMap<String, Set<String>>();

        for (String relccs : sourceRelRules.keySet()) {

            String[] split = relccs.split(":");
            if (split[0].equals(cc)) {
                possibleRelations.put(relccs, sourceRelRules.get(relccs));

            }

            if (split[1].equals(cc)) {
                possibleRelations.put(relccs, sourceRelRules.get(relccs));
            }


        }

        return possibleRelations;

    }

    public String getTime() {
        String time = (" " + (double) (endTime - startTime) / 1000);
        return time.trim();
    }
}

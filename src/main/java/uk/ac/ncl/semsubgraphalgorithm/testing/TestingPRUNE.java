/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.testing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import uk.ac.ncl.semsubgraphalgorithm.OutPutManagament;
import uk.ac.ncl.semsubgraphalgorithm.QueryGraph;
import uk.ac.ncl.semsubgraphalgorithm.QueryGraph.SemanticSub;
import uk.ac.ncl.semsubgraphalgorithm.RandomGraph;
import uk.ac.ncl.semsubgraphalgorithm.SemSearch;
import uk.ac.ncl.semsubgraphalgorithm.SemanticComplexity;
import uk.ac.ncl.semsubgraphalgorithm.SemanticComplexitySubs;
import uk.ac.ncl.semsubgraphalgorithm.SerialisedGraph;
import uk.ac.ncl.semsubgraphalgorithm.SourceGraph;
import uk.ac.ncl.semsubgraphalgorithm.semanticGraphPruning.GraphPruning;
import uk.ac.ncl.semsubgraphalgorithm.spikingGraph.SpikeSourceGraph;


/**
 *
 * @author joemullen
 */
public class TestingPRUNE {

    private double[] thresholds = new double[]{0.0, 0.2, 0.4, 0.6, 0.8, 1.0};
    private double[] SUBthresholds = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
    private FileWriter fstreamTRUE;
    private BufferedWriter bwTRUE;
    private String filenameTRUE;
    private FileWriter fstreamFALSE;
    private BufferedWriter bwFALSE;
    private String filenameFALSE;
    private int replicates;
    private double runST;
    private OutPutManagament op;

    public static void main(String args[]) throws IOException, FileNotFoundException, ClassNotFoundException, Exception {

        double[] sts = new double[]{0.2};
        for (int i = 0; i < sts.length; i++) {
            TestingPRUNE st = new TestingPRUNE(1, sts[i]);
            System.gc();
        }

    }

    public TestingPRUNE(int replicate, double RUNST) throws IOException, FileNotFoundException, ClassNotFoundException, Exception {

        this.op = new OutPutManagament();
        this.filenameTRUE = op.getResultsName()+"testPRUNE_TRUE_" + RUNST + ".txt";
        this.filenameFALSE = op.getResultsName()+"testPRUNE_FALSE_" + RUNST + ".txt";
        this.replicates = replicate;
        this.runST = RUNST;
        SerialisedGraph ser = new SerialisedGraph();
        SourceGraph source = ser.useSerialized("graph.data");
        runWithDifferentSub(source);
        bwTRUE.close();
        bwFALSE.close();
        individParseResults();

    }

    public void runWithDifferentSub(SourceGraph source) throws IOException, FileNotFoundException, ClassNotFoundException, Exception {


        //create a random graph of size 1000 
        this.fstreamTRUE = new FileWriter(filenameTRUE);
        this.bwTRUE = new BufferedWriter(fstreamTRUE);
        this.fstreamFALSE = new FileWriter(filenameFALSE);
        this.bwFALSE = new BufferedWriter(fstreamFALSE);

        for (int i = 1; i <= replicates; i++) {
            bwTRUE.append(">>>" + i + "\n");
            bwFALSE.append(">>>" + i + "\n");
            int[] sizes = new int[]{3, 4, 5, 6};
            SemanticComplexitySubs scs = new SemanticComplexitySubs(SUBthresholds, sizes, source);
            QueryGraph query = new QueryGraph(SemanticSub.SIMCOMPSSMALL);
            RandomGraph ran = new RandomGraph(source, 1000, 1);
            SourceGraph ranSource = ran.getRandomSourceGraph();
            QueryGraph[] subs = scs.getQueryGraphs();

            for (int y = 0; y < subs.length; y++) {
                //spike the graph-DO NOT PLUG IT 
                singleRun(subs[y], ranSource, i);
            }
        }
    }

    public void singleRun(QueryGraph q, SourceGraph ranSource, int replicate) throws IOException, FileNotFoundException, ClassNotFoundException, Exception {

        System.out.println("ACTUAL_SUB: " + q.toString());

        for (int i = 0; i < thresholds.length; i++) {

            System.out.println("Threshold: " + thresholds[i]);

            //this is where you need to change one element of the graph.   
            SemanticComplexity alterSub = new SemanticComplexity(q, 1, thresholds[i], true, ranSource);
            alterSub.createNewQuery();
            QueryGraph alteredSub = alterSub.getNewQuery();

            //spike graph with the altered semantic subgraph
            SpikeSourceGraph g = new SpikeSourceGraph(ranSource, alteredSub, 100, false);
            SourceGraph spiked = g.getSpikedGraph();

            //do a semantic search for the spiked subgraphs using the thresholds
            SemSearch noneSpikedSearchFALSE = new SemSearch(spiked, q, null, null, 8,"any", true, runST, true, false, false);                                                                
            noneSpikedSearchFALSE.completeSearch();
            int nsMatchesFALSE = noneSpikedSearchFALSE.numberOfMathces();
            bwFALSE.append(nsMatchesFALSE + "\t");

//            SemSearch noneSpikedSearchTRUE = new SemSearch(spiked, q, 6, "any", true, runST, true, false, true);
//            noneSpikedSearchTRUE.completeSearch();
//            int nsMatchesTRUE = noneSpikedSearchTRUE.numberOfMathces();
//            bwTRUE.append(nsMatchesTRUE + "\t");


            //prune the graph
            GraphPruning gp = new GraphPruning(spiked, q, runST); //what is going to go here as the Semantic Threshold??
            SourceGraph prunedSource = gp.getPrunedSource();



            SemSearch spikedSearchFALSE = new SemSearch(prunedSource, q, null, null, 8, "any", true, runST, true, false, false);
            spikedSearchFALSE.completeSearch();
            int sMatchesFALSE = spikedSearchFALSE.numberOfMathces();
            bwFALSE.append(sMatchesFALSE + "\t" + "\n");

//            SemSearch spikedSearchTRUE = new SemSearch(prunedSource, q, 6, "any", true, runST, true, false, true);
//            spikedSearchTRUE.completeSearch();
//            int sMatchesTRUE = spikedSearchTRUE.numberOfMathces();
//            bwTRUE.append(sMatchesTRUE + "\t" + "\n");

        }

    }

    public void individParseResults() throws FileNotFoundException, IOException {

        //  BufferedReader brTRUE = new BufferedReader(new FileReader(filenameTRUE));
        //  parseResults(brTRUE, true);
        BufferedReader brFALSE = new BufferedReader(new FileReader(filenameFALSE));
        parseResults(brFALSE, false);

    }

    public void parseResults(BufferedReader br, boolean type) throws FileNotFoundException, IOException {

        String line;

        int[] resultsSpiked00 = new int[thresholds.length];
        int[] resultsNoneSpiked00 = new int[thresholds.length];

        int[] resultsSpiked02 = new int[thresholds.length];
        int[] resultsNoneSpiked02 = new int[thresholds.length];

        int[] resultsSpiked04 = new int[thresholds.length];
        int[] resultsNoneSpiked04 = new int[thresholds.length];

        int[] resultsSpiked06 = new int[thresholds.length];
        int[] resultsNoneSpiked06 = new int[thresholds.length];

        int[] resultsSpiked08 = new int[thresholds.length];
        int[] resultsNoneSpiked08 = new int[thresholds.length];

        int[] resultsSpiked10 = new int[thresholds.length];
        int[] resultsNoneSpiked10 = new int[thresholds.length];

        int count = 0;
        int count2 = 0;

        while ((line = br.readLine()) != null) {
            if (line.startsWith(">>>")) {
                count = 0;
                count2 = 0;

            } else {
                if (count2 % thresholds.length == 0 && count2 > 0) {
                    //System.out.println("c2: " + count2 + " c: " + count);
                    count++;
                    count2 = 0;
                }

                //System.out.println("COUNT: " + count);
                //System.out.println(line);
                String[] split = line.split("\t");
                if (count == 0) {
                    resultsNoneSpiked00[count2] = Integer.parseInt(split[0]) + resultsNoneSpiked00[count2];
                    resultsSpiked00[count2] = Integer.parseInt(split[1]) + resultsSpiked00[count2];
                }
                if (count == 1) {
                    resultsNoneSpiked02[count2] = Integer.parseInt(split[0]) + resultsNoneSpiked02[count2];
                    resultsSpiked02[count2] = Integer.parseInt(split[1]) + resultsSpiked02[count2];
                }
                if (count == 2) {
                    resultsNoneSpiked04[count2] = Integer.parseInt(split[0]) + resultsNoneSpiked04[count2];
                    resultsSpiked04[count2] = Integer.parseInt(split[1]) + resultsSpiked04[count2];
                }
                if (count == 3) {
                    resultsNoneSpiked06[count2] = Integer.parseInt(split[0]) + resultsNoneSpiked06[count2];
                    resultsSpiked06[count2] = Integer.parseInt(split[1]) + resultsSpiked06[count2];
                }
                if (count == 4) {
                    resultsNoneSpiked08[count2] = Integer.parseInt(split[0]) + resultsNoneSpiked08[count2];
                    resultsSpiked08[count2] = Integer.parseInt(split[1]) + resultsSpiked08[count2];
                }
                if (count == 5) {
                    resultsNoneSpiked10[count2] = Integer.parseInt(split[0]) + resultsNoneSpiked10[count2];
                    resultsSpiked10[count2] = Integer.parseInt(split[1]) + resultsSpiked10[count2];
                }
                count2++;
            }
        }

        for (int i = 0; i < thresholds.length; i++) {
            resultsSpiked00[i] = (resultsSpiked00[i] / replicates);
            resultsNoneSpiked00[i] = (resultsNoneSpiked00[i] / replicates);

            resultsSpiked02[i] = (resultsSpiked02[i] / replicates);
            resultsNoneSpiked02[i] = (resultsNoneSpiked02[i] / replicates);

            resultsSpiked04[i] = (resultsSpiked04[i] / replicates);
            resultsNoneSpiked04[i] = (resultsNoneSpiked04[i] / replicates);

            resultsSpiked06[i] = (resultsSpiked06[i] / replicates);
            resultsNoneSpiked06[i] = (resultsNoneSpiked06[i] / replicates);

            resultsSpiked08[i] = (resultsSpiked08[i] / replicates);
            resultsNoneSpiked08[i] = (resultsNoneSpiked08[i] / replicates);

            resultsSpiked10[i] = (resultsSpiked10[i] / replicates);
            resultsNoneSpiked10[i] = (resultsNoneSpiked10[i] / replicates);

        }

        br.close();


        if (type == true) {

            //Keep the existing content and append the new content in the end of the file.
            fstreamTRUE = new FileWriter(filenameTRUE, true);
            bwTRUE = new BufferedWriter(fstreamTRUE);

            bwTRUE.append("\n");
            bwTRUE.append("[INFO] PLOT:" + "\n");
            String conn = Arrays.toString((thresholds));
            bwTRUE.append("Thresholds->c(" + conn.substring(1, conn.length() - 1) + ")" + "\n");
            String spikedString00 = Arrays.toString((resultsSpiked00));
            bwTRUE.append("Pruned00->c(" + spikedString00.substring(1, spikedString00.length() - 1) + ")" + "\n");
            String nonspiked00 = Arrays.toString((resultsNoneSpiked00));
            bwTRUE.append("Not_Pruned00->c(" + nonspiked00.substring(1, nonspiked00.length() - 1) + ")" + "\n");

            String spikedString02 = Arrays.toString((resultsSpiked02));
            bwTRUE.append("Pruned02->c(" + spikedString02.substring(1, spikedString02.length() - 1) + ")" + "\n");
            String nonspiked02 = Arrays.toString((resultsNoneSpiked02));
            bwTRUE.append("Not_Pruned02->c(" + nonspiked02.substring(1, nonspiked02.length() - 1) + ")" + "\n");

            String spikedString04 = Arrays.toString((resultsSpiked04));
            bwTRUE.append("Pruned04->c(" + spikedString04.substring(1, spikedString04.length() - 1) + ")" + "\n");
            String nonspiked04 = Arrays.toString((resultsNoneSpiked04));
            bwTRUE.append("Not_Pruned04->c(" + nonspiked04.substring(1, nonspiked04.length() - 1) + ")" + "\n");

            String spikedString06 = Arrays.toString((resultsSpiked06));
            bwTRUE.append("Pruned06->c(" + spikedString06.substring(1, spikedString06.length() - 1) + ")" + "\n");
            String nonspiked06 = Arrays.toString((resultsNoneSpiked06));
            bwTRUE.append("Not_Pruned06->c(" + nonspiked06.substring(1, nonspiked06.length() - 1) + ")" + "\n");

            String spikedString08 = Arrays.toString((resultsSpiked08));
            bwTRUE.append("Pruned08->c(" + spikedString08.substring(1, spikedString08.length() - 1) + ")" + "\n");
            String nonspiked08 = Arrays.toString((resultsNoneSpiked08));
            bwTRUE.append("Not_Pruned08->c(" + nonspiked08.substring(1, nonspiked08.length() - 1) + ")" + "\n");

            String spikedString10 = Arrays.toString((resultsSpiked10));
            bwTRUE.append("Pruned10->c(" + spikedString10.substring(1, spikedString10.length() - 1) + ")" + "\n");
            String nonspiked10 = Arrays.toString((resultsNoneSpiked10));
            bwTRUE.append("Not_Pruned10->c(" + nonspiked10.substring(1, nonspiked10.length() - 1) + ")" + "\n");

            int[] plot = new int[thresholds.length];
            for (int i = 0; i < thresholds.length; i++) {
                plot [i] = 
                         
                (((resultsNoneSpiked00[i]-  resultsSpiked00[i])+
                        (resultsNoneSpiked02[i]-  resultsSpiked02[i])+
                        (resultsNoneSpiked04[i]-  resultsSpiked04[i])+
                        (resultsNoneSpiked06[i]-  resultsSpiked06[i])+
                        (resultsNoneSpiked08[i]-  resultsSpiked08[i])+
                        (resultsNoneSpiked10[i]-  resultsSpiked10[i]))/6);

            }

            bwTRUE.append("PLOT->c(" + Arrays.toString(plot));

            bwTRUE.close();
        }

        if (type == false) {

            //Keep the existing content and append the new content in the end of the file.
            fstreamFALSE = new FileWriter(filenameFALSE, true);
            bwFALSE = new BufferedWriter(fstreamFALSE);

            bwFALSE.append("\n");
            bwFALSE.append("[INFO] PLOT:" + "\n");
            String conn = Arrays.toString((thresholds));
            bwFALSE.append("Thresholds->c(" + conn.substring(1, conn.length() - 1) + ")" + "\n");
            String spikedString00 = Arrays.toString((resultsSpiked00));
            bwFALSE.append("Pruned00->c(" + spikedString00.substring(1, spikedString00.length() - 1) + ")" + "\n");
            String nonspiked00 = Arrays.toString((resultsNoneSpiked00));
            bwFALSE.append("Not_Pruned00->c(" + nonspiked00.substring(1, nonspiked00.length() - 1) + ")" + "\n");

            String spikedString02 = Arrays.toString((resultsSpiked02));
            bwFALSE.append("Pruned02->c(" + spikedString02.substring(1, spikedString02.length() - 1) + ")" + "\n");
            String nonspiked02 = Arrays.toString((resultsNoneSpiked02));
            bwFALSE.append("Not_Pruned02->c(" + nonspiked02.substring(1, nonspiked02.length() - 1) + ")" + "\n");

            String spikedString04 = Arrays.toString((resultsSpiked04));
            bwFALSE.append("Pruned04->c(" + spikedString04.substring(1, spikedString04.length() - 1) + ")" + "\n");
            String nonspiked04 = Arrays.toString((resultsNoneSpiked04));
            bwFALSE.append("Not_Pruned04->c(" + nonspiked04.substring(1, nonspiked04.length() - 1) + ")" + "\n");

            String spikedString06 = Arrays.toString((resultsSpiked06));
            bwFALSE.append("Pruned06->c(" + spikedString06.substring(1, spikedString06.length() - 1) + ")" + "\n");
            String nonspiked06 = Arrays.toString((resultsNoneSpiked06));
            bwFALSE.append("Not_Pruned06->c(" + nonspiked06.substring(1, nonspiked06.length() - 1) + ")" + "\n");

            String spikedString08 = Arrays.toString((resultsSpiked08));
            bwFALSE.append("Pruned08->c(" + spikedString08.substring(1, spikedString08.length() - 1) + ")" + "\n");
            String nonspiked08 = Arrays.toString((resultsNoneSpiked08));
            bwFALSE.append("Not_Pruned08->c(" + nonspiked08.substring(1, nonspiked08.length() - 1) + ")" + "\n");

            String spikedString10 = Arrays.toString((resultsSpiked10));
            bwFALSE.append("Pruned10->c(" + spikedString10.substring(1, spikedString10.length() - 1) + ")" + "\n");
            String nonspiked10 = Arrays.toString((resultsNoneSpiked10));
            bwFALSE.append("Not_Pruned10->c(" + nonspiked10.substring(1, nonspiked10.length() - 1) + ")" + "\n");

            int[] plot = new int[thresholds.length];
            for (int i = 0; i < thresholds.length; i++) {
                plot [i] = 
                         
                (((resultsNoneSpiked00[i]-  resultsSpiked00[i])+
                        (resultsNoneSpiked02[i]-  resultsSpiked02[i])+
                        (resultsNoneSpiked04[i]-  resultsSpiked04[i])+
                        (resultsNoneSpiked06[i]-  resultsSpiked06[i])+
                        (resultsNoneSpiked08[i]-  resultsSpiked08[i])+
                        (resultsNoneSpiked10[i]-  resultsSpiked10[i]))/6);

            }

            bwFALSE.append("PLOT->c(" + Arrays.toString(plot));
                          
            bwFALSE.close();
        }


    }
}

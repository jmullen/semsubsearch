/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.testing;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import uk.ac.ncl.semsubgraphalgorithm.OutPutManagament;
import uk.ac.ncl.semsubgraphalgorithm.QueryGraph;
import uk.ac.ncl.semsubgraphalgorithm.RandomGraph;
import uk.ac.ncl.semsubgraphalgorithm.RandomQuery;
import uk.ac.ncl.semsubgraphalgorithm.SemSearch;
import uk.ac.ncl.semsubgraphalgorithm.SerialisedGraph;
import uk.ac.ncl.semsubgraphalgorithm.SourceGraph;
import uk.ac.ncl.semsubgraphalgorithm.graphSplitting.GraphSplit_INSTANCE;
import uk.ac.ncl.semsubgraphalgorithm.graphSplitting.RunSplit;
import uk.ac.ncl.semsubgraphalgorithm.graphSplitting.GraphSplit;
import uk.ac.ncl.semsubgraphalgorithm.semanticDistanceSubgraphExclusion.SemanticDistanceCalculator;
import uk.ac.ncl.semsubgraphalgorithm.semanticGraphPruning.GraphPruning;


/**
 *
 * @author joemullen
 */
public class PerformanceTesting {

    public static void main(String[] args) throws IOException, FileNotFoundException, ClassNotFoundException, Exception {

        //SerialisedGraph ser = new SerialisedGraph();
        //SourceGraph source = ser.useSerialized("graph.data");
        PerformanceTesting pt = new PerformanceTesting();

        pt.semScalabilityTest(3);

        //PerformanceTesting pt2 = new PerformanceTesting();
        //pt.testingGraphPruning();
        //pt2.topologyScalabilityTest(5);
        //pt.createRandomQueries(source, 3);
        //pt.topologyScalabilityTest();
        // pt.splitGraphTest();
    }

    
    private OutPutManagament op = new OutPutManagament();
    
    public void testingGraphPruning() throws IOException, FileNotFoundException, ClassNotFoundException, Exception {
    }

    ///Users/joemullen/Dropbox/VFAlgorithm/RandomGraphs
    public void semScalabilityTest(int subsPerSize) throws FileNotFoundException, IOException, ClassNotFoundException, Exception {

        SerialisedGraph ser = new SerialisedGraph();
        SourceGraph source = ser.useSerialized("graph.data");

        ArrayList<Integer> sizes = new ArrayList<Integer>();
        sizes.add(1000);
        sizes.add(5000);
        sizes.add(10000);
        sizes.add(20000);
        sizes.add(30000);
        sizes.add(40000);
        sizes.add(50000);
        sizes.add(60000);
        sizes.add(70000);
        sizes.add(80000);
        sizes.add(90000);
        sizes.add(100000);

        ArrayList<ArrayList<QueryGraph>> allSubs = createRandomQueries(source, subsPerSize);

        //RandomGraph top = new RandomGraph();
        SemanticDistanceCalculator sdc = new SemanticDistanceCalculator();


        BufferedWriter log = new BufferedWriter(new FileWriter(op.getScalabilityName() + "semLOG.txt"));
        BufferedWriter mcfirst = new BufferedWriter(new FileWriter(op.getScalabilityName() + "mostConnectedFirstNodeSD1.txt"));
        BufferedWriter comp = new BufferedWriter(new FileWriter(op.getScalabilityName() + "compoundFirstNodeSD1.txt"));
        BufferedWriter cWComp = new BufferedWriter(new FileWriter(op.getScalabilityName() + "compoundFirstNodeSD1ClosedWorld.txt"));
        BufferedWriter sdcCwComp = new BufferedWriter(new FileWriter(op.getScalabilityName() + "ClosedWorldSDC.txt"));
        BufferedWriter pruneSearch = new BufferedWriter(new FileWriter(op.getScalabilityName() + "ClosedWorldSDCGraphPrune.txt"));
        BufferedWriter pruneScale = new BufferedWriter(new FileWriter(op.getScalabilityName() + "pruning.txt"));
        BufferedWriter randomGraph = new BufferedWriter(new FileWriter(op.getScalabilityName() + "randomGraphProduction.txt"));
        BufferedWriter splitsearch = new BufferedWriter(new FileWriter(op.getScalabilityName() + "splitSearch.txt"));
        BufferedWriter splitsearchWithPrune = new BufferedWriter(new FileWriter(op.getScalabilityName() + "splitSearchwithPrune.txt"));


        int size2 = 0;




        for (Integer siz : sizes) {
            int count = 0;

            log.append("***********************************" + "\n");
            log.append("**** NEW GRAPH SIZE  " + siz + "\n");
            log.append("***********************************" + "\n");
            log.append("\n" + siz + "\n");
            mcfirst.append("\n" + siz + "\n");
            comp.append("\n" + siz + "\n");
            cWComp.append("\n" + siz + "\n");
            sdcCwComp.append("\n" + siz + "\n");
            pruneSearch.append("\n" + siz + "\n");
            splitsearch.append("\n" + siz + "\n");
            splitsearchWithPrune.append("\n" + siz + "\n");
            pruneScale.append("\n" + siz + "\n");

            RandomGraph sem = new RandomGraph(source, siz, 1);
            SourceGraph ran = sem.getRandomSourceGraph();
            randomGraph.append(sem.getSummary() + "\n");

            int count1 = 0;
            for (ArrayList<QueryGraph> queries : allSubs) {
                for (QueryGraph q : queries) {

                    size2 = q.getSub().vertexSet().size();


                    if (count1 % subsPerSize == 0) {
                        log.append("\n");
                        mcfirst.append("\n");
                        comp.append("\n");
                        cWComp.append("\n");
                        sdcCwComp.append("\n");
                        pruneSearch.append("\n");
                        splitsearch.append("\n");
                        splitsearchWithPrune.append("\n");


                    }
                    count1++;

                    // for (QueryGraph q : queries) {
                    System.out.println("");
                    System.out.println("***********************************");
                    System.out.println("**** NEW SUB -size " + size2);
                    System.out.println("***********************************");
                    System.out.println("");



                    if (count % subsPerSize == 0) {
                        log.append("\n");
                        mcfirst.append("\n");
                        comp.append("\n");
                        cWComp.append("\n");
                        sdcCwComp.append("\n");
                        pruneSearch.append("\n");
                        splitsearch.append("\n");

                    }

                    log.append("***********************************" + "\n");
                    log.append("**** NEW SUB" + "\n");
                    log.append("***********************************" + "\n");

                    // SourceGraph duplicate = (SourceGraph) ran.clone();


//                    if (q.getSub().vertexSet().size() < 5) {
//                        //not starting on compound BUT most connected CC not using SDC, semantic depth of1
//                        SemSearch sem10 = new SemSearch(ran, q, null, null, 1, "none", false, 0.8, false, false);
//                        sem10.completeSearch();
//                        mcfirst.append(sem10.getTime() + "\t");
//                        log.append(sem10.getSummary());
//                        sem10 = null;
//
//
//                        //starting on compound not using SDC semantic depth of 1
//                        SemSearch sem4 = new SemSearch(ran, q, 1, "Compound", false, 0.8, false, false);
//                        sem4.completeSearch();
//                        comp.append(sem4.getTime() + "\t");
//                        log.append(sem4.getSummary());
//                        sem4 = null;
//                        //   }
//
//
//                        //this is taking a closed world assumption throughout search and starting on compound
//                        SemSearch sem9 = new SemSearch(ran, q, 1, "Compound", false, 0.8, true, false);
//                        sem9.completeSearch();
//                        cWComp.append(sem9.getTime() + "\t");
//                        log.append(sem9.getSummary());
//                        sem9 = null;
//                    }


                    //this is taking a closed world assumption throughout search and starting on compound and using SDC
                    SemSearch sem11 = new SemSearch(ran, q, null, null, 8, "Compound", true, 0.8, true, false, true);
                
                    sem11.completeSearch();
                    log.append(sem11.getSummary());
                    sdcCwComp.append(sem11.getTime() + "\t");
                    sem11 = null;

                    if (q.getSub().vertexSet().size() > 4) {
                        //do the split search
                        GraphSplit split = new GraphSplit(q, false);
                        GraphSplit_INSTANCE s = split.getSplit();
                        RunSplit spl = new RunSplit(ran, s,  7, "none", true, 0.8, true, true, true);
                        spl.run();
                        splitsearch.append(spl.getSearchTime() + "|" + spl.getMappingTime() + "\t");
                        spl = null;
                    }

                    //graph pruning step
                    GraphPruning gp4 = new GraphPruning(ran, q, 0.8);
                    SemSearch sem8 = new SemSearch(gp4.getPrunedSource(), q, null, null, 8, "Compound", true, 0.8, true, false, true);
                    sem8.completeSearch();
                    pruneSearch.append(sem8.getTime() + "\t");
                    pruneScale.append(gp4.getTime() + ":::" + gp4.getSummary() + "\n");
                    sem8 = null;



                    //splitsearch using the pruned graph
                    if (q.getSub().vertexSet().size() > 4) {
                        //do the split search
                        GraphSplit split = new GraphSplit(q, false);
                        GraphSplit_INSTANCE s = split.getSplit();
                        RunSplit spl = new RunSplit(gp4.getPrunedSource(),s, 7, "none", true, 0.8, true, true, true);
                        spl.run();
                        splitsearchWithPrune.append(spl.getSearchTime() + " | " + spl.getMappingTime() + "\t");
                        spl = null;
                        gp4 = null;
                    }


                    count++;
                }



                //  }


            }
        }

        log.close();
        mcfirst.close();
        comp.close();
        cWComp.close();
        sdcCwComp.close();
        pruneScale.close();
        pruneSearch.close();
        randomGraph.close();
        splitsearch.close();
        splitsearchWithPrune.close();


    }

    public ArrayList<ArrayList<QueryGraph>> createRandomQueries(SourceGraph source, int subsPerSize) throws IOException {

        BufferedWriter querylog = new BufferedWriter(new FileWriter(op.getScalabilityName()+"queryLOG.txt"));

        //   RandomQuery two = new RandomQuery(source.getAverageConnectivityOfEachConceptClass(), 2, "Compound");


        ArrayList<ArrayList<QueryGraph>> allSubs = new ArrayList<ArrayList<QueryGraph>>();

        //   ArrayList<QueryGraph> twoList = new ArrayList<QueryGraph>();

        ArrayList<QueryGraph> threeList = new ArrayList<QueryGraph>();

        ArrayList<QueryGraph> fourList = new ArrayList<QueryGraph>();

        ArrayList<QueryGraph> fiveList = new ArrayList<QueryGraph>();

        ArrayList<QueryGraph> sixList = new ArrayList<QueryGraph>();

        int localSubs = subsPerSize;

        ArrayList<QueryGraph> sub = null;
        for (int g = 6; g < 7; g++) {
//            if (g == 2) {
//                sub = twoList;
//            }
            if (g == 3) {
                sub = threeList;
            }
            if (g == 4) {
                sub = fourList;
            }
            if (g == 5) {
                sub = fiveList;
            }
            if (g == 6) {
                sub = sixList;
            }

            for (int h = 0; h < localSubs; h++) {
                RandomQuery thr = new RandomQuery(source, g, "Compound");
                querylog.append(thr.getSummary());
                sub.add(thr.getRanQuery());
            }
        }

        // allSubs.add(twoList);
        allSubs.add(threeList);
        allSubs.add(fourList);
        allSubs.add(fiveList);
        allSubs.add(sixList);

        System.out.println("[INFO] Finished: " + allSubs.toString());




        return allSubs;

    }

    public void topologyScalabilityTest(int numberOfSubPerSize) throws IOException, FileNotFoundException, ClassNotFoundException {

        BufferedWriter log = new BufferedWriter(new FileWriter(op.getScalabilityName()+"TOPlog.txt"));
        BufferedWriter topp = new BufferedWriter(new FileWriter(op.getScalabilityName()+"topology.txt"));

        SerialisedGraph ser = new SerialisedGraph();
        SourceGraph source = ser.useSerialized("graph.data");

        ArrayList<Integer> sizes = new ArrayList<Integer>();
        sizes.add(500);
        sizes.add(1000);
        sizes.add(1500);
        sizes.add(2000);
        sizes.add(2500);
        sizes.add(3000);


        ArrayList<ArrayList<QueryGraph>> allSubs = createRandomQueries(source, numberOfSubPerSize);


        //RandomGraph top = new RandomGraph();
        SemanticDistanceCalculator sdc = new SemanticDistanceCalculator();


        //no graph pruning using SDC//
        for (Integer siz : sizes) {
            int count = 0;

            log.append("\n" + siz + "\n");
            topp.append("\n" + siz + "\n");

            RandomGraph sem = new RandomGraph(source, siz, 1);
            SourceGraph ran = sem.getRandomSourceGraph();


            for (ArrayList<QueryGraph> queries : allSubs) {

                for (QueryGraph q : queries) {



                    if (count % numberOfSubPerSize == 0) {
                        log.append("\n");
                        topp.append("\n");


                    }



                    //purely topological
                    SemSearch sem12 = new SemSearch(ran, q, null, null, 0, "none", false, 0.8, false, false, true);                                                
                    sem12.completeSearch();
                    topp.append(sem12.getTime() + "\t");
                    log.append(sem12.getSummary());
                    sem12 = null;

                    count++;
                }



            }



        }

        log.close();
        topp.close();


    }

    public void splitGraphTest() throws FileNotFoundException, IOException, ClassNotFoundException {
//        SerialisedGraph ser = new SerialisedGraph();
//        SourceGraph source = ser.useSerialized("/Users/joemullen/Dropbox/SemSubSearch/semsubsearch/graph.data");
//        QueryGraph q = new QueryGraph(SemanticSub.SIXNODE);
//        // q.largeMotif();
//        GraphSplit split = new GraphSplit(q);
//        RandomGraph rp = new RandomGraph(source.getGraphNodeSize(), source.getAverageConnectivityOfEachConceptClass(), 1000, 0.5);
//        rp.createSemanticGraph();
//        TopMultipleRun tm = new TopMultipleRun(split, rp.getRandomGraph(), q);
//        TopSearch ts = new TopSearch(q.getSub(), rp.getRandomGraph());
//        ts.completeSearch(1000, "no");
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.testing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import uk.ac.ncl.semsubgraphalgorithm.OutPutManagament;
import uk.ac.ncl.semsubgraphalgorithm.QueryGraph;
import uk.ac.ncl.semsubgraphalgorithm.RandomGraph;
import uk.ac.ncl.semsubgraphalgorithm.SemSearch;
import uk.ac.ncl.semsubgraphalgorithm.SerialisedGraph;
import uk.ac.ncl.semsubgraphalgorithm.SourceGraph;
import uk.ac.ncl.semsubgraphalgorithm.semanticGraphPruning.GraphPruning;
import uk.ac.ncl.semsubgraphalgorithm.spikingGraph.SpikeSourceGraph;


/**
 *
 * @author joemullen
 */
public class PruneVNonPruneANDTruevFalse {

    private double[] thresholds = new double[]{0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0};
    private FileWriter fstreamTRUE;
    private BufferedWriter bwTRUE;
    private FileWriter fstreamFALSE;
    private BufferedWriter bwFALSE;
    private String filenameTRUE;
    private String filenameFALSE;
    private OutPutManagament op; 
    
    public static void main(String args[]) throws IOException, FileNotFoundException, ClassNotFoundException, Exception {

        PruneVNonPruneANDTruevFalse st = new PruneVNonPruneANDTruevFalse();
        System.gc();

    }

    public PruneVNonPruneANDTruevFalse() throws IOException, FileNotFoundException, ClassNotFoundException, Exception {

        this.op = new OutPutManagament();
        this.filenameTRUE = op.getResultsName()+"pruneVnonePruneTRUE.txt";
        this.filenameFALSE = op.getResultsName()+ "pruneVnonePruneFALSE.txt";
        runWithDifferentSub(5);
        bwTRUE.close();
        bwFALSE.close();
        parseRE();


    }

    public void runWithDifferentSub(int replicate) throws IOException, FileNotFoundException, ClassNotFoundException, Exception {

        SerialisedGraph ser = new SerialisedGraph();
        SourceGraph source = ser.useSerialized("graph.data");

        Set<QueryGraph> subs = new HashSet<QueryGraph>();
        QueryGraph three = new QueryGraph(QueryGraph.SemanticSub.SIMCOMPSSMALL);
        subs.add(three);
        QueryGraph four = new QueryGraph(QueryGraph.SemanticSub.CTCT);
        subs.add(four);
        QueryGraph five = new QueryGraph(QueryGraph.SemanticSub.FIVENODE);
        subs.add(five);
        QueryGraph six = new QueryGraph(QueryGraph.SemanticSub.SIXNODE);
        subs.add(six);

        //create a random graph of size 1000 

        this.fstreamTRUE = new FileWriter(filenameTRUE);
        this.bwTRUE = new BufferedWriter(fstreamTRUE);

        this.fstreamFALSE = new FileWriter(filenameFALSE);
        this.bwFALSE = new BufferedWriter(fstreamFALSE);

        for (int i = 1; i <= replicate; i++) {
            for (QueryGraph sub : subs) {
                singleSubRun(sub, source, i);
            }
        }
    }

    public void singleSubRun(QueryGraph q, SourceGraph source, int replicate) throws IOException, FileNotFoundException, ClassNotFoundException, Exception {

        RandomGraph ran = new RandomGraph(source, 10000, 1);
        SourceGraph ranSou = ran.getRandomSourceGraph();
        SpikeSourceGraph g = new SpikeSourceGraph(ranSou, q, 100, false);
        SourceGraph ranSource = g.getSpikedGraph();

        bwTRUE.append(q.getMotifName() + "REPLICATE_" + replicate + "\n");
        bwFALSE.append(q.getMotifName() + "REPLICATE_" + replicate + "\n");

        //none-pruned

        for (int i = 0; i < thresholds.length; i++) {
            //do a semantic search for the spiked subgraphs using the thresholds

            SemSearch Search = new SemSearch(ranSource, q, null, null, 8,  "Compound", true, thresholds[i], true, false, true);
            Search.completeSearch();
            int nsMatchesTRUE = Search.numberOfMathces();
            bwTRUE.append(nsMatchesTRUE + "\t");


            SemSearch SearchFALSE = new SemSearch(ranSource, q, null, null,  6, "Compound", true, thresholds[i], true, false, false);
            SearchFALSE.completeSearch();
            int nsMatchesFALSE = SearchFALSE.numberOfMathces();
            bwFALSE.append(nsMatchesFALSE + "\t");


        }

        bwTRUE.append("\n");
        bwFALSE.append("\n");

        //pruned


        for (int i = 0; i < thresholds.length; i++) {

            GraphPruning gp = new GraphPruning(ranSource, q, thresholds[i]);

            SemSearch prunedSearchTRUE = new SemSearch(gp.getPrunedSource(), q, null, null, 6, "Compound", true, thresholds[i], true, false, true);
            prunedSearchTRUE.completeSearch();
            int sMatchesTRUE = prunedSearchTRUE.numberOfMathces();
            bwTRUE.append(sMatchesTRUE + "\t");

            SemSearch prunedSearchFALSE = new SemSearch(gp.getPrunedSource(), q, null, null, 8, "Compound", true, thresholds[i], true, false, false);
            prunedSearchFALSE.completeSearch();
            int sMatchesFALSE = prunedSearchFALSE.numberOfMathces();
            bwFALSE.append(sMatchesFALSE + "\t");



        }

        bwTRUE.append("\n");
        bwFALSE.append("\n");

    }

    public void parseRE() throws FileNotFoundException, IOException {
        BufferedReader brTRUE = new BufferedReader(new FileReader(filenameTRUE));
        BufferedReader brFALSE = new BufferedReader(new FileReader(filenameFALSE));
        
        parseResults(brTRUE, true);
        parseResults(brFALSE, false);

    }

    public void parseResults(BufferedReader br, boolean cum) throws FileNotFoundException, IOException {

        //BufferedReader brTRUE = new BufferedReader(new FileReader(filenameTRUE));
        String line;

        int[] search = new int[thresholds.length];
        int[] prunedSearch = new int[thresholds.length];

        int count = 0;
        int replicates = 0;

        while ((line = br.readLine()) != null) {
            count++;
            if (count % 3 == 0) {
                count = 0;
                replicates++;
            }
            if (count == 1) {
                //nothing
            }
            if (count == 2) {
                String[] split = line.split("\t");
                for (int i = 0; i < split.length; i++) {
                    search[i] = Integer.parseInt(split[i]) + search[i];
                }
            }
            if (count == 0) {
                String[] split = line.split("\t");
                for (int i = 0; i < split.length; i++) {
                    prunedSearch[i] = Integer.parseInt(split[i]) + prunedSearch[i];
                }
            }
        }


        for (int i = 0; i < search.length; i++) {
            search[i] = (search[i] / replicates);
            prunedSearch[i] = (prunedSearch[i] / replicates);

        }

        br.close();


        if (cum == false) {

            //Keep the existing content and append the new content in the end of the file.
            fstreamFALSE= new FileWriter(filenameFALSE, true);
            bwFALSE = new BufferedWriter(fstreamFALSE);

            bwFALSE.append("\n");
            bwFALSE.append("[INFO] PLOT:" + "\n");
            String thres = Arrays.toString(thresholds);
            bwFALSE.append("Thresholds->c(" + thres.substring(1, thres.length() - 1) + ")" + "\n");
            String pruned = Arrays.toString(prunedSearch);
            bwFALSE.append("Pruned->c(" + pruned.substring(1, pruned.length() - 1) + ")" + "\n");
            String nonspiked = Arrays.toString(search);
            bwFALSE.append("NonePruned->c(" + nonspiked.substring(1, nonspiked.length() - 1) + ")" + "\n");

            bwFALSE.close();
        }


        if (cum == true) {
            //Keep the existing content and append the new content in the end of the file.
            fstreamTRUE = new FileWriter(filenameTRUE, true);
            bwTRUE = new BufferedWriter(fstreamTRUE);

            bwTRUE.append("\n");
            bwTRUE.append("[INFO] PLOT:" + "\n");
            String thres = Arrays.toString(thresholds);
            bwTRUE.append("Thresholds->c(" + thres.substring(1, thres.length() - 1) + ")" + "\n");
            String pruned = Arrays.toString(prunedSearch);
            bwTRUE.append("Pruned->c(" + pruned.substring(1, pruned.length() - 1) + ")" + "\n");
            String nonspiked = Arrays.toString(search);
            bwTRUE.append("NonePruned->c(" + nonspiked.substring(1, nonspiked.length() - 1) + ")" + "\n");

            bwTRUE.close();
        }


    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.testing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import uk.ac.ncl.semsubgraphalgorithm.OutPutManagament;
import uk.ac.ncl.semsubgraphalgorithm.QueryGraph;
import uk.ac.ncl.semsubgraphalgorithm.RandomGraph;
import uk.ac.ncl.semsubgraphalgorithm.RandomQuery;
import uk.ac.ncl.semsubgraphalgorithm.SemSearch;
import uk.ac.ncl.semsubgraphalgorithm.SerialisedGraph;
import uk.ac.ncl.semsubgraphalgorithm.SourceGraph;
import uk.ac.ncl.semsubgraphalgorithm.graphSplitting.GraphSplit;
import uk.ac.ncl.semsubgraphalgorithm.graphSplitting.RunSplit;

/**
 *
 * @author joemullen
 */
public class GraphSplitPerformance {

    public static void main(String[] args) throws IOException, FileNotFoundException, ClassNotFoundException, Exception {

        OutPutManagament op = new OutPutManagament();
        String dir = op.getTempDirectoryName();
        op.emptyTempFolder();
//
        GraphSplitPerformance gsp = new GraphSplitPerformance();
//        int[] graphSizes = new int[]{1000, 2000};//, 3000, 4000, 5000, 10000, 20000, 30000, 40000, 50000, 100000};
//        String[] repeats = new String[]{"first"};//, "second", "third", "fourth", "fifth", "sixth", "seventh", "eigth", "ninth", "tenth"};
//
//        for (int p = 0; p < repeats.length; p++) {
//            for (int i = 0; i < graphSizes.length; i++) {
//                gsp.indiRun(graphSizes[i], repeats[p], dir);
//
//            }
//
//        }


        gsp.toPlot(dir);
    }

    /**
     * For each file in the directory we want to read the time taken for a
     * search and return this as an array of search times to be plotted
     *
     * @param dir
     */
    public void toPlot(String dir) throws FileNotFoundException {
        Map<Integer, Set<Double>> sixNorm = new HashMap<Integer, Set<Double>>();
        Map<Integer, Set<Double>> sixSplit = new HashMap<Integer, Set<Double>>();
        Map<Integer, Set<Double>> sevenNorm = new HashMap<Integer, Set<Double>>();
        Map<Integer, Set<Double>> sevenSplit = new HashMap<Integer, Set<Double>>();
        File folder = new File(dir);
        if (folder.isDirectory()) {
            for (File child : folder.listFiles()) {
                if (!child.isHidden() == true) {
                    String name = child.getName();
                    String[] nameSplit = name.split("_");
                    int graphSize = Integer.parseInt(nameSplit[0]);
                    int subSize = Integer.parseInt(nameSplit[2].substring(0, 1));
                    String thisLine;
                    try {
                        BufferedReader br = new BufferedReader(new FileReader(child));
                        while ((thisLine = br.readLine()) != null) {
                            if (thisLine.startsWith("SEM:")) {
                                String[] split = thisLine.split("\\s");
                                if (subSize == 6) {
                                    sixNorm = updateMap(sixNorm, graphSize, Double.parseDouble(split[1]));
                                } else {
                                    sevenNorm = updateMap(sevenNorm, graphSize, Double.parseDouble(split[1]));
                                }


                            }
                            if (thisLine.startsWith("SPL:")) {
                                String[] split = thisLine.split("\\s");
                                if (subSize == 6) {
                                    sixSplit = updateMap(sixSplit, graphSize, Double.parseDouble(split[1]));
                                } else {
                                    sevenSplit = updateMap(sevenSplit, graphSize, Double.parseDouble(split[1]));
                                }

                            }


                            child.delete();
                        }

                    } catch (IOException e) {
                        System.err.println("Error: " + e);
                    }
                }
            }

        }

        Set<Integer> graphSizes = sixNorm.keySet();
        List<Integer> gs = new ArrayList<Integer>();
        gs.addAll(graphSizes);

        Collections.sort(gs);


        printOut(gs, sixNorm);
        printOut(gs, sixSplit);
        printOut(gs, sevenNorm);
        printOut(gs, sevenSplit);




    }

    public void printOut(List<Integer> gs, Map<Integer, Set<Double>> data) {


        for (int graph : gs) {
            Set<Double> local = data.get(graph);
            Double total = 0.0;
            for (Double time : local) {
                total += time;
            }

            System.out.print((total / local.size()) + ",");

        }

        System.out.println("");

    }

    /**
     * Add a new time to the map using the graph size of the search as the key
     *
     * @param map
     * @param graph
     * @param time
     * @return
     */
    public Map<Integer, Set<Double>> updateMap(Map<Integer, Set<Double>> map, int graph, double time) {

        if (map.containsKey(graph)) {
            Set<Double> local = map.get(graph);
            local.add(time);
            map.put(graph, local);
        } else {
            Set<Double> local = new HashSet<Double>();
            local.add(time);
            map.put(graph, local);
        }

        return map;

    }

    public void indiRun(int size, String id, String directory) throws IOException, FileNotFoundException, ClassNotFoundException, Exception {

        BufferedWriter sixlog = null;
        BufferedWriter sevenlog = null;

        String sys = System.getProperty("os.name");
        System.out.println(sys);
        if (sys.startsWith("Wind")) {
            sixlog = new BufferedWriter(new FileWriter(directory + "\\" + size + "_" + id + "_6NODE.txt"));
            sevenlog = new BufferedWriter(new FileWriter(directory + "\\" + size + "_" + id + "_7NODE.txt"));

        }
        if (sys.startsWith("Mac") || sys.startsWith("Ubun")) {
            sixlog = new BufferedWriter(new FileWriter(directory + "/" + size + "_" + id + "_6NODE.txt"));
            sevenlog = new BufferedWriter(new FileWriter(directory + "/" + size + "_" + id + "_7NODE.txt"));
        }



        SerialisedGraph ser = new SerialisedGraph();
        SourceGraph source = ser.useSerialized("graph.data");
        RandomGraph ranTar1 = new RandomGraph(source, size, 1.0);
        SourceGraph ranTar = ranTar1.getRandomSourceGraph();

        RandomQuery sixRan = new RandomQuery(source, 6, "Compound");
        RandomQuery sevenRan = new RandomQuery(source, 7, "Compound");
        QueryGraph six = sixRan.getRanQuery();
        QueryGraph seven = sevenRan.getRanQuery();

        //split query
        GraphSplit gSix = new GraphSplit(six, false);
        GraphSplit gSeven = new GraphSplit(seven, false);

        //normal run
        System.out.println("------------------------------NORMAL RUN 6");
        SemSearch sem6 = new SemSearch(ranTar, six, null, null, 8, "Compound", true, 0.0, true, false, true);

        sem6.completeSearch();
        sixlog.append("SEM: " + sem6.getTime() + "\n");

        //split run
        System.out.println("------------------------------SPLIT RUN 6");
        RunSplit rs6 = new RunSplit(ranTar, gSix.getSplit(), 7, "none", true, 0.0, true, false, true);
        rs6.run();
        sixlog.append("SPL: " + rs6.getSearchTime() + "\n");

        sixlog.close();

        //normal run
        System.out.println("------------------------------NORMAL RUN 7");
        SemSearch sem7 = new SemSearch(ranTar, seven, null, null, 8, "Compound", true, 0.8, true, false, true);
        sem7.completeSearch();
        sevenlog.append("SEM: " + sem7.getTime() + "\n");

        //split run
        System.out.println("------------------------------SPLIT RUN 7");
        RunSplit rs7 = new RunSplit(ranTar, gSeven.getSplit(), 7, "none", true, 0.8, true, false, true);
        rs7.run();
        sevenlog.append("SPL: " + rs7.getSearchTime() + "\n");


        sevenlog.close();

    }
}

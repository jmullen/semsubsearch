/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.testing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import uk.ac.ncl.semsubgraphalgorithm.OutPutManagament;
import uk.ac.ncl.semsubgraphalgorithm.QueryGraph;
import uk.ac.ncl.semsubgraphalgorithm.RandomGraph;
import uk.ac.ncl.semsubgraphalgorithm.RandomQuery;
import uk.ac.ncl.semsubgraphalgorithm.SemSearch;
import uk.ac.ncl.semsubgraphalgorithm.SerialisedGraph;
import uk.ac.ncl.semsubgraphalgorithm.SourceGraph;
import uk.ac.ncl.semsubgraphalgorithm.spikingGraph.SpikeSourceGraph;


/**
 *
 * @author joemullen
 */
public class QueryGraphNodeSize {

    private FileWriter fstreamTRUE;
    private BufferedWriter bwTRUE;
    private String filenameTRUE;
    private FileWriter fstreamFALSE;
    private BufferedWriter bwFALSE;
    private String filenameFALSE;
    private int replicates;
    private int[] subSizes;
    private OutPutManagament op;

    public static void main(String args[]) throws IOException, FileNotFoundException, ClassNotFoundException, Exception {

        QueryGraphNodeSize st = new QueryGraphNodeSize(5);
        System.gc();

    }

    public QueryGraphNodeSize(int replicate) throws IOException, FileNotFoundException, ClassNotFoundException, Exception {

        this.op = new OutPutManagament();
        this.filenameTRUE = op.getResultsName()+"QueryGraphNodeSize_TRUE.txt";
        this.filenameFALSE = op.getResultsName()+"QueryGraphNodeSize_FALSE.txt";
        this.replicates = replicate;
        this.subSizes = new int[]{2, 3, 4, 5, 6};
        runWithDifferentSub();
        bwTRUE.close();
        bwFALSE.close();
        individParseResults();

    }

    public void runWithDifferentSub() throws IOException, FileNotFoundException, ClassNotFoundException, Exception {

        SerialisedGraph ser = new SerialisedGraph();
        SourceGraph source = ser.useSerialized("graph.data");

        Set<QueryGraph> subs2 = new HashSet<QueryGraph>();
        Set<QueryGraph> subs3 = new HashSet<QueryGraph>();
        Set<QueryGraph> subs4 = new HashSet<QueryGraph>();
        Set<QueryGraph> subs5 = new HashSet<QueryGraph>();
        Set<QueryGraph> subs6 = new HashSet<QueryGraph>();

        for (int i = 0; i < replicates; i++) {

            RandomQuery rq2 = new RandomQuery(source, 2, "Compound");
            subs2.add(rq2.getRanQuery());
            RandomQuery rq3 = new RandomQuery(source, 3, "Compound");
            subs3.add(rq3.getRanQuery());
            RandomQuery rq4 = new RandomQuery(source, 4, "Compound");
            subs4.add(rq4.getRanQuery());
            RandomQuery rq5 = new RandomQuery(source, 5, "Compound");
            subs5.add(rq5.getRanQuery());
            RandomQuery rq6 = new RandomQuery(source, 6, "Compound");
            subs6.add(rq6.getRanQuery());

        }

        Set<Set<QueryGraph>> allSubs = new HashSet<Set<QueryGraph>>();
        allSubs.add(subs2);
        allSubs.add(subs3);
        allSubs.add(subs4);
        allSubs.add(subs5);
        allSubs.add(subs6);


        this.fstreamTRUE = new FileWriter(filenameTRUE);
        this.bwTRUE = new BufferedWriter(fstreamTRUE);
        this.fstreamFALSE = new FileWriter(filenameFALSE);
        this.bwFALSE = new BufferedWriter(fstreamFALSE);


        RandomGraph ran = new RandomGraph(source, 1000, 1);
        SourceGraph ranSource = ran.getRandomSourceGraph();
        int count = 0;
        for (Set<QueryGraph> subs : allSubs) {
            count = 0;
            for (QueryGraph q : subs) {

                if (count == 0) {
                    bwFALSE.append(">>" + q.getSub().vertexSet().size() + "\n");
                    bwTRUE.append(">>" + q.getSub().vertexSet().size() + "\n");
                }
                count++;
                SemSearch noneSpikedSearchFALSE = new SemSearch(ranSource, q, null, null, 8, "Compound", true, 0.8, true, false, false);                                                             
                noneSpikedSearchFALSE.completeSearch();
                int nsMatchesFALSE = noneSpikedSearchFALSE.numberOfMathces();
                bwFALSE.append(nsMatchesFALSE + "\t");

                SemSearch noneSpikedSearchTRUE = new SemSearch(ranSource, q, null, null, 8, "Compound", true, 0.8, true, false, true);
                noneSpikedSearchTRUE.completeSearch();
                int nsMatchesTRUE = noneSpikedSearchTRUE.numberOfMathces();
                bwTRUE.append(nsMatchesTRUE + "\t");


                SpikeSourceGraph g = new SpikeSourceGraph(ranSource, q, 100, false);

                SemSearch spikedSearchFALSE = new SemSearch(g.getSpikedGraph(), q, null, null, 8, "Compound", true, 0.8, true, false, false);
                spikedSearchFALSE.completeSearch();
                int sMatchesFALSE = spikedSearchFALSE.numberOfMathces();
                bwFALSE.append(sMatchesFALSE + "\t" + "\n");

                SemSearch spikedSearchTRUE = new SemSearch(g.getSpikedGraph(), q, null, null, 8, "Compound", true, 0.8, true, false, true);
                spikedSearchTRUE.completeSearch();
                int sMatchesTRUE = spikedSearchTRUE.numberOfMathces();
                bwTRUE.append(sMatchesTRUE + "\t" + "\n");

            }
        }

    }

    public void individParseResults() throws FileNotFoundException, IOException {

        BufferedReader brTRUE = new BufferedReader(new FileReader(filenameTRUE));
        parseResults(brTRUE, true);
        BufferedReader brFALSE = new BufferedReader(new FileReader(filenameFALSE));
        parseResults(brFALSE, false);

    }

    public void parseResults(BufferedReader br, boolean type) throws FileNotFoundException, IOException {

        String line;


        int[] resultsSpiked = new int[subSizes.length];
        int[] resultsNoneSpiked = new int[subSizes.length];

        int count = 0;

        while ((line = br.readLine()) != null) {
            if (line.startsWith(">>")) {
                String line2 = line.trim();
                System.out.println(line2);
                System.out.println(line2.substring(2).trim());
                count = Integer.parseInt(line2.substring(2).trim());
            } else {
                String[] split = line.split("\t");
                System.out.println("SS0: "+subSizes[0]);
                System.out.println("count: "+count);
                resultsNoneSpiked[count - subSizes[0]] = Integer.parseInt(split[0]) + resultsNoneSpiked[count - subSizes[0]];
                resultsSpiked[count - subSizes[0]] = Integer.parseInt(split[1]) + resultsSpiked[count - subSizes[0]];

               // count++;
            }
        }

        for (int i = 0; i < resultsSpiked.length; i++) {
            resultsSpiked[i] = (resultsSpiked[i] / replicates);
            resultsNoneSpiked[i] = (resultsNoneSpiked[i] / replicates);

        }

        br.close();


        if (type == true) {

            //Keep the existing content and append the new content in the end of the file.
            fstreamTRUE = new FileWriter(filenameTRUE, true);
            bwTRUE = new BufferedWriter(fstreamTRUE);

            bwTRUE.append("\n");
            bwTRUE.append("[INFO] PLOT:" + "\n");
            String conn = Arrays.toString(subSizes);
            bwTRUE.append("Sub_sizes->c(" + conn.substring(1, conn.length() - 1) + ")" + "\n");
            String spikedString = Arrays.toString(resultsSpiked);
            bwTRUE.append("Spiked->c(" + spikedString.substring(1, spikedString.length() - 1) + ")" + "\n");
            String nonspiked = Arrays.toString(resultsNoneSpiked);
            bwTRUE.append("Not_Spiked->c(" + nonspiked.substring(1, nonspiked.length() - 1) + ")" + "\n");

            bwTRUE.close();
        }

        if (type == false) {

            //Keep the existing content and append the new content in the end of the file.
            fstreamFALSE = new FileWriter(filenameFALSE, true);
            bwFALSE = new BufferedWriter(fstreamFALSE);

            bwFALSE.append("\n");
            bwFALSE.append("[INFO] PLOT:" + "\n");
            String conn = Arrays.toString(subSizes);
            bwFALSE.append("Sub_Sizes->c(" + conn.substring(1, conn.length() - 1) + ")" + "\n");
            String spikedString = Arrays.toString(resultsSpiked);
            bwFALSE.append("Spiked->c(" + spikedString.substring(1, spikedString.length() - 1) + ")" + "\n");
            String nonspiked = Arrays.toString(resultsNoneSpiked);
            bwFALSE.append("Not_Spiked->c(" + nonspiked.substring(1, nonspiked.length() - 1) + ")" + "\n");

            bwFALSE.close();
        }


    }
}

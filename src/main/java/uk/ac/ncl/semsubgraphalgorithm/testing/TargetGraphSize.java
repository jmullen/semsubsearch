/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.testing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import uk.ac.ncl.semsubgraphalgorithm.OutPutManagament;
import uk.ac.ncl.semsubgraphalgorithm.QueryGraph;
import uk.ac.ncl.semsubgraphalgorithm.RandomGraph;
import uk.ac.ncl.semsubgraphalgorithm.SemSearch;
import uk.ac.ncl.semsubgraphalgorithm.SerialisedGraph;
import uk.ac.ncl.semsubgraphalgorithm.SourceGraph;
import uk.ac.ncl.semsubgraphalgorithm.spikingGraph.SpikeSourceGraph;


/**
 *
 * @author joemullen
 */
public class TargetGraphSize {

    private int[] size = new int[]{1000, 5000, 10000, 50000};
    private int replicates;
    private FileWriter fstreamTRUE;
    private BufferedWriter bwTRUE;
    private String filenameTRUE;
    private FileWriter fstreamFALSE;
    private BufferedWriter bwFALSE;
    private String filenameFALSE;
    private int numberOfSubs;
    private OutPutManagament op;

    public static void main(String args[]) throws IOException, FileNotFoundException, ClassNotFoundException, Exception {

        //CalculatingST st = new CalculatingST();
        TargetGraphSize tg = new TargetGraphSize(1);

        System.gc();

    }

    public TargetGraphSize(int replicates) throws IOException, FileNotFoundException, ClassNotFoundException, Exception {

        this.op = new OutPutManagament();
        this.filenameTRUE = op.getResultsName()+ "TargetGraphSizeST_TRUE.txt";
        this.filenameFALSE = op.getResultsName()+ "TargetGraphSizeST_FALSE.txt";
        this.replicates = replicates;
        this.numberOfSubs = 4;
        //runWithDifferentSub();
        //bwTRUE.close();
        //bwFALSE.close();
        individParseResults();


    }

    public void runWithDifferentSub() throws IOException, FileNotFoundException, ClassNotFoundException, Exception {

        SerialisedGraph ser = new SerialisedGraph();
        SourceGraph source = ser.useSerialized("graph.data");

        Set<QueryGraph> subs = new HashSet<QueryGraph>();
        QueryGraph three = new QueryGraph(QueryGraph.SemanticSub.SIMCOMPSSMALL);
        subs.add(three);
        QueryGraph four = new QueryGraph(QueryGraph.SemanticSub.CTCT);
        subs.add(four);
        QueryGraph five = new QueryGraph(QueryGraph.SemanticSub.FIVENODE);
        subs.add(five);
        QueryGraph six = new QueryGraph(QueryGraph.SemanticSub.SIXNODE);
        subs.add(six);

        this.numberOfSubs = subs.size();

        //create a random graph of size 1000 

        this.fstreamTRUE = new FileWriter(filenameTRUE);
        this.bwTRUE = new BufferedWriter(fstreamTRUE);
        this.fstreamFALSE = new FileWriter(filenameFALSE);
        this.bwFALSE = new BufferedWriter(fstreamFALSE);


//        for (int i = 1; i <= replicates; i++) {
//            for (QueryGraph sub : subs) {
//                //spike the graph-DO NOT PLUG IT 
//                run(sub, source, i);
//            }
//        }
        
        
        for (int rep = 0; rep < replicates; rep++) {
            for (int i = 0; i < size.length; i++) {
                RandomGraph con1 = new RandomGraph(source, size[i], 1);
                bwTRUE.append(">>>" + size[i] + "\n");
                bwFALSE.append(">>>" + size[i] + "\n");
                for (QueryGraph sub : subs) {
                    //spike the graph-DO NOT PLUG IT 
                    //do a semantic search for the spiked subgraphs using the thresholds

                    SemSearch noneSpikedSearch = new SemSearch(con1.getRandomSourceGraph(), sub, null, null, 8, "Compound", true, 0.8, true, false, true);
                    noneSpikedSearch.completeSearch();
                    int nsMatches = noneSpikedSearch.numberOfMathces();
                    bwTRUE.append(nsMatches + "\t");

                    SemSearch noneSpikedSearchFALSE = new SemSearch(con1.getRandomSourceGraph(), sub, null, null, 8, "Compound", true, 0.8, true, false, false);
                    noneSpikedSearchFALSE.completeSearch();
                    int nsMatchesFALSE = noneSpikedSearchFALSE.numberOfMathces();
                    bwFALSE.append(nsMatchesFALSE + "\t");

                    //  bw.append("\n");

                    //spike graph
                    SpikeSourceGraph g = new SpikeSourceGraph(con1.getRandomSourceGraph(), sub, 100, false);
                    SemSearch SpikedSearch = new SemSearch(g.getSpikedGraph(), sub, null, null, 8, "Compound", true, 0.8, true, false, true);
                    SpikedSearch.completeSearch();
                    int sMatches = SpikedSearch.numberOfMathces();
                    bwTRUE.append(sMatches + "\t");
                    bwTRUE.append("\n");

                    SemSearch SpikedSearchFALSE = new SemSearch(g.getSpikedGraph(), sub, null, null, 8, "Compound", true, 0.8, true, false, false);
                    SpikedSearchFALSE.completeSearch();
                    int sMatchesFALSE = SpikedSearchFALSE.numberOfMathces();
                    bwFALSE.append(sMatchesFALSE + "\t");
                    bwFALSE.append("\n");

                }
            }
        }
    }

        
//        
//    
//
//    public void run(QueryGraph q, SourceGraph s, int replicate) throws IOException {
//
//        bwTRUE.append(">>>" + q.getMotifName() + "_REPLICATE:" + replicate + "\n");
//        bwFALSE.append(">>>" + q.getMotifName() + "_REPLICATE:" + replicate + "\n");
//
//        for (int i = 0; i < size.length; i++) {
//            //do a semantic search for the spiked subgraphs using the thresholds
//            RandomGraph con1 = new RandomGraph(s, size[i], 1);
//            SemSearch noneSpikedSearchTRUE = new SemSearch(con1.getRandomSourceGraph(), q, 6, "Compound", true, 0.8, true, false, true);
//            noneSpikedSearchTRUE.completeSearch();
//            int nsMatchesTRUE = noneSpikedSearchTRUE.numberOfMathces();
//            bwTRUE.append(nsMatchesTRUE + "\t");
//            
//            
//            SemSearch noneSpikedSearchFALSE = new SemSearch(con1.getRandomSourceGraph(), q, 6, "Compound", true, 0.8, true, false, false);
//            noneSpikedSearchFALSE.completeSearch();
//            int nsMatchesFALSE = noneSpikedSearchFALSE.numberOfMathces();
//            bwFALSE.append(nsMatchesFALSE + "\t");
//
//            //  bw.append("\n");
//
//            //spike graph
//            SpikeSourceGraph g = new SpikeSourceGraph(con1.getRandomSourceGraph(), q, 100, false);
//            SemSearch SpikedSearchTRUE = new SemSearch(g.getSpikedGraph(), q, 6, "Compound", true, 0.8, true, false,true);
//            SpikedSearchTRUE.completeSearch();
//            int sMatches = SpikedSearchTRUE.numberOfMathces();
//            bwTRUE.append(sMatches + "\t");
//            bwTRUE.append("\n");
//            
//            SemSearch SpikedSearchFALSE = new SemSearch(g.getSpikedGraph(), q, 6, "Compound", true, 0.8, true, false,false);
//            SpikedSearchFALSE.completeSearch();
//            int sMatchesFALSE = SpikedSearchFALSE.numberOfMathces();
//            bwFALSE.append(sMatchesFALSE + "\t");
//            bwFALSE.append("\n");
//            
//
//        }
//
//        bwTRUE.append("\n");
//        bwFALSE.append("\n");
//
//    }
    
    
     public void individParseResults() throws FileNotFoundException, IOException {

        BufferedReader brTRUE = new BufferedReader(new FileReader(filenameTRUE));
        parseResults(brTRUE, true);
        BufferedReader brFALSE = new BufferedReader(new FileReader(filenameFALSE));
        parseResults(brFALSE, false);

    }
     
     
     public void parseResults(BufferedReader br, boolean type) throws FileNotFoundException, IOException {
        String line;

        int[] spiked = new int[size.length];
        int[] NotSpiked = new int[size.length];

        int count = -1;

        while ((line = br.readLine()) != null) {
//            if (count % numberOfSubs == 0) {
//                count = 0;
//            }
            if (line.startsWith(">>>") || line.isEmpty()) {
                count ++;
            } else {
                String[] split = line.split("\t");         
                spiked[count] = Integer.parseInt(split[1]) + spiked[count];
                NotSpiked[count] = Integer.parseInt(split[0]) + NotSpiked[count];

            }

        }
        
        System.out.println("SPIKED:"+ Arrays.toString(spiked));
        System.out.println("NonSPIKED:"+ Arrays.toString(NotSpiked));

        for (int i = 0; i < spiked.length; i++) {
            spiked[i] = (spiked[i] / (numberOfSubs * replicates));
            NotSpiked[i] = (NotSpiked[i] / (numberOfSubs * replicates));
        }

        br.close();


        if (type == true) {

            //Keep the existing content and append the new content in the end of the file.
            fstreamTRUE = new FileWriter(filenameTRUE, true);
            bwTRUE = new BufferedWriter(fstreamTRUE);

            bwTRUE.append("\n");
            bwTRUE.append("[INFO] PLOT:" + "\n");
            String conn = Arrays.toString(size);
            bwTRUE.append("Size->c(" + conn.substring(1, conn.length() - 1) + ")" + "\n");
            String spikedString = Arrays.toString(spiked);
            bwTRUE.append("Spiked->c(" + spikedString.substring(1, spikedString.length() - 1) + ")" + "\n");
            String nonspiked = Arrays.toString(NotSpiked);
            bwTRUE.append("Not_Spiked->c(" + nonspiked.substring(1, nonspiked.length() - 1) + ")" + "\n");

            bwTRUE.close();
        }

        if (type == false) {

            //Keep the existing content and append the new content in the end of the file.
            fstreamFALSE = new FileWriter(filenameFALSE, true);
            bwFALSE = new BufferedWriter(fstreamFALSE);

            bwFALSE.append("\n");
            bwFALSE.append("[INFO] PLOT:" + "\n");
            String conn = Arrays.toString(size);
            bwFALSE.append("Size->c(" + conn.substring(1, conn.length() - 1) + ")" + "\n");
            String spikedString = Arrays.toString(spiked);
            bwFALSE.append("Spiked->c(" + spikedString.substring(1, spikedString.length() - 1) + ")" + "\n");
            String nonspiked = Arrays.toString(NotSpiked);
            bwFALSE.append("Not_Spiked->c(" + nonspiked.substring(1, nonspiked.length() - 1) + ")" + "\n");

            bwFALSE.close();
        }


    }

//    public void parseResults(BufferedReader br, boolean type) throws FileNotFoundException, IOException {
//
//        String line;
//
//        int[] spiked = new int[size.length];
//        int[] NotSpiked = new int[size.length];
//
//        int count = 0;
//
//
//        while ((line = br.readLine()) != null) {
//            if (line.startsWith(">>>") || line.isEmpty()) {
//                count = 0;
//            } else {
//                String[] split = line.split("\t");
//                //spiked is the first one
//                spiked[count] = Integer.parseInt(split[1]) + spiked[count];
//                //spiked is the first one
//                NotSpiked[count] = Integer.parseInt(split[0]) + NotSpiked[count];
//                count++;
//            }
//        }
//
//        for (int i = 0; i < spiked.length; i++) {
//            spiked[i] = (spiked[i] / (numberOfSubs * replicates));
//            NotSpiked[i] = (NotSpiked[i] / (numberOfSubs * replicates));
//        }
//
//        br.close();
//
//        //Keep the existing content and append the new content in the end of the file.
//       if (type == true) {
//
//            //Keep the existing content and append the new content in the end of the file.
//            fstreamTRUE = new FileWriter(filenameTRUE, true);
//            bwTRUE = new BufferedWriter(fstreamTRUE);
//
//            bwTRUE.append("\n");
//            bwTRUE.append("[INFO] PLOT:" + "\n");
//            String conn = Arrays.toString(size);
//            bwTRUE.append("Graph_Size->c(" + conn.substring(1, conn.length() - 1) + ")" + "\n");
//            String spikedString = Arrays.toString(spiked);
//            bwTRUE.append("Spiked->c(" + spikedString.substring(1, spikedString.length() - 1) + ")" + "\n");
//            String nonspiked = Arrays.toString(NotSpiked);
//            bwTRUE.append("Not_Spiked->c(" + nonspiked.substring(1, nonspiked.length() - 1) + ")" + "\n");
//
//            bwTRUE.close();
//        }
//
//        if (type == false) {
//
//            //Keep the existing content and append the new content in the end of the file.
//            fstreamFALSE = new FileWriter(filenameFALSE, true);
//            bwFALSE = new BufferedWriter(fstreamFALSE);
//
//            bwFALSE.append("\n");
//            bwFALSE.append("[INFO] PLOT:" + "\n");
//            String conn = Arrays.toString(size);
//            bwFALSE.append("Graph_Size->c(" + conn.substring(1, conn.length() - 1) + ")" + "\n");
//            String spikedString = Arrays.toString(spiked);
//            bwFALSE.append("Spiked->c(" + spikedString.substring(1, spikedString.length() - 1) + ")" + "\n");
//            String nonspiked = Arrays.toString(NotSpiked);
//            bwFALSE.append("Not_Spiked->c(" + nonspiked.substring(1, nonspiked.length() - 1) + ")" + "\n");
//
//            bwFALSE.close();
//        }
//
//
//
//    }
}

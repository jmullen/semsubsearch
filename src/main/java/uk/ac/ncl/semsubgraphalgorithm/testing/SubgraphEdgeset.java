/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.testing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import uk.ac.ncl.semsubgraphalgorithm.OutPutManagament;
import uk.ac.ncl.semsubgraphalgorithm.QueryGraph;
import uk.ac.ncl.semsubgraphalgorithm.RandomGraph;
import uk.ac.ncl.semsubgraphalgorithm.RandomQuery;
import uk.ac.ncl.semsubgraphalgorithm.SemSearch;
import uk.ac.ncl.semsubgraphalgorithm.SerialisedGraph;
import uk.ac.ncl.semsubgraphalgorithm.SourceGraph;


/**
 *
 * @author joemullen
 */
public class SubgraphEdgeset {

    public static void main(String[] args) throws IOException, FileNotFoundException, ClassNotFoundException {

        int repeats = 5;
        SerialisedGraph ser = new SerialisedGraph();
        SourceGraph source = ser.useSerialized("graph.data");
        SubgraphEdgeset search = new SubgraphEdgeset(source, repeats);
        
        SubgraphEdgeset plot  = new SubgraphEdgeset(repeats, new File("Results/SubEdgeset_FALSE.txt"));
        


    }
    private QueryGraph[] qs;
    private int[] tarSize;
    private String filenameTRUE;
    private String filenameFALSE;
    private FileWriter fstreamTRUE;
    private FileWriter fstreamFALSE;
    private BufferedWriter bwtrue;
    private BufferedWriter bwfalse;
    private int repeats;
    private Map<String, Set<String>> trueTimes3;
    private Map<String, Set<String>> trueTimes4;
    private Map<String, Set<String>> trueTimes5;
    private Map<String, Set<String>> trueTimes6;
    private OutPutManagament op;

    //constructorForPlot
    public SubgraphEdgeset(int repeats, File name) {

        this.op = new OutPutManagament();
        this.tarSize = new int[]{10000, 20000, 30000, 40000, 50000, 60000, 70000, 80000, 90000, 100000};
        this.trueTimes3 = new HashMap<String, Set<String>>();
        this.trueTimes4 = new HashMap<String, Set<String>>();
        this.trueTimes5 = new HashMap<String, Set<String>>();
        this.trueTimes6 = new HashMap<String, Set<String>>();
        toPlot(repeats, name);


    }

    //constructorForSearch
    public SubgraphEdgeset(SourceGraph source, int repeats) throws IOException {

        this.tarSize = new int[]{10000, 20000, 30000, 40000, 50000, 60000, 70000, 80000, 90000, 100000};//, 6000, 8000, 10000};
        this.filenameTRUE = op.getResultsName()+ "SubEdgeset_TRUE.txt";
        this.filenameFALSE = op.getResultsName()+"SubEdgeset_FALSE.txt";
        this.fstreamTRUE = new FileWriter(filenameTRUE);
        this.fstreamFALSE = new FileWriter(filenameFALSE);
        this.bwtrue = new BufferedWriter(fstreamTRUE);
        this.bwfalse = new BufferedWriter(fstreamFALSE);
        this.repeats = repeats;

        for (int i = 0; i < this.repeats; i++) {
            createSubgraphs(source);
            for (int p = 0; p < tarSize.length; p++) {
                oneRun(source, tarSize[p]);
            }

        }

        bwtrue.close();
        bwfalse.close();

    }

    //creates random sematnic subgraphs of a specified size and edgeset
    public void createSubgraphs(SourceGraph source) throws IOException {
        //want to create a set of semantic subgraphs of a particular nodeset size, whereby the edgeset size increases by one each time
        RandomQuery rq = new RandomQuery(source, 4, "Compound", 3);
        RandomQuery rq2 = new RandomQuery(source, 4, "Compound", 4);
        RandomQuery rq3 = new RandomQuery(source, 4, "Compound", 5);
        RandomQuery rq4 = new RandomQuery(source, 4, "Compound", 6);
        //RandomQuery rq5 = new RandomQuery(source, 4, "Compound", 7);
        QueryGraph[] fourNodes = new QueryGraph[]{rq.getRanQuery(), rq2.getRanQuery(), rq3.getRanQuery(), rq4.getRanQuery()}; //rq5.getRanQuery()};
        this.qs = fourNodes;
    }

    //does a false and a true search for each semantic subgraph in a randomly created target graph
    public void oneRun(SourceGraph source, int tarSize) throws IOException {

        RandomGraph con1 = new RandomGraph(source, tarSize, 1);
        bwtrue.append(">>>>>>>targetSize:" + tarSize + "\n");
        bwfalse.append(">>>>>>>targetSize:" + tarSize + "\n");

        for (int rep = 0; rep < qs.length; rep++) {

            SemSearch trueST = new SemSearch(con1.getRandomSourceGraph(), qs[rep], null, null, 8, "Compound", true, 0.8, true, false, true);
            trueST.completeSearch();
            bwtrue.append(qs[rep].getSub().edgeSet().size() + "\t" + trueST.getTime() + "\n");

            SemSearch falseST = new SemSearch(con1.getRandomSourceGraph(), qs[rep], null, null, 8, "Compound", true, 0.8, true, false, false);
            falseST.completeSearch();
            bwfalse.append(qs[rep].getSub().edgeSet().size() + "\t" + falseST.getTime() + "\n");

        }
    }

    public void toPlot(int duplicates, File name) {

        String thisLine;
        String tarSize2 = "";

        //Open the file for reading
        try {
            BufferedReader brTRUE = new BufferedReader(new FileReader(name));
            while ((thisLine = brTRUE.readLine()) != null) { // while loop begins here

                if (thisLine.startsWith(">>>>>>>targetSize:")) {
                    tarSize2 = thisLine.substring(18, thisLine.length());

                }
                if (thisLine.startsWith("3")) {
                    String[] split = thisLine.split("\t");
                    allocateTime(split[1], trueTimes3, tarSize2);
                }
                if (thisLine.startsWith("4")) {
                    String[] split = thisLine.split("\t");
                    allocateTime(split[1], trueTimes4, tarSize2);
                }
                if (thisLine.startsWith("5")) {
                    String[] split = thisLine.split("\t");
                    allocateTime(split[1], trueTimes5, tarSize2);
                }
                if (thisLine.startsWith("6")) {
                    String[] split = thisLine.split("\t");
                    allocateTime(split[1], trueTimes6, tarSize2);
                }

            } // end while 
        } // end try
        catch (IOException e) {
            System.err.println("Error: " + e);

        } // end for


        System.out.print("3>>");
        //System.out.println(trueTimes3.toString());
        for (int i = 0; i < tarSize.length; i++) {
            //System.out.println(tarSize[i]);
            Set<String> local = trueTimes3.get((Integer.toString(tarSize[i])));
            double total = 0.0;
            int count = 0;
            for (String time : local) {
                total += Double.parseDouble(time);
                count++;
            }
            System.out.print(total / count + ",");

        }

        System.out.println("");

        System.out.print("4>>");
        for (int i = 0; i < tarSize.length; i++) {
            Set<String> local = trueTimes4.get((Integer.toString(tarSize[i])));
            double total = 0.0;
            int count = 0;
            for (String time : local) {
                total += Double.parseDouble(time);
                count++;
            }
            System.out.print(total / count + ",");

        }
        System.out.println("");

        System.out.print("5>>");
           for (int i = 0; i < tarSize.length; i++) {
            Set<String> local = trueTimes5.get((Integer.toString(tarSize[i])));
            double total = 0.0;
            int count = 0;
            for (String time : local) {
                total += Double.parseDouble(time);
                count++;
            }
            System.out.print(total / count + ",");

        }
        System.out.println("");

        System.out.print("6>>");
           for (int i = 0; i < tarSize.length; i++) {
            Set<String> local = trueTimes6.get((Integer.toString(tarSize[i])));
            double total = 0.0;
            int count = 0;
            for (String time : local) {
                total += Double.parseDouble(time);
                count++;
            }
            System.out.print(total / count + ",");

        }


    } // end main

    private void allocateTime(String time, Map<String, Set<String>> allocate, String size) {

        if (allocate.containsKey(size)) {
            Set<String> local = allocate.get(size);
            local.add(time);
            allocate.put(size, local);
        } else {

            Set<String> local = new HashSet<String>();
            local.add(time);
            allocate.put(size, local);
        }

    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.testing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import uk.ac.ncl.semsubgraphalgorithm.OutPutManagament;
import uk.ac.ncl.semsubgraphalgorithm.QueryGraph;
import uk.ac.ncl.semsubgraphalgorithm.RandomGraph;
import uk.ac.ncl.semsubgraphalgorithm.SemSearch;
import uk.ac.ncl.semsubgraphalgorithm.SemanticComplexity;
import uk.ac.ncl.semsubgraphalgorithm.SerialisedGraph;
import uk.ac.ncl.semsubgraphalgorithm.SourceGraph;
import uk.ac.ncl.semsubgraphalgorithm.spikingGraph.SpikeSourceGraph;


/**
 *
 * @author joemullen
 */
public class QueryGraphSemanticComplexity {

    private double[] thresholds = new double[]{1.0, 0.8, 0.6, 0.4, 0.2, 0.0};
    private FileWriter fstreamTRUE;
    private BufferedWriter bwTRUE;
    private String filenameTRUE;
    private FileWriter fstreamFALSE;
    private BufferedWriter bwFALSE;
    private String filenameFALSE;
    private int replicates;
    private OutPutManagament op;

    public static void main(String args[]) throws IOException, FileNotFoundException, ClassNotFoundException, Exception {

        QueryGraphSemanticComplexity st = new QueryGraphSemanticComplexity(5);
        System.gc();

    }

    public QueryGraphSemanticComplexity(int replicate) throws IOException, FileNotFoundException, ClassNotFoundException, Exception {

        this.op = new OutPutManagament();
        this.filenameTRUE = op.getResultsName()+"QueryGraphSemanticComplexity_TRUE.txt";
        this.filenameFALSE = op.getResultsName()+"QueryGraphSemanticComplexity_FALSE.txt";
        this.replicates = replicate;
        runWithDifferentSub();
        bwTRUE.close();
        bwFALSE.close();
        individParseResults();

    }

    public void runWithDifferentSub() throws IOException, FileNotFoundException, ClassNotFoundException, Exception {

        SerialisedGraph ser = new SerialisedGraph();
        SourceGraph source = ser.useSerialized("graph.data");

        Set<QueryGraph> subs = new HashSet<QueryGraph>();
        QueryGraph elements10 = new QueryGraph(QueryGraph.SemanticSub.TEST10ELEMENT);
        subs.add(elements10);

        //create a random graph of size 1000 

        this.fstreamTRUE = new FileWriter(filenameTRUE);
        this.bwTRUE = new BufferedWriter(fstreamTRUE);
        this.fstreamFALSE = new FileWriter(filenameFALSE);
        this.bwFALSE = new BufferedWriter(fstreamFALSE);



        for (int i = 1; i <= replicates; i++) {
            RandomGraph ran = new RandomGraph(source, 1000, 1);
            SourceGraph ranSource = ran.getRandomSourceGraph();
            for (QueryGraph sub : subs) {
                //spike the graph-DO NOT PLUG IT 
                singleRun(sub, ranSource, i);
            }
        }
    }

    public void singleRun(QueryGraph q, SourceGraph ranSource, int replicate) throws IOException, FileNotFoundException, ClassNotFoundException, Exception {

        for (int i = 0; i < thresholds.length; i++) {
            //create a random graph          
            QueryGraph spikeWith;
            //create a new semanticSubgraph with a ST that matches the threshold
            SemanticComplexity sc = new SemanticComplexity(q, (i * 2), 0.0, false, ranSource);
            spikeWith = sc.getNewQuery();

            //do a semantic search for the spiked subgraphs using the thresholds
            SemSearch noneSpikedSearchFALSE = new SemSearch(ranSource, q, null, null, 8, "Compound", true, thresholds[i], true, false, false);
            noneSpikedSearchFALSE.completeSearch();
            int nsMatchesFALSE = noneSpikedSearchFALSE.numberOfMathces();
            bwFALSE.append(nsMatchesFALSE + "\t");

            SemSearch noneSpikedSearchTRUE = new SemSearch(ranSource, q, null, null, 8, "Compound", true, thresholds[i], true, false, true);
            noneSpikedSearchTRUE.completeSearch();
            int nsMatchesTRUE = noneSpikedSearchTRUE.numberOfMathces();
            bwTRUE.append(nsMatchesTRUE + "\t");

            //spike graph with the altered semantic subgraph
            SpikeSourceGraph g = new SpikeSourceGraph(ranSource, spikeWith, 100, false);

            SemSearch spikedSearchFALSE = new SemSearch(g.getSpikedGraph(), q, null, null, 8, "Compound", true, thresholds[i], true, false, false);
            spikedSearchFALSE.completeSearch();
            int sMatchesFALSE = spikedSearchFALSE.numberOfMathces();
            bwFALSE.append(sMatchesFALSE + "\t" + "\n");

            SemSearch spikedSearchTRUE = new SemSearch(g.getSpikedGraph(), q,  null, null, 8, "Compound", true, thresholds[i], true, false, true);
            spikedSearchTRUE.completeSearch();
            int sMatchesTRUE = spikedSearchTRUE.numberOfMathces();
            bwTRUE.append(sMatchesTRUE + "\t" + "\n");

        }

    }

    public void individParseResults() throws FileNotFoundException, IOException {

        BufferedReader brTRUE = new BufferedReader(new FileReader(filenameTRUE));
        parseResults(brTRUE, true);
        BufferedReader brFALSE = new BufferedReader(new FileReader(filenameFALSE));
        parseResults(brFALSE, false);

    }

    public void parseResults(BufferedReader br, boolean type) throws FileNotFoundException, IOException {

        String line;

        int[] resultsSpiked = new int[thresholds.length];
        int[] resultsNoneSpiked = new int[thresholds.length];

        int count = 0;

        while ((line = br.readLine()) != null) {
            if (count == thresholds.length) {
                count = 0;
            }
            String[] split = line.split("\t");
            resultsNoneSpiked[count] = Integer.parseInt(split[0]) + resultsNoneSpiked[count];
            resultsSpiked[count] = Integer.parseInt(split[1]) + resultsSpiked[count];

            count++;
        }

        for (int i = 0; i < resultsSpiked.length; i++) {
            resultsSpiked[i] = (resultsSpiked[i] / replicates);
            resultsNoneSpiked[i] = (resultsNoneSpiked[i] / replicates);

        }

        br.close();


        if (type == true) {

            //Keep the existing content and append the new content in the end of the file.
            fstreamTRUE = new FileWriter(filenameTRUE, true);
            bwTRUE = new BufferedWriter(fstreamTRUE);

            bwTRUE.append("\n");
            bwTRUE.append("[INFO] PLOT:" + "\n");
            String conn = Arrays.toString(flipAround(thresholds));
            bwTRUE.append("Thresholds->c(" + conn.substring(1, conn.length() - 1) + ")" + "\n");
            String spikedString = Arrays.toString(flipAround(resultsSpiked));
            bwTRUE.append("Spiked->c(" + spikedString.substring(1, spikedString.length() - 1) + ")" + "\n");
            String nonspiked = Arrays.toString(flipAround(resultsNoneSpiked));
            bwTRUE.append("Not_Spiked->c(" + nonspiked.substring(1, nonspiked.length() - 1) + ")" + "\n");

            bwTRUE.close();
        }

        if (type == false) {

            //Keep the existing content and append the new content in the end of the file.
            fstreamFALSE = new FileWriter(filenameFALSE, true);
            bwFALSE = new BufferedWriter(fstreamFALSE);

            bwFALSE.append("\n");
            bwFALSE.append("[INFO] PLOT:" + "\n");
            String conn = Arrays.toString(flipAround(thresholds));
            bwFALSE.append("Thresholds->c(" + conn.substring(1, conn.length() - 1) + ")" + "\n");
            String spikedString = Arrays.toString(flipAround(resultsSpiked));
            bwFALSE.append("Spiked->c(" + spikedString.substring(1, spikedString.length() - 1) + ")" + "\n");
            String nonspiked = Arrays.toString(flipAround(resultsNoneSpiked));
            bwFALSE.append("Not_Spiked->c(" + nonspiked.substring(1, nonspiked.length() - 1) + ")" + "\n");

            bwFALSE.close();
        }


    }

    public double[] flipAround(double[] flip) {
        int length = flip.length;
        double[] local = new double[length];
        for (int i = 0; i < length; i++) {
            int newPosit = (length - (i+1));
            local[newPosit] = flip[i];
        }
        return local;
    }
    
    public int[] flipAround(int[] flip) {
        int length = flip.length;
        int[] local = new int[length];
        for (int i = 0; i < length; i++) {
            int newPosit = (length - (i+1));
            local[newPosit] = flip[i];
        }
        return local;
    }
}

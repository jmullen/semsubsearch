/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.testing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import uk.ac.ncl.semsubgraphalgorithm.OutPutManagament;
import uk.ac.ncl.semsubgraphalgorithm.QueryGraph;
import uk.ac.ncl.semsubgraphalgorithm.RandomGraph;
import uk.ac.ncl.semsubgraphalgorithm.SemSearch;
import uk.ac.ncl.semsubgraphalgorithm.SerialisedGraph;
import uk.ac.ncl.semsubgraphalgorithm.SourceGraph;
import uk.ac.ncl.semsubgraphalgorithm.spikingGraph.SpikeSourceGraph;


/**
 *
 * @author joemullen
 */
public class CalculatingST {

    private double[] thresholds = new double[]{0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0};
    private FileWriter fstream;
    private BufferedWriter bw;
    private String filename;
    private boolean type;
    private OutPutManagament op;

    public static void main(String args[]) throws IOException, FileNotFoundException, ClassNotFoundException, Exception {

        CalculatingST st = new CalculatingST(false);
        System.gc();

    }

    public CalculatingST(boolean type) throws IOException, FileNotFoundException, ClassNotFoundException, Exception {

        this.type = type;
        this.filename = op.getResultsName()+"calculatingST_"+type+".txt";   
        runWithDifferentSub(5);
        bw.close();
        parseResults();



    }

    public void runWithDifferentSub(int replicate) throws IOException, FileNotFoundException, ClassNotFoundException, Exception {

        SerialisedGraph ser = new SerialisedGraph();
        SourceGraph source = ser.useSerialized("graph.data");

        Set<QueryGraph> subs = new HashSet<QueryGraph>();
        QueryGraph three = new QueryGraph(QueryGraph.SemanticSub.SIMCOMPSSMALL);
        subs.add(three);
        QueryGraph four = new QueryGraph(QueryGraph.SemanticSub.CTCT);
        subs.add(four);
        QueryGraph five = new QueryGraph(QueryGraph.SemanticSub.FIVENODE);
        subs.add(five);
        QueryGraph six = new QueryGraph(QueryGraph.SemanticSub.SIXNODE);
        subs.add(six);

        //create a random graph of size 1000 

        this.fstream = new FileWriter(filename);
        this.bw = new BufferedWriter(fstream);


        for (int i = 1; i <= replicate; i++) {
            for (QueryGraph sub : subs) {
                //spike the graph-DO NOT PLUG IT 
                singleRun(sub, source, i);
            }
        }
    }

    public void singleRun(QueryGraph q, SourceGraph source, int replicate) throws IOException, FileNotFoundException, ClassNotFoundException, Exception {

        RandomGraph ran = new RandomGraph(source, 10000, 1);
        SourceGraph ranSource = ran.getRandomSourceGraph();

        bw.append(q.getMotifName() +"_REPLICATE:"+ replicate+ "\n");

        for (int i = 0; i < thresholds.length; i++) {
            //do a semantic search for the spiked subgraphs using the thresholds

            SemSearch noneSpikedSearch = new SemSearch(ranSource, q,null, null, 8, "Compound", true, thresholds[i], true, false, type);
            
    
            noneSpikedSearch.completeSearch();
            int nsMatches = noneSpikedSearch.numberOfMathces();
            bw.append(nsMatches + "\t");


        }

        bw.append("\n");

        SpikeSourceGraph g = new SpikeSourceGraph(ranSource, q, 100, false);

        for (int i = 0; i < thresholds.length; i++) {

            SemSearch spikedSearch = new SemSearch(g.getSpikedGraph(),  q,null, null, 8, "Compound", true, thresholds[i], true, false, type);
            spikedSearch.completeSearch();
            int sMatches = spikedSearch.numberOfMathces();
            bw.append(sMatches + "\t");

            //bw.append(sMatches - nsMatches + "\n");
        }

        bw.append("\n");

    }

    public void parseResults() throws FileNotFoundException, IOException {

        BufferedReader br = new BufferedReader(new FileReader(filename));
        String line;

        int[] resultsSpiked = new int[thresholds.length];
        int[] resultsNoneSpiked = new int[thresholds.length];

        int count = 0;
        int replicates = 0;

        while ((line = br.readLine()) != null) {
            count++;
            if (count % 3 == 0) {
                count = 0;
                replicates++;
            }
            if (count == 1) {
                //nothing
            }
            if (count == 2) {
                String[] split = line.split("\t");
                for (int i = 0; i < split.length; i++) {
                    resultsSpiked[i] = Integer.parseInt(split[i]) + resultsSpiked[i];
                }
            }
            if (count == 0) {
                String[] split = line.split("\t");
                for (int i = 0; i < split.length; i++) {
                    resultsNoneSpiked[i] = Integer.parseInt(split[i]) + resultsNoneSpiked[i];
                }
            }
        }

        for (int i = 0; i < resultsSpiked.length; i++) {
            resultsSpiked[i] = (resultsSpiked[i] / replicates);
            resultsNoneSpiked[i] = (resultsNoneSpiked[i] / replicates);

        }

        br.close();

        //Keep the existing content and append the new content in the end of the file.
        fstream = new FileWriter(filename, true);
        bw = new BufferedWriter(fstream);

        bw.append("\n");
        bw.append("[INFO] PLOT:" + "\n");
        String thres = Arrays.toString(thresholds);
        bw.append("Thresholds->c(" + thres.substring(1, thres.length() - 1) + ")" + "\n");
        String spiked = Arrays.toString(resultsSpiked);
        bw.append("None_Spiked->c(" + spiked.substring(1, spiked.length() - 1) + ")" + "\n");
        String nonspiked = Arrays.toString(resultsNoneSpiked);
        bw.append("Spiked->c(" + nonspiked.substring(1, nonspiked.length() - 1) + ")" + "\n");

        bw.close();


    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.testing;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.io.File;
import uk.ac.ncl.semsubgraphalgorithm.drugBankWork.GetShortestPath;
import uk.ac.ncl.semsubgraphalgorithm.drugBankWork.GetUniquePairs;
import uk.ac.ncl.semsubgraphalgorithm.drugBankWork.MergingALLPairs;
import uk.ac.ncl.semsubgraphalgorithm.drugBankWork.SubGraphMatches2Accession;
import uk.ac.ncl.semsubgraphalgorithm.OutPutManagament;
import uk.ac.ncl.semsubgraphalgorithm.QueryGraph;
import uk.ac.ncl.semsubgraphalgorithm.QueryGraph.SemanticSub;
import uk.ac.ncl.semsubgraphalgorithm.SemSearch;
import uk.ac.ncl.semsubgraphalgorithm.SerialisedGraph;
import uk.ac.ncl.semsubgraphalgorithm.SourceGraph;

/**
 *
 * @author joemullen
 */
public class DrugBankWorkPaper {

    private OutPutManagament op;

    public static void main(String args[]) throws FileNotFoundException, IOException, ClassNotFoundException {

        DrugBankWorkPaper dbw = new DrugBankWorkPaper();
        dbw.run();

    }

    public DrugBankWorkPaper() {

        this.op = new OutPutManagament();

    }

    public void run() throws FileNotFoundException, IOException, ClassNotFoundException {

        SerialisedGraph ser = new SerialisedGraph();
        SourceGraph source = ser.useSerialized("graph.data");
        part1_GETUNIQUEPAIRS();
        part2_GETSHORTESTPATHS(source);
        Set<QueryGraph> subs = part3_SEARCHFORSUBS(source);
        part4_CONVERTMATCHES2DTPAIRS(subs);
        part5_MAPTODB3();


    }

    public void part1_GETUNIQUEPAIRS() throws FileNotFoundException, IOException {
        GetUniquePairs gup = new GetUniquePairs();
        gup.getCommonCompounds();
        gup.getCommonProteins();
        gup.ExtractMissingElements();
        System.out.println("[INFO] Calculated all unique pairs from DB3");

    }

    public void part2_GETSHORTESTPATHS(SourceGraph source) throws IOException {
        String DBfile = op.getDrugBankDirectoryPath();
        GetShortestPath sp = new GetShortestPath(source);
        sp.getPairs(DBfile + "VALIDnomatchesdb3.txt");
        sp.getAccessions(DBfile + "con_listOP_55e0c.tsv");
        sp.convertToUndirected();
        sp.getAllShortestPaths();
        System.out.println("[INFO] Got the shortest path betwen all valid DB3 pairs");

    }

    public Set<QueryGraph> part3_SEARCHFORSUBS(SourceGraph source) throws IOException {

        
        Set<QueryGraph> subs = new HashSet<>();
        QueryGraph q1 = new QueryGraph(SemanticSub.SIMTARGSSMALL);
        QueryGraph q2 = new QueryGraph(SemanticSub.CTIT);
        QueryGraph q3 = new QueryGraph(SemanticSub.CTCT);
        QueryGraph q4 = new QueryGraph(SemanticSub.SIMCOMPSSMALL);
        QueryGraph q5 = new QueryGraph(SemanticSub.NEWTARGIND);

        String dir1 = op.getSearhchesDirectoryPath();
        File dir = new File(dir1);
        if (dir.isDirectory()) {
            for (File child : dir.listFiles()) {
                if (child.delete()) {
                    System.out.println("[INFO]" + child.getName() + " is deleted!");
                } else {
                    System.out.println("[INFO] Delete operation failed- check for duplicates with differing timestamps");
                }
            }
        }

        subs.add(q1);
        subs.add(q3);
        subs.add(q4);
        subs.add(q5);

        for (QueryGraph q : subs) {

            SemSearch sem = new SemSearch(source, q, null, null, 8, "Compound", true, 0.8, true, true, true);
            sem.completeSearch();
        }

        System.out.println("[INFO] Completed a semantic search for all semantic subgraphs");
        return subs;

    }

    public void part4_CONVERTMATCHES2DTPAIRS(Set<QueryGraph> subs) throws FileNotFoundException, IOException {
        String dir1 = op.getSearhchesDirectoryPath();
        File dir = new File(dir1);
        if (dir.isDirectory()) {
            for (File child : dir.listFiles()) {
                String filenam = child.toString();
                if (filenam.contains("[")) {
                    String[] subName1 = filenam.split("\\[");
                    String name1 = subName1[1];
                    String[] split2 = name1.split("\\]_");
                    String subName = split2[0];

                    for (QueryGraph q : subs) {
                        if (q.getMotifName().equals(subName)) {
                            SubGraphMatches2Accession mm = new SubGraphMatches2Accession(filenam, q, true, true);
                            mm.mapp();
                        }

                    }
                }
            }
        }

        System.out.println("[INFO] Extracted the DB accesssion and Uniprot ID from every match (and removed duplication)");
    }

    public void part5_MAPTODB3() throws FileNotFoundException, IOException {
        String[] files = new String[4];
        String dir2 = op.getuniquePairsDirectoryPath();
        File dir3 = new File(dir2);
        int count = 0;
        if (dir3.isDirectory()) {
            for (File child : dir3.listFiles()) {
                files[count] = child.toString();
                count++;
            }
        }

        MergingALLPairs mp = new MergingALLPairs();
        mp.mergeAllUniqueFromTheseFiles(files);
        System.out.println("[INFO] Identified all pairs that are identified by all semantic subgraphs");
    }
}

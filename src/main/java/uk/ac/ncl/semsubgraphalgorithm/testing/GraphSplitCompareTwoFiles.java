/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.testing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author joemullen
 */
public class GraphSplitCompareTwoFiles {

    public static void main(String[] args) throws IOException, FileNotFoundException, ClassNotFoundException, Exception {

        String split = "C:/Users/b0918176/Desktop/SemSubgraphAlgorithm/Results/Searches/[_split_]_ST0.8_14020047202.txt";
        String normal = "C:/Users/b0918176/Desktop/SemSubgraphAlgorithm/Results/Searches/[RANDOM(6-8)]_ST0.8_14020047086.txt";
        GraphSplitCompareTwoFiles gst = new GraphSplitCompareTwoFiles(split, normal);
        gst.populateMathches();
        gst.compareList();



    }
    private String[] splitOrder;
    private String[] normalOrder;
    private File splitMatches;
    private File normalMatches;
    private Set<String> splits2;
    private Set<String> completeSearch2;
    private ArrayList<String> onlyInMatch;
    private Set<String> onlyInMatch2;

    public GraphSplitCompareTwoFiles(String split, String norm) {
        //this.normalOrder = normalOrder;
        //this.splitOrder = splitOrder;
        this.splitMatches = new File(split);
        this.normalMatches = new File(norm);
        this.splits2 = new HashSet<String>();
        this.completeSearch2 = new HashSet<String>();
        this.onlyInMatch = new ArrayList<String>();
        this.onlyInMatch2 = new HashSet<String>();

    }

    public void populateMathches() throws FileNotFoundException, IOException {


        String thisLine;
        int count = 0;
        int lines=0;
        //Add all the split subs to a hashmap 'splits'
        try {
            BufferedReader br = new BufferedReader(new FileReader(splitMatches));
       
            while ((thisLine = br.readLine()) != null) { // while loop begins here
                Set<String> local = new HashSet<String>();
                lines++;
                String[] split = thisLine.split("\t");
                ArrayList<Integer> numbers = new ArrayList<Integer>();
                for (int i = 0; i < split.length; i++) {

                    numbers.add(Integer.parseInt(split[i].trim()));
                    local.add(split[i]);
                    count++;
                }

                Collections.sort(numbers);
                splits2.add(numbers.toString());

            }

        } catch (IOException e) {
            System.err.println("Error: " + e);
        }

        System.out.println(lines);
        count = 0;

        try {
            BufferedReader br = new BufferedReader(new FileReader(normalMatches));
            while ((thisLine = br.readLine()) != null) { // while loop begins here
                if (thisLine.startsWith("SN:")) {

                    String line = thisLine.substring(3, thisLine.length());

                    ArrayList<Integer> numbers = new ArrayList<Integer>();
                    Set<String> local = new HashSet<String>();
                    String[] split = line.split("\t");

                    for (int i = 0; i < split.length; i++) {
                        numbers.add(Integer.parseInt(split[i].trim()));
                        local.add(split[i]);
                        count++;
                    }

                    Collections.sort(numbers);
                    completeSearch2.add(numbers.toString());

                }
            }

        } catch (IOException e) {
            System.err.println("Error: " + e);
        }



    }

    
     public void compareList() {

        int onlyInSplits = 0;
        int inBoth = 0;

        //go through every keyset in the splits hashmap
        for (String key : splits2) {

            
            if (completeSearch2.contains(key)) {
                //splits.remove(key);
                //System.out.println("in both");
                inBoth++;


            } else {
                onlyInSplits++;
                System.out.println("OISplits "+key);

            }


        }

        for (String key : completeSearch2) {

            
            if (splits2.contains(key)) {
            } else {
                onlyInMatch2.add(key);
                System.out.println("OINorm "+key);


            }


        }

        System.out.println("[INFO] In both: " + inBoth);
        System.out.println("[INFO] Only in splits: " + onlyInSplits);
        System.out.println("[INFO] Splits Size: " + splits2.size());
        System.out.println("[INFO] ONLY in match " + onlyInMatch2.size());

    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.drugBankWork;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author joemullen
 */
public class SummarisingResults {

    public static void main(String[] args) throws FileNotFoundException, IOException {
        String chlor = "/Users/joemullen/Desktop/Comparing2drugBanks/Results/Chlor/matcheschlor&newtargs.txt";
        String newtargsmall = "/Users/joemullen/Desktop/Comparing2drugBanks/Results/NewTargSmall/matchesnewtargsmallMotif&newtargsfromDB3.txt";
        String newboth = "/Users/joemullen/Desktop/Comparing2drugBanks/Results/NewTarNewComp/matchesnewBothMotif&newtargsfromDB3.txt";
        String simTarg = "/Users/joemullen/Desktop/Comparing2drugBanks/Results/SimilarTarget/matchesnewtargMotif&newtargsfromDB3.txt";

        SummarisingResults sr = new SummarisingResults();
        sr.mergeResults(simTarg, "nt");
        sr.mergeResults(chlor, "chl");
        sr.mergeResults(newtargsmall, "nts");
        sr.mergeResults(newboth, "nb");
        sr.checkDifferences();
        sr.checkRepeats();


    }
    Set<String> allmatches = new HashSet<String>();
    Set<String> chlor = new HashSet<String>();
    Set<String> newtarg = new HashSet<String>();
    Set<String> newboth = new HashSet<String>();
    Set<String> newtargsmall = new HashSet<String>();

    public void mergeResults(String file, String id) throws FileNotFoundException, IOException {

        BufferedReader br = new BufferedReader(new FileReader(file));
        String line2;
        while ((line2 = br.readLine()) != null) {
            if (id.equals("chl")) {
                chlor.add(line2);
            }
            if (id.equals("nb")) {
                newboth.add(line2);
            }
            if (id.equals("nts")) {
                newtargsmall.add(line2);
            }
            if (id.equals("nt")) {
                newtarg.add(line2);
            }

            allmatches.add(line2);
        }



    }

    public void checkDifferences() {
        System.out.println("----------------------AllUniqueMatches");
        System.out.println("matches size: " + allmatches.size());

        System.out.println("----------------------Chlo");
        System.out.println("SIZE: " + chlor.size());
        System.out.println("NEWTARG: " + match(chlor, newtarg));
        System.out.println("NEWBOTH: " + match(chlor, newboth));
        System.out.println("NEWTARGSMALL: " + match(chlor, newtargsmall));
        for (String pair : chlor) {
            System.out.println(pair);
        }

        System.out.println("----------------------NewTargSim");
        System.out.println("SIZE: " + newtarg.size());
        System.out.println("CHLO: " + match(newtarg, chlor));
        System.out.println("NEWBOTH: " + match(newtarg, newboth));
        System.out.println("NEWTARGSMALL: " + match(newtarg, newtargsmall));
        for (String pair : newtarg) {
            System.out.println(pair);
        }


        System.out.println("----------------------NewTargetSmall");
        System.out.println("SIZE: " + newtargsmall.size());
        System.out.println("NEWTARG: " + match(newtargsmall, newtarg));
        System.out.println("CHLOR: " + match(newtargsmall, chlor));
        System.out.println("NEWBOTH: " + match(newtargsmall, newboth));
        for (String pair : newtargsmall) {
            System.out.println(pair);
        }

        System.out.println("----------------------NewBoth");
        System.out.println("SIZE: " + newboth.size());
        System.out.println("NEWTARG: " + match(newboth, newtarg));
        System.out.println("CHLOR: " + match(newboth, chlor));
        System.out.println("NEWTARGSMALL: " + match(newboth, newtargsmall));
        for (String pair : newboth) {
            System.out.println(pair);
        }


    }

    public int match(Set<String> check, Set<String> check2) {
        int matches = 0;
        for (String pair : check) {
            if (check2.contains(pair)) {
                matches++;
            }

        }
        return matches;

    }

    public void checkRepeats() {

        int chlorONLY = 0;
        int ntsimONLY = 0;
        int ntSmallONLY = 0;

        int chlorANDntsim = 0;
        int chlorANDntSMALL = 0;
        int ntsimANDntSMALL = 0;


        int all = 0;


        for (String pair : allmatches) {
            if (chlor.contains(pair)) {
                if (newtarg.contains(pair) && !newtargsmall.contains(pair)) {
                    chlorANDntsim++;
                } else if (newtargsmall.contains(pair) && !newtarg.contains(pair)) {
                    chlorANDntSMALL++;
                } else if (newtarg.contains(pair) && newtargsmall.contains(pair)) {
                    all++;
                } else {
                    chlorONLY++;
                }



            } else if (newtarg.contains(pair)) {
                if (newtargsmall.contains(pair)) {
                    ntsimANDntSMALL++;
                } else {
                    ntsimONLY++;
                }

            } else if (newtargsmall.contains(pair)) {
                ntSmallONLY++;
            }
        }

        System.out.println("----------------------Overlaps");
        System.out.println("Chlor only: " + chlorONLY);
        System.out.println("ntsim only: " + ntsimONLY);
        System.out.println("ntSmallONLY: " + ntSmallONLY);
        System.out.println("----------------------");

        System.out.println("CHLO&ntSIM: " + chlorANDntsim);
        System.out.println("CHLO&ntSMALL: " + chlorANDntSMALL);
        System.out.println("ntSIm&ntSMALL: " + ntsimANDntSMALL);
        System.out.println("in all: " + all);

        System.out.println("TOTAL MATCHES: " + allmatches.size());

    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.drugBankWork;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.jgrapht.DirectedGraph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import uk.ac.ncl.semsubgraphalgorithm.OutPutManagament;
import uk.ac.ncl.semsubgraphalgorithm.SourceGraph;


/**
 *
 * @author joemullen
 */
public class GetShortestPath {

    private DirectedGraph<String, DefaultEdge> targ;
    private SourceGraph sp;
    private Map<String, String> DRUGBANKID;
    private Map<String, String> UNIPROTID;
    private ArrayList<String> PAIRS;
    private int found = 0;
    private int nullpath = 0;
    private int greaterthan10 = 0;
    private String file;
    private Map<String, Integer> commonPaths;
    private Map<String, Integer> closedWorld;
    private int noIDs = 0;
    private SimpleGraph<String, DefaultEdge> UDorigi;
    private int failedClosedWorld;
    private int occurringOnce;
    private OutPutManagament op;

    public GetShortestPath(SourceGraph sg) throws IOException {

        this.sp = sg;
        this.targ = sg.getSourceGraph();
        this.PAIRS = new ArrayList<String>();
        this.DRUGBANKID = new HashMap<String, String>();
        this.UNIPROTID = new HashMap<String, String>();
        this.op = new OutPutManagament();
        this.file = op.getDrugBankDirectoryPath()+"pathsFINALallVALID.txt";
        this.commonPaths = new HashMap<String, Integer>();
        this.closedWorld = new HashMap<String, Integer>();
        this.UDorigi = new SimpleGraph<String, DefaultEdge>(DefaultEdge.class);


    }

    public static void main(String[] args) throws IOException, FileNotFoundException, ClassNotFoundException {

        //SourceGraph source = new SourceGraph(new File("/Users/joemullen/Desktop/OP_tab_exporter/LargePLUSindsPLUSdisgenet/con_listOP_55e0c.tsv"), new File("/Users/joemullen/Desktop/OP_tab_exporter/LargePLUSindsPLUSdisgenet/rel_listOP_55e0c.tsv"));
      


    }

    public void getAccessions(String file) throws FileNotFoundException, IOException {
        BufferedReader br = new BufferedReader(new FileReader(file));
        String line;
        br.readLine();
        while ((line = br.readLine()) != null) {
            String[] split = line.split("\t");
            if (!split[4].equals("NO_DRUGBANK")) {
                DRUGBANKID.put(split[4], split[0]);
                //System.out.println("Added DB: " + split[0] + ":" + split[4]);
            }

            if (!split[5].equals("NO_UNIPROT")) {
                UNIPROTID.put(split[5], split[0]);
                //System.out.println("Added UNi: " + split[0] + ":" + split[5]);
            }



        }
        // System.out.println(DDBID2ID.get("DB01656"));
    }

    public void getPairs(String file) throws FileNotFoundException, IOException {

        BufferedReader br = new BufferedReader(new FileReader(file));

        String line;
        while ((line = br.readLine()) != null) {
            PAIRS.add(line);

        }
    }

    public void getAllShortestPaths() throws IOException {

        BufferedWriter br = new BufferedWriter(new FileWriter(file));

        for (String pair : PAIRS) {
            //System.out.println("--------------------------------");
            String DB = null;
            String UNIPROT = null;
            String[] split = pair.split("\t");
            if (split.length < 2) {
             //   System.out.println("MISSING INFO: " + DB + "  " + UNIPROT);

            } else {
                if (split[0].equals("Not Available") || split[1].equals("Not Available")) {
                   // System.out.println("COULD NOT PARSE PAIR- one or more elements are missing");
                    noIDs++;
                } else {
                    if (DRUGBANKID.containsKey(split[0])) {
                        DB = DRUGBANKID.get(split[0]);

                    }
                    if (UNIPROTID.containsKey(split[1])) {
                        UNIPROT = UNIPROTID.get(split[1]);
                    }
                    if (DB != null && UNIPROT != null) {
                      //  System.out.println("Searching for path between db" + DB + " uni" + UNIPROT);
                        br.append(DB + "\t" + UNIPROT + "\t" + getShortestPath(DB, UNIPROT) + "\n");
                        found++;
                    } else {
                      //  System.out.println("COULD NOT FIND IDs FOR: " + split[0] + "  or: " + split[1]);
                        noIDs++;
                    }
                }
            }
        }

        br.append("\n");
        br.append("[INFO] SUMMARY" + "\n");
        br.append("[INFO]------------------------------" + "\n");
        br.append("[INFO] Searched for " + PAIRS.size() + " drug target interactions" + "\n");
        br.append("[INFO]------------------------------" + "\n");
        br.append("[INFO] No of different graphs found: " + commonPaths.size() + "\n");
        br.append("[INFO] Total sucessful finds: " + found + "\n");
        br.append("[INFO]------------------------------" + "\n");
        br.append("[INFO] No IDs (failed at parsing pairs): " + noIDs + "\n");
        br.append("[INFO] Nullpaths (unsuccesful): " + nullpath + "\n");
        br.append("[INFO] Paths greater than 10 (not included): " + greaterthan10 + "\n");
        br.append("[INFO] ------------------------------" + "\n");
        br.append("\n");
        br.append("[INFO] CLOSED WORLD SUMMARY" + "\n");
        br.append("[INFO] ------------------------------" + "\n");
        br.append("[INFO] No of different graphs found: " + closedWorld.size() + "\n");
        br.append("[INFO] No of different graphs (occurring more than once) found: " + occurringOnce + "\n");
        br.append("[INFO] Failed closed world: " + failedClosedWorld + "\n");
        br.append("[INFO] ------------------------------" + "\n");
        br.append("\n");


        Set<String> graphs = commonPaths.keySet();
        for (int y = 1900; y > 0; y--) {
            //System.out.println("Y: "+ y);
            for (String graph : graphs) {
                if (commonPaths.get(graph) == y) {
                    //System.out.println("count: "+ commonPaths.get(graph));
                    br.append("------------------------------------------------" + y + "\n");
                    br.append(graph + "\t" + commonPaths.get(graph) + "\n");
                }
            }
        }

        br.append("**********************************************" + "\n");
        br.append("**** CLOSED" + "\n");
        br.append("**** WORLD" + "\n");
        br.append("**********************************************" + "\n");



        Set<String> graphs2 = closedWorld.keySet();
        for (int y = 1900; y > 0; y--) {
            //System.out.println("Y: "+ y);
            for (String graph2 : graphs2) {
                if (closedWorld.get(graph2) == y) {
                    //System.out.println("count: "+ commonPaths.get(graph));
                    br.append("------------------------------------------------" + y + "\n");
                    br.append(graph2 + "\t" + closedWorld.get(graph2) + "\n");
                    if (y == 1) {
                        occurringOnce++;
                    }
                }
            }
        }

        System.out.println("[INFO] Finished calculating shortest path between "+ PAIRS.size()+ "D-T pairs");

        br.close();

    }

    public void convertToUndirected() {

        // SimpleGraph<String, DefaultEdge>  UDorigi = new SimpleGraph<String, DefaultEdge>(DefaultEdge.class);
        Set<String> allNodes = targ.vertexSet();
        for (String no : allNodes) {
            UDorigi.addVertex(no);
        }
        Set<DefaultEdge> allEdges = targ.edgeSet();
        for (DefaultEdge ed : allEdges) {
            String edgey = ed.toString().substring(1, ed.toString().length() - 1);
            String[] split = edgey.split(":");
            String from = split[0].trim();
            String to = split[1].trim();
            if (!from.equals(to)) {
                UDorigi.addEdge(from, to);
            }
            // System.out.println(ed.toString().substring(1, ed.toString().length() - 1));

        }

        //System.out.println("UD: " + UDorigi.toString());

    }

    public String getShortestPath(String node1, String node2) {

       // System.out.println("[INFO] Calling getShortestPath for: " + node1 + "(COMP)  " + node2 + "(TARG)");
        Set<String> added = new HashSet<String>();

        if (UDorigi.containsVertex(node1) && UDorigi.containsVertex(node2)) {
            DijkstraShortestPath tr2;
            tr2 = new DijkstraShortestPath(UDorigi, node1, node2);
            //System.out.println("TR2 to string: " + tr2.toString());
            //System.out.println("TR2 path length: " + tr2.getPathLength());

            String relations = "";
            String nodes = "";
            String conceptClass = "";
            Map<String, String> id2cc = new HashMap<String, String>();


            if (tr2 == null) {
                //System.out.println("THE EDGES FOUND ARE EMPTY.... why??? Could not calculate the shortest path");
                nullpath++;

            } else if (tr2.getPathLength() > 10) {

                greaterthan10++;


            } else if (tr2 != null && tr2.getPathLength() < 10) {

                Object[] why;
                //if (tr2.getPathEdgeList().isEmpty()) {
//                    System.out.println("TR2 pathedgelist is empty!!");
                //              } else {
                why = tr2.getPathEdgeList().toArray();
                int conNumber = 1;
                for (int i = 0; i < why.length; i++) {
                    String wh = why[i].toString();
                    String wh2 = wh.substring(1, wh.length() - 1);
                    //                   System.out.println("WH2:" + wh2);

                    String[] split = wh2.split(":");
                    String[] ccrel = new String[split.length];

                    if (!added.contains(split[0].trim())) {
                        nodes = nodes + split[0].trim() + "\t";
                        added.add(split[0].trim());
                        conceptClass = conceptClass + sp.getConceptClass(split[0].trim()) + "\t";
                        id2cc.put(split[0].trim(), sp.getConceptClass(split[0].trim()) + conNumber);
                        conNumber++;
                    }



                    if (!added.contains(split[1].trim())) {
                        nodes = nodes + split[1].trim() + "\t";
                        added.add(split[1].trim());
                        conceptClass = conceptClass + sp.getConceptClass(split[1].trim()) + "\t";
                        id2cc.put(split[1].trim(), sp.getConceptClass(split[1].trim()) + conNumber);
                        conNumber++;
                    }


                    relations = relations + why[i].toString();

                }

                relations.trim();
                nodes.trim();
                conceptClass.trim();
            //    System.out.println(nodes);
            //    System.out.println(conceptClass);
            //    System.out.println(relations);
            //    System.out.println("ID2CC: " + id2cc.toString());

                String ccrels = relations;
                Set<String> IDs = id2cc.keySet();
                for (String id : IDs) {
                    ccrels = ccrels.replace(id, id2cc.get(id));

                }

               // System.out.println("CONVERTED SUB RELS: " + ccrels);

                if (commonPaths.containsKey(conceptClass)) {
                    int local = commonPaths.get(conceptClass);
                    commonPaths.put(conceptClass, (local + 1));
                } else {
                    commonPaths.put(conceptClass, 1);
                }

                //get the edges of the path in the form of edges
                List<DefaultEdge> edgeies = tr2.getPathEdgeList();
                //conver these to string
                List<String> edgies2 = new ArrayList<String>();
                //populate to string 
                for (DefaultEdge ed : edgeies) {
                    //System.out.println("EDGE:" + ed);
                    edgies2.add(ed.toString().trim());
                }

                //need to account for symmetrical relations!!!!

                String[] nds = nodes.split("\t");


                if (closedWorldCheck(nds, edgies2) == true) {
                    if (closedWorld.containsKey(conceptClass)) {
                        int local = closedWorld.get(conceptClass);
                        closedWorld.put(conceptClass, (local + 1));
                    } else {
                        closedWorld.put(conceptClass, 1);
                    }
                }

                return conceptClass;

            }

        }

        return "Could Not Get Shortest Path- too large (>10)";

    }

    public boolean closedWorldCheck(String[] nodes, List<String> edges) {
     //   System.out.println("EDGES: " + edges.toString());

        Set<String> symm = new HashSet<String>();
        symm.add("Compound");
        symm.add("Target");
        symm.add("Indications");
        symm.add("Protein");

        boolean check = true;
        for (int h = 0; h < nodes.length; h++) {
            for (int i = 0; i < nodes.length; i++) {
                //if the target graph contains the edge
                if (targ.containsEdge(nodes[h], nodes[i])) {
                    //if the edge is not in the subgraph
                    if (!edges.contains(targ.getEdge(nodes[h], nodes[i]).toString())) {
                        //if the opposite edge is and the nodes have the same conceptClass that allows for symmetry
                        if (targ.containsEdge(nodes[i], nodes[h])) {


                            if (edges.contains(targ.getEdge(nodes[i], nodes[h]).toString())) {
                                if (sp.getConceptClass(nodes[h].trim()).equals(sp.getConceptClass(nodes[i].trim())) && symm.contains(sp.getConceptClass(nodes[i].trim()))) {
                                    //nothing
                                } else {

                                  //  System.out.println("Extra Edge between from:" + nodes[h].trim() + " to " + nodes[i].trim());
                                    check = false;
                                    failedClosedWorld++;
                                }
                            }


                        } else {

                       //     System.out.println("Extra Edge between from:" + nodes[h].trim() + " to " + nodes[i].trim());
                            check = false;
                            failedClosedWorld++;
                        }
                    }
                }
            }

        }
        return check;
    }

    public class ShortestPathGraph {

        String relations;
        String concepts;
        Map<String, String> id2CC;

        public String getRelations() {

            return relations;
        }

        public String getConcepts() {

            return concepts;
        }

        public ShortestPathGraph(String cons, String rels, HashMap<String, String> id2cc) {
            this.concepts = cons;
            this.relations = rels;
            this.id2CC = id2cc;
        }
    }
}

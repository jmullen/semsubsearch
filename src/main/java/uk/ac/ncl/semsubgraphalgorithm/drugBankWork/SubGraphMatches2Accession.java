/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.drugBankWork;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import uk.ac.ncl.semsubgraphalgorithm.OutPutManagament;
import uk.ac.ncl.semsubgraphalgorithm.QueryGraph;
import uk.ac.ncl.semsubgraphalgorithm.QueryGraph.SemanticSub;


/**
 *
 * @author joemullen
 */
public class SubGraphMatches2Accession {

    private Map<String, String> DRUG;
    private Map<String, String> PROTEIN;
    private ParseDrugBankData pi;
    private String read;
    private String write;
    private QueryGraph q;
    private boolean writeUnique;
    private boolean writeNonUnique;
    private OutPutManagament op;

    public static void main(String[] args) throws FileNotFoundException, IOException {
        
        SubGraphMatches2Accession s = new SubGraphMatches2Accession("Results/DB3WORK/SemSearch Results/Q5/OP_CIT_IN.txt", new QueryGraph(SemanticSub.NEWTARGIND),false, true);
        s.mapp();
        
        
        
        
    }

    public SubGraphMatches2Accession(String read, QueryGraph q, boolean writeUnique, boolean writeNonUnique) throws FileNotFoundException, IOException {
        this.q = q;
        this.read = read;
        this.pi = new ParseDrugBankData();
        pi.getDatassetInfo();
        pi.getDrugBankV3Info();
        this.DRUG = pi.getDrugBank2NAMEsDATASET();
        this.PROTEIN = pi.getUniprot2NAMEsDATASET();
        this.writeNonUnique = writeNonUnique;
        this.writeUnique = writeUnique;
        this.op = new OutPutManagament();

    }

    public void mapp() throws FileNotFoundException, IOException {

        int elements = 2;
        int drugpostion = 0;
        int proteinposition = 1;

        BufferedReader br = new BufferedReader(new FileReader(read));


        Set<String> uniquePairs = new HashSet<String>();
        ArrayList<String> allPairs = new ArrayList<String>();
        int count = 0;
        int replication = 0;
        int nullcount = 0;
        String line;
        while ((line = br.readLine()) != null) {


            if (line.startsWith("IN:")) {


                String local = line.substring(3);
                String[] split = local.split("\t");
                if (split.length < 2) {

                    // System.out.println(local + " less than 2 elements");
                    nullcount++;
                } else {

                    String MATCH = DRUG.get(split[drugpostion].replaceAll("[^A-Za-z0-9]", "").toLowerCase().trim()) + "\t" + PROTEIN.get(split[proteinposition].replaceAll("[^A-Za-z0-9]", "").toLowerCase().trim()) + "\t" + split[drugpostion] + "\t" + split[proteinposition];
                    allPairs.add(MATCH);
                    if (MATCH.contains("null")) {
                        nullcount++;

                        // System.out.println(MATCH);
                    } else {
                        if (uniquePairs.contains(MATCH)) {
                            replication++;
                        } else {
                            uniquePairs.add(MATCH);
                            count++;
                        }
                    }
                }
            }
        }

        System.out.println("[INFO] SUMMARY--------------");
        System.out.println("[INFO] "+(count + replication + nullcount) + " compound -> target pairs were parsed");
        System.out.println("[INFO] "+nullcount + " contained null values");
        System.out.println("[INFO] "+replication + " of these were repeats");
        System.out.println("[INFO] "+count + " were added to file as unique " + uniquePairs.size());
        System.out.println("[INFO] "+allPairs.size()+" were added to file as nonunique " );


        if (writeUnique == true) {
            BufferedWriter bw = new BufferedWriter(new FileWriter(op.getuniquePairsDirectoryPath() + q.getMotifName() + ".txt"));

            for (String mat : uniquePairs) {
                bw.append(mat + "\n");
            }

            bw.close();
        }

        if (writeNonUnique == true) {

            BufferedWriter bw2 = new BufferedWriter(new FileWriter(op.getALLPairsDirectoryPath() + q.getMotifName() + ".txt"));
            for (String mat : allPairs) {
                bw2.append(mat + "\n");
            }

            bw2.close();
        }



    }
}

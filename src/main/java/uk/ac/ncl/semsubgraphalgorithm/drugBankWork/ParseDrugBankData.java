/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.drugBankWork;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import uk.ac.ncl.semsubgraphalgorithm.OutPutManagament;

/**
 *
 * @author joemullen
 */
public class ParseDrugBankData {

    private Set<String> allCompoundsDATASET;
    private Set<String> allProteinsDATASET;
    private Set<String> allCompoundsDBv3;
    private Set<String> allProteinsDBv3;
    private Set<String> allValidUNIQUEPairsDB3;
    private Set<String> allUniquePairsDB3;
    private Map<String, String> DrugBank2NAMEsDATASET;
    private Map<String, String> Uniprot2NAMEsDATASET;
    private OutPutManagament op;

    public ParseDrugBankData() {
        this.allCompoundsDATASET = new HashSet<String>();
        this.allProteinsDATASET = new HashSet<String>();
        this.allCompoundsDBv3 = new HashSet<String>();
        this.allProteinsDBv3 = new HashSet<String>();
        this.allValidUNIQUEPairsDB3 = new HashSet<String>();
        this.allUniquePairsDB3 = new HashSet<String>();
        this.DrugBank2NAMEsDATASET = new HashMap<String, String>();
        this.Uniprot2NAMEsDATASET = new HashMap<String, String>();
        this.op = new OutPutManagament();

    }

    public void getDatassetInfo() throws FileNotFoundException, IOException {
        String DBfile = op.getDrugBankDirectoryPath();
        BufferedReader dbs = new BufferedReader(new FileReader(DBfile+ "con_listOP_55e0c.tsv"));
        String line;
        dbs.readLine();
        while ((line = dbs.readLine()) != null) {
            String[] split = line.split("\t");

            if (!split[4].equals("NO_DRUGBANK")) {
                allCompoundsDATASET.add(split[4]);

            }
            if (!split[5].equals("NO_UNIPROT")) {

                allProteinsDATASET.add(split[5]);
            }
            if (split.length > 1) {
                String DBID = split[4];
                String NAME = split[3];
                String UNIID = split[5];

                if (!DBID.equals("NO_DRUGBANK")) {

                    String fromName = NAME.replaceAll("[^A-Za-z0-9]", "").trim().toLowerCase();

                    DrugBank2NAMEsDATASET.put(fromName, DBID);
                }
                if (!UNIID.equals("NO_UNIPROT")) {
                    String toName = NAME.replaceAll("[^A-Za-z0-9]", "").trim().toLowerCase();
                    Uniprot2NAMEsDATASET.put(toName, UNIID);
                }
            }
        }
    }

    public void getDrugBankV3Info() throws FileNotFoundException, IOException {
        String DBfile = op.getDrugBankDirectoryPath();
        BufferedReader dbs = new BufferedReader(new FileReader(DBfile+ "DRUGBANKv3-Compounds2TargetsRELS.txt"));

        String line2;
        while ((line2 = dbs.readLine()) != null) {
            String[] split = line2.split("\t");
            String DBID = split[0];
            String unip = split[1];
            allCompoundsDBv3.add(DBID);
            allProteinsDBv3.add(unip);


        }
    }

    public void getValidPairsInfo() throws FileNotFoundException, IOException {
         String DBfile = op.getDrugBankDirectoryPath();
        BufferedReader validpairs = new BufferedReader(new FileReader(DBfile+ "ValidNONMATCHESfromdb3.txt"));
        String line2;
        while ((line2 = validpairs.readLine()) != null) {
            allValidUNIQUEPairsDB3.add(line2);
        }
    }
    

    public Map<String, String> getDrugBank2NAMEsDATASET() {
        return DrugBank2NAMEsDATASET;

    }

    public Map<String, String> getUniprot2NAMEsDATASET() {
        return Uniprot2NAMEsDATASET;

    }

    public Set<String> getallCompoundsDATASET() {

        return allCompoundsDATASET;
    }

    public Set<String> getallProtiensDATASET() {

        return allProteinsDATASET;
    }

    public Set<String> getallCompoundsDBv3() {

        return allCompoundsDBv3;
    }

    public Set<String> getallProteinsDBv3() {

        return allProteinsDBv3;
    }

    public Set<String> getallValidUNIQUEPairsDB3() {

        return allValidUNIQUEPairsDB3;
    }

    public Set<String> getallUniquePairsDB3() {

        return allUniquePairsDB3;
    }
}

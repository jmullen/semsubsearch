/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.drugBankWork;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author joemullen
 */
public class InstanceInformation {

    private Map<String, Integer> instanceInformation;
    private Map<String, Integer> overallInstanceInformation;
    private String fileIN;
    private String fileOUT;
    private boolean writeToFile;
    private boolean indivual;

    public static void main(String[] args) throws IOException {

        InstanceInformation ii = new InstanceInformation("Results/DB3WORK/SemSearch Results/Q5/OPALLPAIRS.txt", "Results/DB3WORK/test.txt", true, false);
        ii.multipleCheck();
        //ii.singleCheck();
        ii.printOut();

    }

    public InstanceInformation(String fileIN, String fileOUT, boolean writeToFile, boolean individual) {

        this.instanceInformation = new HashMap<String, Integer>();
        this.overallInstanceInformation = new HashMap<String, Integer>();
        this.fileIN = fileIN;
        this.fileOUT = fileOUT;
        this.writeToFile = writeToFile;
        this.indivual = individual;

    }

    public void singleCheck() {

        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(fileIN));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(InstanceInformation.class.getName()).log(Level.SEVERE, null, ex);
        }

        String line;
        try {
            while ((line = br.readLine()) != null) {

                if (instanceInformation.containsKey(line)) {
                    int local = instanceInformation.get(line) + 1;
                    instanceInformation.put(line, local);
                } else {
                    instanceInformation.put(line, 1);
                }

            }
        } catch (IOException ex) {
            Logger.getLogger(InstanceInformation.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void multipleCheck() throws IOException {


        File q1 = new File("Results/DB3WORK/SemSearch Results/Q1/instanceInformation.txt");
        File q3 = new File("Results/DB3WORK/SemSearch Results/Q3/instanceInformation.txt");
        File q4 = new File("Results/DB3WORK/SemSearch Results/Q4/instanceInformation.txt");
        File q5 = new File("Results/DB3WORK/SemSearch Results/Q5/instanceInformation.txt");

        Set<File> files = new HashSet<File>();
        files.add(q1);
        files.add(q3);
        files.add(q4);
        files.add(q5);

        for (File insFile : files) {

            BufferedReader br = null;
            try {
                br = new BufferedReader(new FileReader(insFile));
            } catch (FileNotFoundException ex) {
                Logger.getLogger(InstanceInformation.class.getName()).log(Level.SEVERE, null, ex);
            }

            String line;
            try {
                while ((line = br.readLine()) != null) {

                    String[] split = line.split("\t");
                    int occurrence = Integer.parseInt(split[0]);
                    if (split.length < 3) {
                        //System.out.println("to short: " + line);
                    } else {
                        String interaction = split[1] +"\t"+ split[2];


                        if (overallInstanceInformation.containsKey(interaction)) {
                            int already = overallInstanceInformation.get(interaction);
                            overallInstanceInformation.put(interaction, (already + occurrence));

                        } else {
                            overallInstanceInformation.put(interaction, occurrence);

                        }
                    }


                }
            } catch (IOException ex) {
                Logger.getLogger(InstanceInformation.class.getName()).log(Level.SEVERE, null, ex);
            }

        }


    }

    public void printOut() throws IOException {


        //max number that the instances will be sorted by
        int max = 1000;

        Map<String, Integer> map = null;

        if (indivual == true) {
            map = instanceInformation;
        } else {
            map = overallInstanceInformation;
        }

        BufferedWriter bw2 = new BufferedWriter(new FileWriter(fileOUT));

        for (int i = max; i > 0; i--) {

            for (String key : map.keySet()) {
                int occurrence = map.get(key);
                if (i == max) {
                    if (occurrence >= i) {

                        if (writeToFile == true) {

                            bw2.append(occurrence + "\t" + key + "\n");

                        } else {
                            System.out.println(occurrence + "\t" + key);
                        }
                    }

                } else {
                    if (occurrence == i) {

                        if (writeToFile == true) {

                            bw2.append(occurrence + "\t" + key + "\n");

                        } else {
                            System.out.println(occurrence + "\t" + key);
                        }
                    }
                }

            }
        }
    }
}

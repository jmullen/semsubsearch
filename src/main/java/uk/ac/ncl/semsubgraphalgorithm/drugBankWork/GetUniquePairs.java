/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.drugBankWork;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import uk.ac.ncl.semsubgraphalgorithm.OutPutManagament;

/**
 *
 * @author joemullen
 */
public class GetUniquePairs {

    private Set<String> proteins = new HashSet<String>();
    private Set<String> compounds = new HashSet<String>();
    private ParseDrugBankData pi;
    private OutPutManagament op;

    public GetUniquePairs() throws FileNotFoundException, IOException {
        this.pi = new ParseDrugBankData();
        pi.getDatassetInfo();
        pi.getDrugBankV3Info();
        this.op = new OutPutManagament();


    }

    public static void main(String[] args) throws FileNotFoundException, IOException {

        GetUniquePairs el = new GetUniquePairs();
        el.getCommonCompounds();
        el.getCommonProteins();
        el.ExtractMissingElements();
    }

    public void getCommonCompounds() throws FileNotFoundException, IOException {

        int NOMATCH = 0;
        int MATCH = 0;

        Set<String> alldb2 = pi.getallCompoundsDATASET();;

        Set<String> alldb3 = pi.getallCompoundsDBv3();

        for (String DBID : alldb3) {
            if (!alldb2.contains(DBID)) {
                //System.out.println("Not in both: " + DBID);
                NOMATCH++;
            } else {
                MATCH++;
                compounds.add(DBID);
            }


        }
        System.out.println("[INFO] ---------------------------Compounds");
        System.out.println("[INFO] In db2:  " + alldb2.size());
        System.out.println("[INFO] In db3:  " + alldb3.size());
        System.out.println("[INFO] Not in both total:  " + NOMATCH);
        System.out.println("[INFO] In both total:  " + MATCH);


    }

    public void getCommonProteins() throws FileNotFoundException, IOException {
        int NOMATCH = 0;
        int MATCH = 0;

        Set<String> alldb2 = pi.getallProtiensDATASET();
        Set<String> alldb3 = pi.getallProteinsDBv3();


        for (String DBID : alldb3) {
            if (!alldb2.contains(DBID)) {
                //System.out.println("Not in both: " + DBID);
                NOMATCH++;
            } else {
                MATCH++;
                proteins.add(DBID);
            }


        }
        System.out.println("[INFO]---------------------------Targets");
        System.out.println("[INFO] In db2:  " + alldb2.size());
        System.out.println("[INFO] In db3:  " + alldb3.size());
        System.out.println("[INFO] Not in both total:  " + NOMATCH);
        System.out.println("[INFO] In both total:  " + MATCH);

    }

    public void ExtractMissingElements() throws FileNotFoundException, IOException {

        int validPairs = 0;
        int invalidPairs = 0;
        String DBdirPath  = op.getDrugBankDirectoryPath();
        BufferedReader br = new BufferedReader(new FileReader(DBdirPath+"nomatchesdb3.txt"));

        BufferedWriter bw = new BufferedWriter(new FileWriter(DBdirPath+"VALIDnomatchesdb3.txt"));

        String line;
        while ((line = br.readLine()) != null) {
            String[] split = line.split("\t");
            if (compounds.contains(split[0]) && proteins.contains(split[1])) {
                validPairs++;
                  bw.append(line + "\n");
            } else {

                invalidPairs++;
            }

        }

        bw.close();

        System.out.println("[INFO]------------------Summary");
        System.out.println("[INFO] Valid pairs: " + validPairs);
        System.out.println("[INFO] Invalid pairs: " + invalidPairs);
    }
}

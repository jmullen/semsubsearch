/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.drugBankWork;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import uk.ac.ncl.semsubgraphalgorithm.OutPutManagament;

/**
 *
 * @author joemullen
 */
public class Comparing2ListsOfRelations {

    private ArrayList<String> pairs1 = new ArrayList<String>();
    private ArrayList<String> pairs2 = new ArrayList<String>();
    private ArrayList<String> nonmatchpairs1 = new ArrayList<String>();
    private ArrayList<String> nonmatchpairs2 = new ArrayList<String>();
    private ArrayList<String> allMATCHES = new ArrayList<String>();
    private Map<String, String> DB2DBACCS2NAME = new HashMap<String, String>();
    private Map<String, String> DB2UNIACCS2NAMES = new HashMap<String, String>();
    private Map<String, String> DB3DBACCSNAME = new HashMap<String, String>();
    private Map<String, String> DB3UNIACCS2NAMES = new HashMap<String, String>();
    private String file1;
    private String file2;
    private String idfile1;
    private String idfile2;
    private int unavailable = 0;
    private int repeatsDB2 = 0;
    private int singleDB2 = 0;
    private int singleDB3 = 0;
    private OutPutManagament op;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {


    }
    
    public Comparing2ListsOfRelations(String file1, String file1id, String file2, String file2id){
        
        this.file1 = file1;
        this.file2 = file2;
        this.idfile1 = file1id;
        this.idfile2 = file2id;
   
    
    }

    public void getfile1rels() throws FileNotFoundException, IOException {

        BufferedReader br = new BufferedReader(new FileReader(file1));

        String line;
        while ((line = br.readLine()) != null) {

            String[] split = line.split("\t");

            if (split.length > 1) {
                String from = split[0];
                String to = split[1];

                if (pairs1.contains((from + "," + to))) {
                    repeatsDB2++;
                } else {
                    pairs1.add(from + "\t" + to);
                }
            } else {
                singleDB2++;

            }
        }



    }

    public void getfile2rels() throws FileNotFoundException, IOException {

        BufferedReader br = new BufferedReader(new FileReader(file2));
        String line;
        while ((line = br.readLine()) != null) {

            String[] split = line.split("\t");

            if (split.length > 1) {
                String from = split[0];
                String to = split[1];
                
                if(to.equals("Not Available")){
                    unavailable ++;
                
                }

                pairs2.add(from + "\t" + to);
            } else {

                singleDB3++;

            }
        }



    }

    public void match() throws IOException {
        int matches = 0;
        int nomatchesDB2 = 0;
        int nomatchesDB3 = 0;
        for (String pair : pairs2) {
            if (pairs1.contains(pair)) {
                System.out.println("MATCH: " + pair);
                allMATCHES.add(pair);


            } else {
                nonmatchpairs2.add(pair);
            }

        }

        for (String pair : pairs1) {
            if (pairs2.contains(pair)) {
                if (!allMATCHES.contains(pair)) {
                    allMATCHES.add(pair);
                }


            } else {
                //System.out.println("NO MATCH");
                //nomatchesDB2 ++;
                nonmatchpairs1.add(pair);
            }

        }

        System.out.println("[INFO]--------------------------------SUMMARY");
        System.out.println("[INFO] "+allMATCHES.size() + "  MATCHES");
        System.out.println("[INFO]--------------------------------FILE 1");
        System.out.println("[INFO] "+pairs1.size() + " Pairs in file");
        System.out.println("[INFO] " + repeatsDB2 + " Repeated in file");
        System.out.println("[INFO] "+ singleDB2 + "  Not recognised");
        System.out.println("[INFO] " + nonmatchpairs1.size() + "  None matches");
        System.out.println("[INFO] "+ unavailable + " Includes a target that is unavailable");
        System.out.println("[INFO]--------------------------------FILE 2");
        System.out.println("[INFO] "+ pairs2.size() + "  Pairs in the file");
        System.out.println("[INFO] "+ singleDB3 + "  Not recognised");
        System.out.println("[INFO] "+ nonmatchpairs2.size() + "  None matches");



        BufferedWriter bw3 = new BufferedWriter(new FileWriter(op.getDrugBankDirectoryPath() + idfile1 + "&"+ idfile2+".txt"));
        for (String pair : allMATCHES) {
            bw3.append(pair + "\n");
        }
        bw3.close();

    }
    

    public Map<String, String> getDB2DBNAMES() {
        return DB2DBACCS2NAME;
    }

    public Map<String, String> getDB2UNINAMES() {
        return DB2UNIACCS2NAMES;
    }

    public Map<String, String> getDB3DBNAMES() {
        return DB3DBACCSNAME;
    }

    public Map<String, String> getDB3UNINAMES() {
        return DB3UNIACCS2NAMES;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.drugBankWork;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author joemullen
 */
public class GetSubgraphAccessions {
    
    Map<String, String> DRUG;
    Map<String, String> PROTEIN;
    ParseDrugBankData pi = new ParseDrugBankData();

    public static void main(String[] args) throws FileNotFoundException, IOException {
        GetSubgraphAccessions mm = new GetSubgraphAccessions();
        mm.mapp();
    }
    

    public GetSubgraphAccessions() throws FileNotFoundException, IOException {
        this.pi = new ParseDrugBankData();
        pi.getDatassetInfo();
        pi.getDrugBankV3Info();
        this.DRUG = pi.getDrugBank2NAMEsDATASET();
        this.PROTEIN = pi.getUniprot2NAMEsDATASET();

    }

    public void mapp() throws FileNotFoundException, IOException {
        
        int elements = 4;
        int drugpostion = 3;
        int proteinposition = 2;

        BufferedReader br = new BufferedReader(new FileReader("/Users/joemullen/Desktop/Results For Paper/SemSearch Results/CTCT/OP_CTCT.txt"));
        BufferedWriter bw = new BufferedWriter(new FileWriter("/Users/joemullen/Desktop/OPUNIQUPAIRS.txt"));

        Set<String> pairs = new HashSet<String>();
        int count = 0;
        int replication = 0;
        int nullcount = 0;
        String line;
        while ((line = br.readLine()) != null) {


            if (line.startsWith("NN:")) {


                String local = line.substring(3);
                String[] split = local.split("\t");
                if (split.length < 3) {

                    System.out.println(local + " less than 3 elements");
                    nullcount++;
                } else {

                    //CTT
                    //String MATCH = DRUG.get(split[0].replaceAll("[^A-Za-z0-9]", "").toLowerCase().trim()) + "\t" + PROTEIN.get(split[2].replaceAll("[^A-Za-z0-9]", "").toLowerCase().trim()) + "\t" + split[0] + "\t" + split[2];
                    //CCT
                   // String MATCH = DRUG.get(split[1].replaceAll("[^A-Za-z0-9]", "").toLowerCase().trim()) + "\t" + PROTEIN.get(split[2].replaceAll("[^A-Za-z0-9]", "").toLowerCase().trim()) + "\t" + split[1] + "\t" + split[2];
                     //CIT
                    String MATCH = DRUG.get(split[drugpostion].replaceAll("[^A-Za-z0-9]", "").toLowerCase().trim()) + "\t" + PROTEIN.get(split[proteinposition].replaceAll("[^A-Za-z0-9]", "").toLowerCase().trim()) + "\t" + split[drugpostion] + "\t" + split[proteinposition];
                    if (MATCH.contains("null")) {
                        nullcount++;

                        System.out.println(MATCH);
                    } else {
                        if (pairs.contains(MATCH)) {

                            replication++;
                        } else {
                            pairs.add(MATCH);
                            count++;
                        }
                    }
                }
            }
        }

        System.out.println("SUMMARY--------------");
        System.out.println("[INFO] "+(count + replication + nullcount) + " compound -> target pairs were parsed");
        System.out.println("[INFO] "+nullcount + " contained null values");
        System.out.println("[INFO] "+replication + " of these were repeats");
        System.out.println("[INFO] "+count + " were added to file as unique " + pairs.size());


        for (String mat : pairs) {
            bw.append(mat + "\n");
        }
        bw.close();

    }

  
}

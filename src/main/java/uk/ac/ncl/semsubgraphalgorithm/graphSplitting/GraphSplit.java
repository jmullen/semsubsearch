/*
 * Class takes a sub graph
 * If it has |V|  > 4 then the graph is split into graphs containing nodes of !> 4
 */
package uk.ac.ncl.semsubgraphalgorithm.graphSplitting;
/*
 * How can we quantify splitting the subgraphs?
 * Based on connectivity?!?!
 * Graph measure 
 */

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.jgrapht.DirectedGraph;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import uk.ac.ncl.semsubgraphalgorithm.GraphMethods;
import uk.ac.ncl.semsubgraphalgorithm.QueryGraph;
import uk.ac.ncl.semsubgraphalgorithm.RandomQuery;
import uk.ac.ncl.semsubgraphalgorithm.SerialisedGraph;
import uk.ac.ncl.semsubgraphalgorithm.SourceGraph;
import uk.ac.ncl.semsubgraphalgorithm.visualisation.GS_Core_Visualisation;

/**
 *
 * @author joemullen this is the implemtation of the GraphSplit algorithm
 * described in the paper. It takes a query graph and returns a set of query
 * graphs whereby every graph in the set has a nodeset of less than 4.
 */
public class GraphSplit extends GraphMethods {

    private QueryGraph q;
    private DirectedGraph<String, DefaultEdge> original;
    private UndirectedGraph<String, DefaultEdge> UDorigi;
    private Map<String, String> originalNodeSem;
    private Map<String, String> originalRelSem;
    private Set<GraphSplit_INSTANCE> newSubs;
    private HashMap<String, ArrayList<String>> nodeForeachGraph;
    private Set<DefaultEdge> extraEdges;
    private HashMap<String, int[]> edgset;
    private String mostConnected;
    private int subsizes = 3;
    private GraphSplit_INSTANCE splitGraph;
    private QueryGraph orig;
    private boolean draw;

    public static void main(String[] args) throws IOException, FileNotFoundException, ClassNotFoundException {
        //use random graph
        SerialisedGraph ser = new SerialisedGraph();
        SourceGraph source = ser.useSerialized("ran_1000_graph.data");
        // SourceGraph source = ser.useSerialized("graph.data");
        RandomQuery ran = new RandomQuery(source, 7, "Compound");
        QueryGraph six = ran.getRanQuery();

        //split query
        GraphSplit g = new GraphSplit(six, true);
    }

    public GraphSplit(QueryGraph s, boolean draw) {

        this.orig = s;
        this.q = s;
        this.original = s.getSub();
        this.originalNodeSem = s.getQueryNodeInfo();
        this.originalRelSem = s.getQueryRelInfo();
        this.newSubs = new HashSet<GraphSplit_INSTANCE>();
        this.extraEdges = new HashSet<DefaultEdge>();
        this.edgset = nameEdgeset(original);
        this.mostConnected = mostConnectedTop(edgset);
        this.UDorigi = new SimpleGraph<String, DefaultEdge>(DefaultEdge.class);
        this.nodeForeachGraph = new HashMap<String, ArrayList<String>>();
        this.splitGraph = new GraphSplit_INSTANCE();
        this.draw = draw;
        long startTime = System.currentTimeMillis();
        run();
        long endTime = System.currentTimeMillis();

        System.out.println("[INFO] Splitting large subgraph took " + (endTime - startTime) / 1000 + " seconds");
        System.out.println("[INFO] ----------------------------------------------------------");
        System.out.println(originalRelSem.toString());



    }

    public void run() {
        System.out.println("[INFO] Starting to split sub of size " + original.vertexSet().size());
        convertToUndirected();
        shortestPath();
        getGraphEdgeset(original);
        createSubs();
    }

    public GraphSplit_INSTANCE getSplit() {

        return splitGraph;
    }

    //converts the graph to that of an undirected graph
    public void convertToUndirected() {
        Set<String> allNodes = original.vertexSet();
        for (String no : allNodes) {
            UDorigi.addVertex(no);
        }
        Set<DefaultEdge> allEdges = original.edgeSet();
        for (DefaultEdge ed : allEdges) {
            String edgey = ed.toString().substring(1, ed.toString().length() - 1);
            String[] split = edgey.split(":");
            String from = split[0].trim();
            String to = split[1].trim();
            UDorigi.addEdge(from, to);

        }

    }

    @SuppressWarnings("unchecked")
    public void shortestPath() {
        Set<String> allNodes = UDorigi.vertexSet();
        String pair = "";
        int distance = 0;
        for (String no : allNodes) {
            String start = no;
            for (String no2 : allNodes) {
                DijkstraShortestPath tr2 = new DijkstraShortestPath(UDorigi, start, no2);
                int dis = tr2.getPathEdgeList().size();
                if (dis > distance) {
                    distance = dis;
                    pair = start + ">>" + no2;
                }

            }

        }

        String[] split2 = pair.split(">>");

        DijkstraShortestPath tr2 = new DijkstraShortestPath(UDorigi, mostConnected, split2[0]);
        int disOne = tr2.getPathEdgeList().size();
        String name1 = "one";
        ArrayList<String> nodes1 = nodesFromEdgeList(tr2.getPathEdgeList());
        nodeForeachGraph.put(name1, nodes1);


        DijkstraShortestPath tr3 = new DijkstraShortestPath(UDorigi, mostConnected, split2[1]);
        int distwo = tr3.getPathEdgeList().size();
        String name2 = "two";
        ArrayList<String> nodes2 = nodesFromEdgeList(tr3.getPathEdgeList());
        nodeForeachGraph.put(name2, nodes2);

        ArrayList<String> leftowa = nodesNotIncluded(nodesFromEdgeList(tr2.getPathEdgeList()), nodesFromEdgeList(tr3.getPathEdgeList()));
        allocatingLefOverNodes(leftowa);


    }

    public void createSubs() {
        Set<String> graphs = nodeForeachGraph.keySet();
        String addtoo = "";
        int count = 0;
        Set<DefaultEdge> allEdgeFromOrig = original.edgeSet();
        Set<DefaultEdge> added = new HashSet<DefaultEdge>();
        Set<DefaultEdge> NotADDED = new HashSet<DefaultEdge>();
        QueryGraph q1 = null;
        QueryGraph q2 = null;

        for (String gr : graphs) {
            count++;
            ArrayList<String> thisgraph = nodeForeachGraph.get(gr);
            DirectedGraph<String, DefaultEdge> name = new DefaultDirectedGraph<String, DefaultEdge>(DefaultEdge.class);
            for (String node : thisgraph) {
                name.addVertex(node);
            }

            for (DefaultEdge loc : allEdgeFromOrig) {
                String from = original.getEdgeSource(loc);
                String to = original.getEdgeTarget(loc);
                if (name.containsVertex(from) && name.containsVertex(to)) {
                    name.addEdge(from, to);
                    added.add(loc);
                }
            }

            if (count == 1) {
                q1 = new QueryGraph(name, gr, getSemNodeInfo(name), getSemRelInfo(name));
                getSemRelInfo(name);
                if (draw == true) {
                    q1.draw();
                }
            }

            if (count == 2) {
                q2 = new QueryGraph(name, gr, getSemNodeInfo(name), getSemRelInfo(name));
                getSemRelInfo(name);
                if (draw == true) {
                    q2.draw();
                }

            }
            System.out.println("[INFO] Created the sub: " + name.toString());

        }

        for (DefaultEdge ed : allEdgeFromOrig) {
            if (!added.contains(ed)) {
                NotADDED.add(ed);
            }

        }

        if (draw == true) {
            q.draw();
        }


        System.out.println("[INFO] Edges left over: " + NotADDED.toString());
        System.out.println("[INFO] Crossover node " + getCrossoverNode() + " with edgeset " + Arrays.toString(getCrossoverNodeEdgeset()));

        splitGraph = new GraphSplit_INSTANCE(q1, q2, orig, mostConnected);

    }

    public void allocatingLefOverNodes(ArrayList<String> notMatched) {

        for (String nod : notMatched) {
            System.out.println("Allocating: " + nod);
            //if the node is only connected to one other node add it to that graph
            if (rule1(nod) == true) {
                addRule1(nod);
                System.out.println("Added rule_1");
                //if it has a greater number of edges between one of the graphs
            } else if (!rule2(nod).equals("equal")) {
                String moreRelsInGraph = rule2(nod);
                addRule2(nod, moreRelsInGraph);
                System.out.println("Added rule_2");
                //if it has an equal number of edges between the 2 graphs add it to the smallest graph
            } else if (rule3(nod) == true) {
                addRule3(nod);
                System.out.println("Added rule_3");
            }
        }
    }

    public ArrayList<String> nodesNotIncluded(ArrayList<String> one, ArrayList<String> two) {
        ArrayList<String> local = new ArrayList<String>(one);
        local.addAll(two);

        ArrayList<String> noMatch = new ArrayList<String>();
        Set<String> all = original.vertexSet();
        for (String node : all) {
            if (!local.contains(node)) {
                noMatch.add(node);
            }
        }
        return noMatch;

    }

    public ArrayList<String> nodesFromEdgeList(List<DefaultEdge> edgelist) {
        ArrayList<String> nodes = new ArrayList<String>();

        int count = 0;
        for (DefaultEdge rel : edgelist) {
            String source = UDorigi.getEdgeSource(rel);
            String targ = UDorigi.getEdgeTarget(rel);
            count++;

            if (count == 1) {

                nodes.add(source);
                nodes.add(targ);

            } else {
                if (!nodes.contains(targ)) {
                    nodes.add(targ);
                } else {
                    nodes.add(source);
                }
            }

        }

        return nodes;
    }

    //calculates how many new subgraphs will (ideally) be created
    public int getNumberOfnewSubs(int graphSize) {

        int g = graphSize + 1;
        int newSubs = 1;
        for (int i = 0; i < 10; i++) {

            if (g < subsizes && g > 0) {
                newSubs = i + 1;
                break;
            }
            if (g == 0) {
                newSubs = i;
                break;
            } else {
                g = g - subsizes;
            }

        }

        //System.out.println("There will be this many new graphs " + newSubs);
        return newSubs;
    }

    public ArrayList<Integer> getSizeOfNewSubs(int numberofsubs, int graphsize) {
        ArrayList<Integer> insubsizes = new ArrayList<Integer>();
        int withOverlap = graphsize + (numberofsubs - 1);
        for (int i = 0; i < 10; i++) {

            if (withOverlap > subsizes) {
                insubsizes.add(subsizes);
            } else if (withOverlap < subsizes || withOverlap == subsizes) {
                insubsizes.add(withOverlap);
                break;

            }
            withOverlap = withOverlap - subsizes;
        }
        //System.out.println("Of these sizes " + insubsizes.toString());

        return insubsizes;

    }

    public Map<String, String> getSemNodeInfo(DirectedGraph<String, DefaultEdge> sub) {
        Map<String, String> ccs = new HashMap<String, String>();
        //get the vertex set of the new graph
        Set<String> vertex = sub.vertexSet();
        for (String node : vertex) {

            if (originalNodeSem.containsKey(node)) {

                ccs.put(node, originalNodeSem.get(node));

            }

        }

        return ccs;


    }

    public Map<String, String> getSemRelInfo(DirectedGraph<String, DefaultEdge> sub) {
        Map<String, String> ccs = new HashMap<String, String>();
        //get the edge set of the new graph
        Set<DefaultEdge> edge = sub.edgeSet();
        for (DefaultEdge ed : edge) {
            if (originalRelSem.containsKey(sub.getEdgeSource(ed) + ":" + sub.getEdgeTarget(ed))) {


                ccs.put(sub.getEdgeSource(ed) + ":" + sub.getEdgeTarget(ed), originalRelSem.get(sub.getEdgeSource(ed) + ":" + sub.getEdgeTarget(ed)));


            }

        }

        return ccs;


    }

    /*
     * Below we have all the rules by which the remaining nodes are allocated to 
     * the smaller graphs. This is detailed in the Supplementary Mterial of the 
     * accompanying paper.
     */
    public boolean rule1(String node) {
        boolean rule1 = false;
        if (UDorigi.degreeOf(node) == 1) {

            rule1 = true;
        }
        return rule1;
    }

    public void addRule1(String node) {
        Set<DefaultEdge> edgis = UDorigi.edgesOf(node);
        String targ = "";
        for (DefaultEdge s : edgis) {

            String src = UDorigi.getEdgeSource(s);
            String tar = UDorigi.getEdgeTarget(s);
            if (!node.equals(src)) {
                targ = src;
            } else {
                targ = tar;
            }
        }

        Set<String> graphs = nodeForeachGraph.keySet();
        String addtoo = "";


        for (String gr : graphs) {
            ArrayList<String> thisgraph = nodeForeachGraph.get(gr);
            if (thisgraph.contains(targ)) {
                addtoo = gr;
            }

        }

        //if the ON is the node that the node connects too that means
        //it can be connected to EITHER graph- in that case we want to
        //add it to the smallest
        if (targ.equals(getCrossoverNode())) {
            for (String gr : graphs) {
                //if it can be connected to both graphs than add it to the smallest
                if (nodeForeachGraph.get(gr).size() < nodeForeachGraph.get(addtoo).size()) {
                    addtoo = gr;

                }
            }
        }

        ArrayList<String> h = nodeForeachGraph.get(addtoo);
        h.add(node);
        nodeForeachGraph.put(addtoo, h);

    }

    public String rule2(String node) {
        String moreRels = "equal";
        //get the edges
        Set<DefaultEdge> ed = original.edgesOf(node);
        //see if how many edges attach to each graph
        int d1 = 0;
        int d2 = 0;
        String d1name = "";
        String d2name = "";

        //store the names for each graph
        int count = 0;
        for (String gr : nodeForeachGraph.keySet()) {
            if (count == 0) {
                d1name = gr;
            } else {
                d2name = gr;
            }
            count++;

        }

        //how many edges are there between the node to be allocated and the nodes in the first graph?
        for (String nod : nodeForeachGraph.get(d1name)) {

            if (original.containsEdge(nod, node)) {
                d1++;
            }

            if (original.containsEdge(node, nod)) {
                d1++;
            }

        }

        //how many edges are there between the node to be allocated and the nodes in the second graph?
        for (String nod : nodeForeachGraph.get(d2name)) {

            if (original.containsEdge(nod, node)) {
                d2++;
            }

            if (original.containsEdge(node, nod)) {
                d2++;
            }

        }

        if (d1 > d2) {
            moreRels = d1name;
        }

        if (d1 < d2) {
            moreRels = d2name;
        }

        return moreRels;
    }

    public void addRule2(String node, String graphName) {
        ArrayList<String> h = nodeForeachGraph.get(graphName);
        h.add(node);
        nodeForeachGraph.put(node, h);
    }

    public boolean rule3(String node) {
        boolean equal = false;
        //get the edges
        Set<DefaultEdge> ed = original.edgesOf(node);
        //see if how many edges attach to each graph
        int d1 = 0;
        int d2 = 0;
        String d1name = "";
        String d2name = "";

        //store the names for each graph
        int count = 0;
        for (String gr : nodeForeachGraph.keySet()) {
            if (count == 0) {
                d1name = gr;
            } else {
                d2name = gr;
            }
            count++;

        }

        //how many edges are there between the node to be allocated and the nodes in the first graph?
        for (String nod : nodeForeachGraph.get(d1name)) {

            if (original.containsEdge(nod, node)) {
                d1++;
            }

            if (original.containsEdge(node, nod)) {
                d1++;
            }

        }

        //how many edges are there between the node to be allocated and the nodes in the second graph?
        for (String nod : nodeForeachGraph.get(d2name)) {

            if (original.containsEdge(nod, node)) {
                d2++;
            }

            if (original.containsEdge(node, nod)) {
                d2++;
            }

        }

        if (d1 == d2) {
            equal = true;

        }
        return equal;

    }

    public void addRule3(String node) {


        String smallestGraph = "";
        int size = 100;
        for (String gr : nodeForeachGraph.keySet()) {
            if (nodeForeachGraph.get(gr).size() < size) {
                smallestGraph = gr;
                size = nodeForeachGraph.get(gr).size();

            }
        }
        ArrayList<String> h = nodeForeachGraph.get(smallestGraph);
        h.add(node);
        nodeForeachGraph.put(node, h);

    }
    /*
     * Get set methods
     */

    public String getCrossoverNode() {
        return mostConnected;
    }
    //get the edgeset of the most connected node as it is in the original subgraph

    public int[] getCrossoverNodeEdgeset() {
        return edgset.get(mostConnected);
    }

    public Set<GraphSplit_INSTANCE> getNewSubs() {
        return newSubs;
    }

    public Set<DefaultEdge> getExtraEdges() {

        return extraEdges;

    }

    public QueryGraph getOirignalQueryGraph() {
        return q;

    }
}

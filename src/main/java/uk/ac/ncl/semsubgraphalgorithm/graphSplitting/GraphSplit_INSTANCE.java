/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.graphSplitting;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Set;
import uk.ac.ncl.semsubgraphalgorithm.QueryGraph;
import uk.ac.ncl.semsubgraphalgorithm.QueryGraph.SemanticSub;
import uk.ac.ncl.semsubgraphalgorithm.RandomGraph;
import uk.ac.ncl.semsubgraphalgorithm.SemSearch;
import uk.ac.ncl.semsubgraphalgorithm.SerialisedGraph;
import uk.ac.ncl.semsubgraphalgorithm.SourceGraph;

/**
 *
 * @author joemullen
 */
public class GraphSplit_INSTANCE {

    public static void main(String[] args) throws IOException, FileNotFoundException, ClassNotFoundException, Exception {

   

    }
    QueryGraph q1;
    QueryGraph q2;
    QueryGraph orig;
    String crossover;
    boolean check = false;
    Set<QueryGraph> matches;

    public GraphSplit_INSTANCE() {
    }

    public GraphSplit_INSTANCE(QueryGraph q1, QueryGraph q2, QueryGraph orig, String crossoverNode) {


        if (q2.getSub().vertexSet().size() > q1.getSub().vertexSet().size()) {
            this.q1 = q2;
            this.q2 = q1;

        } else {
            this.q1 = q1;
            this.q2 = q2;

        }

        this.crossover = crossoverNode;
        this.orig = orig;

        System.out.println(
                "[INFO] Created new splitinstance");

    }

    public QueryGraph getQueryGraph1() {
        return q1;

    }

    public QueryGraph getQueryGraph2() {
        return q2;

    }

    public QueryGraph getOrig() {
        return orig;

    }

    public String getCrossoverNode() {

        return crossover;
    }

    public boolean checkSplitSizes() {
        boolean check = true;


        if (q1.getSub().vertexSet().size() > 4) {
            check = false;
        }

        if (q2.getSub().vertexSet().size() > 4) {
            check = false;
        }

        return check;

    }

    public boolean getCheck() {

        return check;

    }

    public void setCheck(boolean c) {

        this.check = c;

    }

    @Override
    public String toString() {

        String summary = "[INFO] Q1: " + q1.getSub().toString() + "  Q2: " + q2.getSub().toString() + "  OverlappingNode(ON): " + crossover;

        return summary;


    }

    public Set<QueryGraph> getMatches() {

        return matches;

    }

    public void SetMatches(Set<QueryGraph> s) {

        this.matches = s;


    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.graphSplitting;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.jgrapht.graph.DefaultEdge;
import uk.ac.ncl.semsubgraphalgorithm.GraphMethods;
import uk.ac.ncl.semsubgraphalgorithm.OutPutManagament;
import uk.ac.ncl.semsubgraphalgorithm.QueryGraph;
import uk.ac.ncl.semsubgraphalgorithm.RandomQuery;
import uk.ac.ncl.semsubgraphalgorithm.SemSearch;
import uk.ac.ncl.semsubgraphalgorithm.SerialisedGraph;
import uk.ac.ncl.semsubgraphalgorithm.SourceGraph;
import uk.ac.ncl.semsubgraphalgorithm.semanticDistanceSubgraphExclusion.SemanticDistanceCalculator;

/**
 *
 * @author joemullen
 */
public class RunSplit extends GraphMethods {

    private QueryGraph q1;
    private QueryGraph q2;
    private GraphSplit_INSTANCE si;
    private Set<QueryGraph> allQueries;
    private Set<QueryGraph> remainingQueries;
    private QueryGraph originalSub;
    private String getCrossoverNode;
    private Set<DefaultEdge> extraEdges;
    private ArrayList<String> allMatches1;
    private ArrayList<String> allMatches2;
    private SourceGraph source;
    private GraphSplit split;
    private String[] sub_order_1;
    private String[] sub_order_2;
    private int[] orderConverter;
    private ArrayList<String> mergedMatches;
    private ArrayList<String> scoredMatches;
    private SemanticDistanceCalculator sdc;
    private Map<String, String> origiSem;
    private double threshold;
    private long startTime;
    private long endTime;
    private double searchTime;
    private int semDepth;
    private String startingClass;
    private boolean SDC;
    private boolean closedWorld;
    private boolean outPut;
    private boolean noElementsBelowST;
    private String[] origNodeOrder;
    private String summary;
    private int failedClosedWorld;
    private int failedSDC;
    private Set<String> duplication;
    private OutPutManagament op;

    public static void main(String[] args) throws IOException, FileNotFoundException, ClassNotFoundException, Exception {


        //create query

        QueryGraph six = new QueryGraph(QueryGraph.SemanticSub.SIXSTAR);


        //use random graph
        SerialisedGraph ser = new SerialisedGraph();
        SourceGraph source = ser.useSerialized("ran_1000_graph.data");
        // SourceGraph source = ser.useSerialized("graph.data");
        //RandomQuery ran = new RandomQuery(source, 5, "Compound");
         //QueryGraph six = ran.getRanQuery();


        //split query
        GraphSplit g = new GraphSplit(six, true);

        //normal run
        System.out.println("------------------------------NORMAL RUN");
        SemSearch sem = new SemSearch(source, six, null, null, 8, "Compound", true, 0.8, true, true, true);
        sem.completeSearch();

        System.out.println(sem.getSuccesfulInitialNodes().toString());

        //split run with a normal 
        System.out.println("------------------------------SPLIT RUN");
        RunSplit rs2 = new RunSplit(source, g.getSplit(), 8, "any", true, 0.8, true, true, true);
        rs2.run();
        





    }

    public RunSplit(SourceGraph source, GraphSplit_INSTANCE s, int semDepth, String startClass, boolean SDC, double threshold, boolean closedWorld, boolean outPut, boolean noElementsBelowST) {


        this.startingClass = startingClass;
        this.semDepth = semDepth;
        this.SDC = SDC;
        this.closedWorld = closedWorld;
        this.outPut = outPut;
        this.noElementsBelowST = noElementsBelowST;
        this.si = s;
        this.originalSub = s.getOrig();
        this.getCrossoverNode = s.getCrossoverNode();
        this.q1 = s.getQueryGraph1();
        this.q2 = s.getQueryGraph2();
        this.source = source;
        this.mergedMatches = new ArrayList<String>();
        this.scoredMatches = new ArrayList<String>();
        this.sdc = new SemanticDistanceCalculator();
        this.origiSem = originalSub.getQueryNodeInfo();
        this.threshold = threshold;
        this.failedClosedWorld = 0;
        this.failedSDC = 0;
        this.duplication = new HashSet<String>();
        this.op = new OutPutManagament();


    }

    public void run() throws IOException, FileNotFoundException, ClassNotFoundException, Exception {


        startTime = System.currentTimeMillis();

        SemSearch splitrun = new SemSearch(source, q1, getOrigiSub(), null, semDepth, "none", SDC, threshold, closedWorld, outPut, noElementsBelowST);
        splitrun.completeSearch();

        allMatches1 = splitrun.getAllMatches();
        sub_order_1 = splitrun.getSubOrder();
        searchTime = Double.parseDouble(splitrun.getTime());

        // if we have no matches from the first search then there is no point continuing!
        if (allMatches1.size() == 0) {
            System.out.println("No matches found");

        } else {

            SemSearch splitrun2 = new SemSearch(source, q2, getOrigiSub(), splitrun.getSuccesfulInitialNodes(), semDepth, "none", SDC, threshold, closedWorld, outPut, noElementsBelowST);
            splitrun = null;
            splitrun2.completeSearch();
            searchTime = searchTime + Double.parseDouble(splitrun2.getTime());
            allMatches2 = splitrun2.getAllMatches();

            if (allMatches2.size() == 0) {
                System.out.println("No matches found");

            } else {
                sub_order_2 = splitrun2.getSubOrder();
                mergeMatches();
                getOrigOrder();
                scoreSubs();
            }
        }

        System.out.println(summary);
        endTime = System.currentTimeMillis();
        if (outPut == true) {
            writeMatchesToFile(op.getSearhchesDirectoryPath()+"[" + "_split_" + "]_ST" + threshold + "_" + (System.currentTimeMillis() / 100) + ".txt");
        }
    }

    public void mergeMatches() {

        int count = 0;

        for (String match : allMatches1) {

            String[] match1 = match.split("\t");
            for (String match2 : allMatches2) {
                String[] match3 = match2.split("\t");
                if (match1[0].equals(match3[0])) {
                    // String[] mergeArray = new String[originalSub.getSub().vertexSet().size()];
                    count++;
                    String merged = match;

                    for (int p = 1; p < match3.length; p++) {
                        merged = merged + "\t" + match3[p];
                    }
                    
                     if (match1[0].equals("5829")) {
                            System.out.println(merged);
                        }
                    if (duplicateCheck(merged.split("\t")) == true) {
                       
                        mergedMatches.add(merged);
                    }
                }
            }

        }

    }

    public void getOrigOrder() {

        origNodeOrder = new String[originalSub.getSub().vertexSet().size()];
        for (int k = 0; k < sub_order_1.length; k++) {
            origNodeOrder[k] = sub_order_1[k];

        }

        for (int k = 1; k < sub_order_2.length; k++) {
            origNodeOrder[k + (sub_order_1.length - 1)] = sub_order_2[k];
        }
    }

    public void scoreSubs() throws Exception {

        int failed = 0;

        for (String math : mergedMatches) {
            boolean check = true;
            String[] match = math.split("\t");


            //check for any repeated nodes
            if ((check == true)) {
                if (repeatabiltyCHECK(match) == false) {
                    check = false;
                }
            }

            //closedWorldFirst
            if (check == true) {
                if (closedWorldCHECK(match, origNodeOrder, source.getSourceGraph(), originalSub.getSub(), true, 2) == false) {
                    check = false;
                    failedClosedWorld++;
                }
            }

            //check SDC
            if (check == true) {
                if (scoreMatchBooleanCHECK(originalSub, source, origNodeOrder, match, originalSub.getQueryNodeInfo(), sdc, sdc.getAllConceptClasses(), SDC, noElementsBelowST, threshold, 2) == false) {

                    check = false;
                    failedSDC++;

                }
            }

            if (check == true) {
                if (!scoredMatches.contains(math)) {
                    scoredMatches.add(math);
                }
            } else {
                failed++;
            }

        }

        summary = "-----------------------------------------------------" + "\n" + "[SplitSearch Took " + searchTime + " seconds]" + "\n" + "[Found: " + scoredMatches.size() + " matches with ST: " + threshold + "]" + "\n" + "[Matches Failing Closed World: " + failedClosedWorld + "]" + "\n" + "[Matches Failing SDC: " + failedSDC + "]" + "\n" + "[Subgraph size= " + originalSub.getSub().vertexSet().size() + "/" + originalSub.getSub().edgeSet().size() + "]" + "\n" + "[TarGraphSize= " + source.getSourceGraph().vertexSet().size() + "/" + source.getSourceGraph().edgeSet().size() + "]" + "\n" + "[Sub_ORDER:" + Arrays.toString(origNodeOrder) + "]";


    }

    private boolean duplicateCheck(String[] match) {
        ArrayList<Integer> numbers = new ArrayList<Integer>();
        for (int i = 0; i < match.length; i++) {
            numbers.add(Integer.parseInt(match[i].trim()));
        }

        Collections.sort(numbers);
        if (duplication.contains(numbers.toString())) {

            return false;
        } else {

            duplication.add(numbers.toString());
        }

        return true;

    }

    public void writeMatchesToFile(String filename) throws IOException {

        FileWriter fstream = new FileWriter(filename);
        BufferedWriter bwtrue = new BufferedWriter(fstream);
        for (String match : scoredMatches) {
            match.replace("\\s", "\t");
            bwtrue.append(match + "\n");
        }

        bwtrue.close();
    }

    public ArrayList<String> getScoredMatches() {

        return scoredMatches;
    }

    public int[] getOrder() {
        return orderConverter;
    }

    public QueryGraph getOrigiSub() {

        return originalSub;
    }

    public String getMappingTime() {
        String time = (" " + (double) (endTime - startTime) / 1000);
        return time.trim();
    }

    public double getSearchTime() {
        return searchTime;

    }
}

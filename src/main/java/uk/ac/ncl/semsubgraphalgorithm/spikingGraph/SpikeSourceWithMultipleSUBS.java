/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.spikingGraph;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import uk.ac.ncl.semsubgraphalgorithm.QueryGraph;
import uk.ac.ncl.semsubgraphalgorithm.RandomGraph;
import uk.ac.ncl.semsubgraphalgorithm.SemSearch;
import uk.ac.ncl.semsubgraphalgorithm.SerialisedGraph;
import uk.ac.ncl.semsubgraphalgorithm.SourceGraph;


/**
 *
 * @author joemullen
 */
public class SpikeSourceWithMultipleSUBS {

    Set<QueryGraph> subsToSpike;
    SourceGraph integrated;
    private boolean plug;
    private int numberOfSpikes;

    public SpikeSourceWithMultipleSUBS(SourceGraph s, Set<QueryGraph> subs, int num, boolean plug) throws IOException {
        this.subsToSpike = subs;
        this.integrated = s;
        this.plug = plug;
        this.numberOfSpikes = num;
        spikeGraph();

    }

    public static void main(String args[]) throws IOException, FileNotFoundException, ClassNotFoundException {

        SerialisedGraph ser = new SerialisedGraph();
        SourceGraph source = ser.useSerialized("graph.data");
        //SemanticDistanceCalculator sdc = new SemanticDistanceCalculator();
        
        Set<QueryGraph> subs =  new HashSet<QueryGraph>();
        
        QueryGraph q = new QueryGraph(QueryGraph.SemanticSub.SIMCOMPSSMALL);
        subs.add(q);
        
        QueryGraph q1= new QueryGraph(QueryGraph.SemanticSub.SIMCOMSIMTAR);
        subs.add(q1);
        //GraphPruning gp4 = new GraphPruning(source, q, sdc, 0.8);
        RandomGraph sem = new RandomGraph(source, 1000, 1);
        SpikeSourceWithMultipleSUBS g = new SpikeSourceWithMultipleSUBS(sem.getRandomSourceGraph(), subs, 10, true);
        
        SemSearch sem11 = new SemSearch(g.getSpikedGraph(), q, null, null, 8, "Compound", true, 0.8, true, false, false);
        
        sem11.completeSearch();

    }

    public void spikeGraph() throws IOException {

        //for each of the semantic subs spike them to the sourcegraph
        //then set that as the sourcegraph in the class
        for (QueryGraph q : getSubsToSpike()) {
            if (getPlug() == true) {
                SpikeSourceGraph s = new SpikeSourceGraph(getSpikedGraph(), q, getNoOfSpikes(), getPlug());
                setSpiked(s.getSpikedGraph());
                setPlug(false);
            } else {
                SpikeSourceGraph s = new SpikeSourceGraph(getSpikedGraph(), q, getNoOfSpikes(), getPlug());
                setSpiked(s.getSpikedGraph());
            }

        }
    }

    public SourceGraph getSpikedGraph() {

        return integrated;
    }

    public void setSpiked(SourceGraph s) {

        this.integrated = s;
    }

    public Set<QueryGraph> getSubsToSpike() {

        return subsToSpike;
    }

    public int getNoOfSpikes() {

        return numberOfSpikes;
    }

    public void setPlug(boolean check) {
        this.plug = check;

    }

    public boolean getPlug() {
        return plug;
    }
}

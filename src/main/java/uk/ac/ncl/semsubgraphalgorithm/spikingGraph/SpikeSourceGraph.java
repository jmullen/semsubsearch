/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm.spikingGraph;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import uk.ac.ncl.semsubgraphalgorithm.QueryGraph;
import uk.ac.ncl.semsubgraphalgorithm.QueryGraph.SemanticSub;
import uk.ac.ncl.semsubgraphalgorithm.RandomGraph;
import uk.ac.ncl.semsubgraphalgorithm.SerialisedGraph;
import uk.ac.ncl.semsubgraphalgorithm.SourceGraph;


/**
 *
 * @author joemullen
 */
public class SpikeSourceGraph extends SourceGraph {

    private SourceGraph s;
    private DirectedGraph<String, DefaultEdge> newtarget;
    private String[][] newSemInfo;
    private HashMap<String, String> newAllRelations = new HashMap<String, String>();
    private Map<String, Set<String>> sourceRelRules = new HashMap<String, Set<String>>();
    private QueryGraph q;
    private DirectedGraph<String, DefaultEdge> sub;
    private Map<String, String> qNodeInfo = new HashMap<String, String>();
    private Map<String, String> qRelInfo = new HashMap<String, String>();
    private int numberOfSpikes;
    private SourceGraph spiked;
    private Random rand;
    private long startTime;
    private long endTime;
    private int nodesADDED;
    private int relsADDED;
    private ArrayList<String> allTO;
    private ArrayList<String> allFROM;
    private String CCFROM;
    private String CCTO;
    private boolean plug;
    private int plugged;
    private int succesfulSpikes;
    private boolean linkingEdgeFollowsRules;

    //want to be able to spike a source graph with x number of instances of 
    //a semantic sub q
    public SpikeSourceGraph(SourceGraph s, QueryGraph q, int num, boolean plug) throws IOException {

        this.s = s;
        this.newSemInfo = s.getAllCOnceptInfoCOPY();
        this.newAllRelations.putAll(s.getRelationInfo());
        this.sourceRelRules.putAll(s.getRelationTypeRules());
        this.newtarget = s.getSourceGraphCOPY();
        this.numberOfSpikes = num;
        this.q = q;
        this.qNodeInfo.putAll(q.getQueryNodeInfo());
        this.qRelInfo.putAll(q.getQueryRelInfo());
        this.sub = q.getSub();
        this.rand = new Random();
        this.CCFROM = q.getQueryNodeCC(q.getInferredFROM());
        this.CCTO = q.getQueryNodeCC(q.getInferredTO());
        this.plug = plug;
        this.allTO = new ArrayList<String>();
        this.allFROM = new ArrayList<String>();

        if (this.plug == true) {
            getAllRelevantNodes();
            plugRelations();
        }

        addAllSpikes();
        setNewSource();
        System.out.println(getSummary());

    }

    public void addAllSpikes() throws IOException {
        startTime = System.currentTimeMillis();
        for (int x = 0; x < spikes(); x++) {
            addSingleSpike(x);

        }
        endTime = System.currentTimeMillis();

    }

    public static void main(String[] args) throws IOException, FileNotFoundException, ClassNotFoundException, Exception {

        SerialisedGraph ser = new SerialisedGraph();
        SourceGraph source = ser.useSerialized("graph.data");

        //  source.getRelationTypeRules();

        QueryGraph q = new QueryGraph(SemanticSub.SIXNODE);
//  
        RandomGraph sem4 = new RandomGraph(source, 1000, 0.5);
        RandomGraph sem = new RandomGraph(source, 1000, 1);
        RandomGraph sem2 = new RandomGraph(source, 1000, 2);
        RandomGraph sem3 = new RandomGraph(source, 1000, 5);




    }

    public void addSingleSpike(int y) {

        //System.out.println("SoourceSemRules: "+sourceRelRules.toString());

        //new nodes added must have an associated node in the semantic subgraph
        Map<String, String> newNode2semNode = new HashMap<String, String>();


        //add nodes
        for (String node : sub.vertexSet()) {
            String id = getNodeID();
            String PID = "SPIKE";
            String conceptClass = qNodeInfo.get(node);
            String name = "SPIKE_" + q.getMotifName() + "_" + y + "_RANSPIKE";

            //add the node to the graph
            newtarget.addVertex(id);

            //add this node and it's associated sem node to the hashmap for 
            // adding relations
            newNode2semNode.put(node, id);

            //update the semantics of the graph
            newSemInfo = addSemanticNode(id, PID, conceptClass, name, newSemInfo);

            nodesADDED++;

        }

        //add edges
        for (DefaultEdge edge : sub.edgeSet()) {

            String fromSUB = sub.getEdgeSource(edge);
            String toSUB = sub.getEdgeTarget(edge);
            String queryRel = fromSUB + ":" + toSUB;
            String edgeType = qRelInfo.get(queryRel);

            String fromNEW = newNode2semNode.get(fromSUB);
            String toNEW = newNode2semNode.get(toSUB);

            newtarget.addEdge(fromNEW, toNEW);
            //System.out.println("Added relation: " + fromNEW + "/" + getNodeName(fromNEW) + "/" + getNodeCC(fromNEW) + " " + toNEW + "/" + getNodeName(toNEW) + "/" + getNodeCC(toNEW));
            newAllRelations = (HashMap<String, String>) addSemanticRel(fromNEW, toNEW, edgeType, newAllRelations);
            //System.out.println("Added reltype (SPIKE): "+ edgeType);
            relsADDED++;
            //need to add the relation semantics to the graph
        }

        //need a single edge that will link the semantic sub to the rest of the graph

        //int select2 = rand.nextInt(newNode2semNode.size());

        boolean check = false;
        int iterations = 0;
        while (check == false) {
            iterations ++;
            int count = 0;
            if (iterations == 50) {
                break;
            }
            int select1 = rand.nextInt(newNode2semNode.size());
            for (String sem : newNode2semNode.keySet()) {
                if (count == select1) {
                    check = addLinkingEdgeTO(newNode2semNode.get(sem));

                }
                count++;
            }
        }

        boolean check2 = false;
        int iterations2 = 0;
        while (check2 == false) {
            iterations2++;
            int count2 = 0;
            if (iterations2 == 50) {
                break;
            }
            int select2 = rand.nextInt(newNode2semNode.size());
            for (String sem : newNode2semNode.keySet()) {
                if (count2 == select2) {
                    check2 = addLinkingEdgeFROM(newNode2semNode.get(sem));

                }
                count2++;
            }
        }

        succesfulSpikes++;
    }

    public void getAllRelevantNodes() {


        for (String node : newtarget.vertexSet()) {

            if (getNodeCC(node).equals(CCFROM)) {
                allFROM.add(node);

            }

            if (getNodeCC(node).equals(CCTO)) {
                allTO.add(node);
            }
        }

    }

    public boolean addLinkingEdgeTO(String node) {

        String to = Integer.toString(rand.nextInt(newtarget.vertexSet().size() - 1) + 1);
        String edgeType = getRandomRelType(getNodeCC(to), getNodeCC(node));

        int count = 0;

        while (edgeType == null || to.equals("0") || getNodeName(to).startsWith("SPIKE") || canWeHave(to) == false) {
            if (count == 100) {
                return false;
                //break;
            }
            to = Integer.toString(rand.nextInt(newtarget.vertexSet().size() - 1) + 1);
            edgeType = getRandomRelType(getNodeCC(to), getNodeCC(node));
            //System.out.println("FROM CC:"+getNodeCC(to)+ "TOCC:"+getNodeCC(node));
            //System.out.println(edgeType);
            count++;

        }


        newtarget.addEdge(to, node);
        newAllRelations = (HashMap<String, String>) addSemanticRel(to, node, edgeType, newAllRelations);
        //System.out.println("Added LINK relation: "  + to + "/" + getNodeCC(to)+  " " +node + "/" + getNodeCC(node)+"/"+ getNodeName(node));
        relsADDED++;
        //System.out.println("TRUEEEEE");
        return true;


    }

    public boolean addLinkingEdgeFROM(String node) {

        String to = Integer.toString(rand.nextInt(newtarget.vertexSet().size() - 1) + 1);
        String edgeType = getRandomRelType(getNodeCC(node), getNodeCC(to));

        int count = 0;

        while (edgeType == null || to.equals("0") || getNodeName(to).startsWith("SPIKE") || canWeHave(to) == false) {
            if (count == 100) {
                return false;
                //break;
            }
            to = Integer.toString(rand.nextInt(newtarget.vertexSet().size() - 1) + 1);
            edgeType = getRandomRelType(getNodeCC(node), getNodeCC(to));
            //System.out.println("FROM CC:"+getNodeCC(to)+ "TOCC:"+getNodeCC(node));
            //System.out.println(edgeType);
            count++;

        }


        newtarget.addEdge(node, to);
        newAllRelations = (HashMap<String, String>) addSemanticRel(node, to, edgeType, newAllRelations);
        //System.out.println("Added LINK relation: "  + to + "/" + getNodeCC(to)+  " " +node + "/" + getNodeCC(node)+"/"+ getNodeName(node));
        relsADDED++;
        //System.out.println("TRUEEEEE");
        return true;


    }

    public boolean canWeHave(String node) {
        String conceptClass = getNodeCC(node);

        for (String cc : qNodeInfo.keySet()) {
            if (qNodeInfo.get(cc).equals(conceptClass)) {
                return false;

            }


        }

        return true;

    }

    public String getNodeID() {
        return Integer.toString(newSemInfo.length + 1);

    }

    //method creates relations between all nodes of type 'from' and all nodes 
    //of type 'to', meaning no more relations can be inferred other than those 
    //deleted
    public void plugRelations() {


        for (String node : allFROM) {

            for (String node2 : allTO) {

                if (!newtarget.containsEdge(node, node2)) {

                    newtarget.addEdge(node, node2);
                    String edgeType = getRandomRelType(getNodeCC(node), getNodeCC(node2));
                    newAllRelations = (HashMap<String, String>) addSemanticRel(node, node2, edgeType, newAllRelations);
                    // System.out.println("Added reltype (PLUG RELATIONS): "+ edgeType);
                    plugged++;
                }

            }
        }

    }

    public void setNewSource() throws IOException {
        spiked = new SourceGraph(newtarget, newSemInfo, newAllRelations);
    }

    public SourceGraph getSpikedGraph() {
        return spiked;
    }

    public int spikes() {
        return numberOfSpikes;
    }

    public int succesfulSpikes() {
        return succesfulSpikes;
    }

    public int calculateAdditionalNodes() {
        return q.getSub().vertexSet().size() * numberOfSpikes;
    }

    public String getSummary() {
        return "-----------------------------------------------------" + "\n" + "[SPIKEGRAPH TOOK: " + getTime() + " seconds]" + "\n" + "[ADDED: " + succesfulSpikes() + " instances of " + q.getMotifName() + "]" + "\n" + "[PLUGGED RELS: " + plugged + "]" + "\n" + "[NEW_GRAPH_SIZE: " + spiked.getSourceGraph().vertexSet().size() + "/" + spiked.getSourceGraph().edgeSet().size() + "]";

    }

    public String getTime() {
        String time = (" " + (double) (endTime - startTime) / 1000);
        return time.trim();
    }

    public String getRandomRelType(String ccfrom, String ccto) {


        String relType = "";
        String ccRel = ccfrom + ":" + ccto;
        //System.out.println("Looking for a rel between: " +  ccRel);
        if (sourceRelRules.containsKey(ccRel)) {
            Set<String> relTypes = sourceRelRules.get(ccRel);
            Random rand = new Random();
            relType = (String) relTypes.toArray()[rand.nextInt(relTypes.size())];
        } else {
            relType = null;
        }

        return relType;

    }

    public String getNodeCC(String nodeName) {
        //System.out.println("Calling concept Class for: "+ nodeName);
        int local = Integer.parseInt(nodeName);
        int local2 = local - 1;

        return newSemInfo[local2][2];
    }

    public String getNodeName(String node) {

        int local = Integer.parseInt(node);
        int local2 = local - 1;

        return newSemInfo[local2][3];

    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleDirectedGraph;

/**
 *
 * @author joemullen
 */
public class RandomQuery extends SourceGraph {

    private String[][] sourceNodeRules;
    private Map<String, Set<String>> sourceRelRules;
    private int size;
    private int edgsetSize;
    private QueryGraph random;
    private String[] ccsToPickFrom;
    private String startingConceptClass;
    private boolean edges;
    private long startTime;
    private long endTime;
    private Map<String, String> queryNODEInfo;
    private Map<String, String> queryRELInfo;

    public static void main(String[] args) throws IOException, FileNotFoundException, ClassNotFoundException {
        SerialisedGraph ser = new SerialisedGraph();
        SourceGraph source = ser.useSerialized("graph.data");
        RandomQuery rq = new RandomQuery(source, 7, "Compound");

    }

    //constructor that allows us to specify the edgeset of the subgrpah
    public RandomQuery(SourceGraph s, int size, String startingConceptClass, int edgeset) throws IOException {

        this.sourceNodeRules = s.getAverageConnectivityOfEachConceptClass();
        this.sourceRelRules = s.getRelationTypeRules();
        this.size = size;
        this.startingConceptClass = startingConceptClass;
        this.queryNODEInfo = new HashMap<String, String>();
        this.queryRELInfo = new HashMap<String, String>();
        this.edgsetSize = edgeset;

        getCCOccurrences();
        createRanQuery();

        while (random.getSub().edgeSet().size() != edgeset) {
            System.out.println("[INFO] Calling createRan again");
            queryNODEInfo = new HashMap<String, String>();
            queryRELInfo = new HashMap<String, String>();
            createRanQuery();
        }
        System.out.println(getSummary());

    }

    public RandomQuery(SourceGraph s, int size, String startingConceptClass) throws IOException {

        this.sourceNodeRules = s.getAverageConnectivityOfEachConceptClass();
        this.sourceRelRules = s.getRelationTypeRules();
        this.size = size;
        this.startingConceptClass = startingConceptClass;
        this.queryNODEInfo = new HashMap<String, String>();
        this.queryRELInfo = new HashMap<String, String>();

        getCCOccurrences();
        createRanQuery();
        if (size > 3) {
            while (random.getSub().vertexSet().size() >= random.getSub().edgeSet().size()) {
                System.out.println("[INFO] Calling createRan again");
                queryNODEInfo = new HashMap<String, String>();
                queryRELInfo = new HashMap<String, String>();
                createRanQuery();
            }
        } else if (size < 4) {
            while (random.getSub().vertexSet().size() > random.getSub().edgeSet().size()) {
                System.out.println("[INFO] Calling createRan again");
                queryNODEInfo = new HashMap<String, String>();
                queryRELInfo = new HashMap<String, String>();
                createRanQuery();
            }

        }
        System.out.println(getSummary());

    }

    private void createRanQuery() {

        startTime = System.currentTimeMillis();
        DirectedGraph<String, DefaultEdge> SemGraph = new SimpleDirectedGraph<String, DefaultEdge>(DefaultEdge.class);

        int name = 0;
        String nodename = startingConceptClass + name;

        //we want to look for compounds in the graph- we start each random
        //query graph by adding a node of type compound
        SemGraph.addVertex(nodename);
        queryNODEInfo.put(nodename, startingConceptClass);
        name++;
        Random rand = new Random();
        //we start at one- have already added one node
        for (int i = 1; i < size; i++) {
            Set<String> nodes = SemGraph.vertexSet();
            String[] nodesinArray = new String[nodes.size()];
            int count = 0;
            for (String nod : nodes) {
                nodesinArray[count] = nod;
                count++;

            }

            //randomly pick a node from the graph to create a relation from
            int rando = rand.nextInt(nodes.size());
            String fromName = nodesinArray[rando];
            String fromCC = queryNODEInfo.get(fromName);


            String[] rules = new String[1];
            for (int p = 0; p < sourceNodeRules.length; p++) {
                if (sourceNodeRules[p][0].equals(fromCC)) {

                    rules = sourceNodeRules[p][3].split(",");
                    break;

                }

            }




            String[][] rulespruned = new String[rules.length][2];
            int count2 = 0;
            int total = 0;

            // System.out.println("Searchingsmeinfo: " + Arrays.deepToString(seminfo));

            if (rules.length < 1) {
                createRanQuery();


            }


            for (int y = 0; y < rules.length; y++) {

                for (int u = 0; u < sourceNodeRules.length; u++) {

                    String[] split = rules[y].split("=");
                    if (sourceNodeRules[u][0].equals(split[0].trim())) {


                        rulespruned[y][0] = split[0];
                        rulespruned[y][1] = split[1];
                        total += Integer.parseInt(split[1]);
                        count2++;

                    }
                }

            }



            int prunedcount = 0;

            String[] possibleCCs = new String[total];
            if (rulespruned.length < 2) {
                //createRanQuery();
                break;

            } else {

                for (int j = 0; j < rulespruned.length; j++) {

                    for (int h = 0; h < Integer.parseInt(rulespruned[j][1]); h++) {

                        possibleCCs[prunedcount] = rulespruned[j][0];
                        prunedcount++;


                    }
                }


            }

            int rando3 = rand.nextInt(prunedcount);
            String toCC = possibleCCs[rando3];

            toCC = possibleCCs[rando3];

            nodename = toCC + name;
            SemGraph.addVertex(nodename);
            queryNODEInfo.put(nodename, toCC);
            name++;

            SemGraph.addEdge(fromName, nodename);
            String rt = getRandomRelType(getNodeCC(fromName), getNodeCC(nodename));
            queryRELInfo = addSemanticRel(fromName, nodename, rt, queryRELInfo);
            //check that there may be any symmetrical relations- if so need to add 
            //the closed world assumption will mean we miss any that do not include
            //this information
            if (checkForSymmetricalRelation(fromCC, toCC) == true) {
                SemGraph.addEdge(nodename, fromName);
                queryRELInfo = addSemanticRel(nodename, fromName, rt, queryRELInfo);

            }


        }

        if (SemGraph.vertexSet().size() != size) {
            queryNODEInfo = new HashMap<String, String>();
            queryRELInfo = new HashMap<String, String>();
            createRanQuery();
        } else {
            System.out.println("[INFO] Created graph with:" + SemGraph.vertexSet().size() + "/" + SemGraph.edgeSet().size());
            String mname = "RANDOM(" + SemGraph.vertexSet().size() + "-" + SemGraph.edgeSet().size() + ")";
            random = new QueryGraph(SemGraph, mname, queryNODEInfo, queryRELInfo);
            endTime = System.currentTimeMillis();
        }

    }

    private String getNodeCC(String node) {

        return queryNODEInfo.get(node);


    }

    private boolean checkForSymmetricalRelation(String fromCC, String toCC) {
        boolean add = false;

        //get the rules for the toNode
        String[] rules = new String[0];
        for (int p = 0; p < sourceNodeRules.length; p++) {
            if (sourceNodeRules[p][0].equals(toCC)) {

                rules = sourceNodeRules[p][3].split(",");
                break;

            }

        }
        //if the rules contains the from CC then we want to add another edge
        if (rules.length > 0) {
            for (int u = 0; u < rules.length; u++) {
                String[] split = rules[u].split("=");
                if (split[0].trim().equals(fromCC)) {

                    add = true;
                }

            }
        }

        //System.out.println("add= " + add);
        return add;
    }

    private boolean rulesContainsCC(String[] rules, String cc) {
        boolean check = false;

        for (int y = 0; y < rules.length; y++) {
            if (rules[y].equals(cc)) {
                check = true;
            }

        }


        return check;
    }

    //returns an array of length 100, with the correct number (percentage) of each concept 
    //class as appears in the large graph
    private void getCCOccurrences() {

        String[] ccs = new String[100];
        int count = 0;
        for (int i = 0; i < sourceNodeRules.length; i++) {
            String conceptClass = sourceNodeRules[i][0];
            //System.out.println(conceptClass);
            int frequency = Integer.parseInt(sourceNodeRules[i][1]);
            //System.out.println(frequency);
            for (int y = 0; y < frequency; y++) {
                if (count < 10) {
                    ccs[count] = conceptClass;
                    count++;
                }
            }

        }

        ccsToPickFrom = ccs;

    }

    public QueryGraph getRanQuery() {

        return random;

    }

    public String[][] getSemInfo() {

        return sourceNodeRules;
    }

    public String getRandomRelType(String ccfrom, String ccto) {

        String relType = "";
        String ccRel = ccfrom + ":" + ccto;
        Set<String> relTypes = sourceRelRules.get(ccRel);

        Random rand = new Random();
        relType = (String) relTypes.toArray()[rand.nextInt(relTypes.size())];

        return relType;

    }

    public String getSummary() {
        return "-----------------------------------------------------" + "\n" + "[Random Query Graph took " + (double) (endTime - startTime) / 1000 + " seconds]" + "\n" + "[Size: " + random.getSub().vertexSet().size() + "/" + random.getSub().edgeSet().size() + "]" + "\n" + "[NODE_INFO: " + queryNODEInfo.toString() + "]" + "\n" + "[REL_INFO: " + queryRELInfo.toString() + "]";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import uk.ac.ncl.semsubgraphalgorithm.SerialisedGraph;
import uk.ac.ncl.semsubgraphalgorithm.SourceGraph;

/**
 *
 * @author joemullen
 */
public class SourceGraph extends GraphMethods implements java.io.Serializable, Cloneable {

    String[][] conceptInfo;
    HashMap<String, String> alllRelations;
    private File nodeInfo = null;
    private File relationInfo = null;
    private DirectedGraph<String, DefaultEdge> source = null;
    private static final long serialVersionUID = 33326404614661473L;

//    public static void main(String[] args) throws IOException {
//        //SourceGraph p = new SourceGraph();
//
//        SourceGraph p = new SourceGraph(new File("/Users/joemullen/Desktop/OP_tab_exporter/con_listOP_35647.tsv"), new File("/Users/joemullen/Desktop/OP_tab_exporter/rel_listOP_35647.tsv"));
//        DirectedGraph<String, DefaultEdge> source = p.createSourceGraph();
//
//        QueryGraph q = new QueryGraph();
//        DirectedGraph<String, DefaultEdge> query = q.chlorpromMotif();
//
//        TopSearch s = new TopSearch(query, source);
//        s.completeSearch();
//        s.printOutMatches();
//
//
//    }
    public SourceGraph(File nodeInfo, File relationInfo) throws IOException {
        this.nodeInfo = nodeInfo;
        this.relationInfo = relationInfo;
        this.source = createSourceGraph();

    }

//      public void SerializeGraph(SourceGraph s) throws FileNotFoundException, IOException {
//        try (FileOutputStream f_out = new FileOutputStream("graph.data")) {
//            ObjectOutputStream obj_out = new ObjectOutputStream(f_out);
//            //obj_out.writeObject(s);
//            obj_out.writeObject(s);
//            obj_out.close();
//        }
//        System.out.printf("Serialized graph is saved as...");
//
//    }
    public SourceGraph() throws IOException {
    }

    public SourceGraph(DirectedGraph<String, DefaultEdge> graph, String[][] SemInfo, HashMap<String, String> alllRelations) throws IOException {
        this.alllRelations = alllRelations;
        this.conceptInfo = SemInfo;
        this.source = graph;
    }

    public SourceGraph getSourceGraphInstance() throws IOException {

        return new SourceGraph(source, conceptInfo, alllRelations);
    }

    public DirectedGraph<String, DefaultEdge> getSourceGraph() {
        return source;

    }

    public DirectedGraph<String, DefaultEdge> getSourceGraphCOPY() {

        DirectedGraph<String, DefaultEdge> copy = new DefaultDirectedGraph<String, DefaultEdge>(
                DefaultEdge.class);

        Set<String> nodes = source.vertexSet();
        for (String nod : nodes) {
            copy.addVertex(nod);
        }

        Set<DefaultEdge> edges = source.edgeSet();
        for (DefaultEdge de : edges) {
            copy.addEdge(source.getEdgeSource(de), source.getEdgeTarget(de));
        }
        return copy;

    }

    public DirectedGraph<String, DefaultEdge> createSourceGraph() throws IOException {

        //if no files have been passed then we create the test graph
        if (nodeInfo != null && relationInfo != null) {
            try {
                DirectedGraph<String, DefaultEdge> targ3 = populateEmptyGraph(getConceptName(getConceptInfo(nodeInfo)),
                        getStoredRelations());
                return targ3;
            } catch (FileNotFoundException fe) {
                System.out.println(fe);
            }
        } else {

            return definedGraph();
        }
        return null;
    }

    // this method extracts all info regarding nodes from the file; sotoring them in conceptInfo array
    public String[][] getConceptInfo(File nodeInfo) throws FileNotFoundException {

        String concep = "";
        String concep2 = "";
        String concep3 = "";
        String concep4 = "";

        Scanner x = new Scanner(nodeInfo);

        x.nextLine();
        int count = 0;
        while (x.hasNextLine()) {

            String line = x.nextLine();
            //System.out.println(line+ "\n");
            String[] temp = line.split("\t");
            //System.out.println("TEMP, length: "+ temp.length+ Arrays.deepToString(temp));
            String ID = temp[0];
            String PID = temp[1];
            String ConceptType = temp[2];
            String name = "";
            if (temp.length > 3) {
                name = temp[3];
            } else {
                name = "NONE_ASSIGNED";
            }
            concep += (ID + ":");
            concep2 += (PID + ":");
            concep3 += (ConceptType + ":");
            concep4 += (name + ":");
            count++;
            if (count % 5000 == 0) {
                System.out.println("[INFO] Vertices Parsed: " + count);
            }
        }

        String[] ids = concep.trim().split(":");
        String[] pids = concep2.trim().split(":");
        String[] contype = concep3.trim().split(":");
        String[] names = concep4.trim().split(":");

        int size = ids.length;
        String[][] allIds = new String[size][4];

        for (int f = 0; f < size; f++) {
            allIds[f][0] = ids[f];
            allIds[f][1] = pids[f];
            allIds[f][2] = contype[f];
            allIds[f][3] = names[f];
        }

        conceptInfo = allIds;

        return allIds;
    }

    // extracts all the relations from the relationInfo file and stores them in alllRelations
    public String[] getStoredRelations() throws IOException {
        HashMap<String, String> alllRelations2 = new HashMap<String, String>();

        BufferedReader br = new BufferedReader(new FileReader(relationInfo));
        String relType = "";
        String line;
        // String relations = "";
        String allEdges = "";
        int count = 0;

        while ((line = br.readLine()) != null) {

            if (line.startsWith(">>>")) {
                relType = line.substring(3, line.length());
                //System.out.println("relationtype: " + relType);
            } else {
                String line2 = line.trim();
                String[] allRelations = line2.split("\t");
                for (int y = 0; y < allRelations.length; y++) {
                    String localRel = allRelations[y].trim();
                    //System.out.println(localRel + " ");
                    allEdges = allEdges + " " + localRel;
                    if (count % 10000 == 0 && count > 0) {
                        System.out.println("[INFO] Edges Parsed: " + count);
                    }
                    //count++;
                    //System.out.println(count);
                    if (!alllRelations2.containsKey(localRel)
                            && !localRel.equals("")) {
                        // count ++;
                        // System.out.println("why are you not getting count? "+
                        // count);
                        alllRelations2.put(localRel, relType);
                        count++;
                        //System.out.println("added");
                    } else if (!localRel.equals("")) {
                        String add = alllRelations2.get(localRel);
                        add = add + " " + relType;
                        alllRelations2.put(localRel, add);
                        count++;
                    }
                }

            }

            //System.out.println(count);
        }
        alllRelations = alllRelations2;
        //System.out.println(alllRelations.get("47:35"));
        //System.out.println(alllRelations.toString());
        String allOfThem = allEdges.trim();
        String[] allRelationsArray = allOfThem.split(" ");
        //System.out.println("allRelations: "
        // + Arrays.deepToString(allRelationsArray));
        br.close();
        return allRelationsArray;
    }

    // this method takes a list of nodes and relations aextracted in the previous method to create a SourceGraph 
    public static DirectedGraph<String, DefaultEdge> populateEmptyGraph(
            String[] nodes, String[] relations) {

        DirectedGraph<String, DefaultEdge> graph = new DefaultDirectedGraph<String, DefaultEdge>(
                DefaultEdge.class);


        for (String node : nodes) {
            graph.addVertex(node);

        }
        for (String rel : relations) {
            if (rel.equals("")) {
            } else {

                String[] local = rel.split(":");
                String source = local[0];
                String target = local[1];
                if (graph.containsVertex(source) && graph.containsVertex(target)) {
                    //System.out.println("sour: "+ source +" tar:" +target);
                    graph.addEdge(source, target);
                } else {
                    //System.out.println("graph does not coontain the vertex " + source + " or " + target);

                }
            }



        }
        //System.out.println("Network produced: " + graph.toString());
        //return network2;
        return graph;
    }
//allows a candidate set to be returned that matches a particular conceptClass

    public ArrayList<String> firstSemanticMatch(ArrayList<String> initialNodes,
            String conceptClass) throws IOException {
        ArrayList<String> matches = new ArrayList<String>();

        if (conceptClass.equals("Comp") || conceptClass.equals("Compund")) {

            for (String s : initialNodes) {
                String[] local = s.split(",,,");

                String node = local[1].trim();

                if (getConceptClass(node).equals("Comp") || getConceptClass(node).equals("Compound")) {
                    matches.add(s);
                }
                // .out.println(s);
            }



        } else {

            for (String s : initialNodes) {
                String[] local = s.split(",,,");

                String node = local[1].trim();

                if (getConceptClass(node).equals(conceptClass)) {
                    matches.add(s);
                }
                // .out.println(s);
            }
        }

        FileWriter fstream = new FileWriter("infoOnInitalNodes.txt");
        BufferedWriter out = new BufferedWriter(fstream);

        out.append("Initial Nodes with degree >= most connected subgraph node: "
                + initialNodes.size() + "\n");
        out.append("After applying the Semantic match (" + conceptClass + "): "
                + matches.size() + "\n");

        out.close();

        return matches;

    }

    //this method returns the relation type, taking the from as well as to node as parameters
    public String getRelationType(String v1, String v2) {
        //System.out.println("called the get relationtype method on "+ v1+v2);

        String edge = v1 + ":" + v2;
        //System.out.println(v1+"  "+v2);

        String relationType = "";

        if (alllRelations.containsKey(edge)) {
            relationType = alllRelations.get(edge);
            //System.out.println("found: " + relationType);
        } else {
            relationType = "no_relation_found";
        }
        //System.out.println("relation returned: "+ relationType);
        return relationType;

    }

    //the follwing methods are used to extract information regarding nodes
    public String[][] getAllCOnceptInfo() {

        return conceptInfo;

    }

    public String[][] getAllCOnceptInfoCOPY() {

        String[][] copy = new String[conceptInfo.length][4];
        for (int i = 0; i < conceptInfo.length; i++) {
            for (int p = 0; p < 4; p++) {
                copy[i][p] = conceptInfo[i][p];
            }

        }

        return copy;

    }

    public HashMap<String, String> getRelationInfo() {

        return alllRelations;
    }

    public String[][] getConceptNames(String[][] nodeNames) throws IOException {

        String[][] conceptClasses = new String[nodeNames.length][nodeNames[0].length];
        for (int y = 0; y < nodeNames.length; y++) {
            if (nodeNames[y][0] == null) {
                break;
            } else {

                String[] local = nodeNames[y];
                for (int x = 0; x < local.length; x++) {

                    String conName = getConceptName(local[x]);
                    if (conName.equals("")) {
                        conName = "NO_NAME_ASSIGNED";
                    }
                    conceptClasses[y][x] = conName;
                }

            }
        }

        return conceptClasses;

    }

    public String getConceptName(String nodeName) {
        int local = Integer.parseInt(nodeName);
        int local2 = local - 1;
        return conceptInfo[local2][3].trim();
    }

    public String[] getConceptName(String[][] allInfo) {
        String[] conceptNames = new String[allInfo.length];
        for (int i = 0; i < allInfo.length; i++) {
            conceptNames[i] = allInfo[i][0];

        }
        return conceptNames;

    }

    /**
     * Get the relation types of a match ready for scoring
     *
     * @param match
     * @return
     */
    public String[] getMatchRelationsRELTYPE(String[] match) {

        String[] local = match;
        String[] reltypes = new String[match.length];
        for (int x = 0; x < local.length; x++) {
            String[] split = local[x].split(":");
            String first = split[0].trim();
            String second = split[1].trim();
            String relationType = getRelationType(first, second);
            reltypes[x] = relationType;
        }
        return reltypes;
    }

    public String getConceptClass(String nodeName) {
        int local = Integer.parseInt(nodeName);
        int local2 = local - 1;
        return conceptInfo[local2][2];
    }
//    

    public String[][] getConceptClass(String[][] nodeNames) throws IOException {

        String[][] conceptClasses = new String[nodeNames.length][nodeNames[0].length];
        for (int y = 0; y < nodeNames.length; y++) {
            if (nodeNames[y][0] == null) {
                break;
            } else {

                String[] local = nodeNames[y];
                for (int x = 0; x < local.length; x++) {

                    String conClass = getConceptClass(local[x]);
                    conceptClasses[y][x] = conClass;
                }

            }
        }

        return conceptClasses;

    }

    public String[] getConceptClass(String[] local) {
        String[] ConIDs = new String[local.length];
        for (int i = 0; i < ConIDs.length; i++) {
            ConIDs[i] = getConceptClass(local[i]);
        }
        return ConIDs;
    }

    //takes a String [] of relations and returns their types
    public String[][] getRelationType(String[][] relationsNames) {

        String[][] relationTypes = new String[relationsNames.length][relationsNames[0].length];

        if (relationsNames.length > 0) {

            for (int y = 0; y < relationsNames.length; y++) {
                if (relationsNames[y][0] == null) {
                    break;
                } else {

                    String[] local = relationsNames[y];

                    for (int x = 0; x < local.length; x++) {
                        String[] split = local[x].split(":");
                        String first = split[0].trim();
                        String second = split[1].trim();
                        String relationType = getRelationType(first, second);
                        relationTypes[y][x] = relationType;
                    }

                }

            }
        } else {
            System.out
                    .println("[INFO] No matches have been found: \"failed to getRelationTypes\"");
        }

        return relationTypes;

    }

    //here we have a test graph; that has been predefined
    public DirectedGraph<String, DefaultEdge> definedGraph() {
        DirectedGraph<String, DefaultEdge> network = new DefaultDirectedGraph<String, DefaultEdge>(
                DefaultEdge.class);

        for (int i = 0;
                i <= 5; i++) {

            network.addVertex("DRUG (Chlo)" + "  " + i);
            network.addVertex("DRUG (Trim)" + "  " + i);
            network.addVertex("TARGET" + "  " + i);
            network.addVertex("Extra" + "  " + i);
            network.addVertex("Extra2" + "  " + i);
            network.addVertex("DRUG" + "  " + i);
            network.addVertex("TARGET" + "  " + i);
            network.addVertex("PROTEIN_1" + "  " + i);
            network.addVertex("PROTEIN_2" + "  " + i);
            network.addVertex("DISEASE_1" + "  " + i);
            network.addVertex("DISEASE_2" + "  " + i);


            network.addEdge(("DRUG" + "  " + i), ("TARGET" + "  " + i));
            network.addEdge(("TARGET" + "  " + i), ("PROTEIN_1" + "  " + i));
            network.addEdge(("PROTEIN_1" + "  " + i), ("PROTEIN_2" + "  " + i));
            network.addEdge(("PROTEIN_2" + "  " + i), ("PROTEIN_1" + "  " + i));
            network.addEdge(("PROTEIN_1" + "  " + i), ("DISEASE_1" + "  " + i));
            network.addEdge(("PROTEIN_2" + "  " + i), ("DISEASE_2" + "  " + i));
            network.addVertex("Extra3" + "  " + i);
            network.addVertex("Extra4" + "  " + i);
            network.addVertex("Extra5" + "  " + i);
            network.addVertex("Extra6" + "  " + i);
            network.addVertex("Extra7" + "  " + i);

            //subgraph.addEdge(("DRUG"), ("TARGET"));
            //subgraph.addEdge(("TARGET"), ("PROTEIN_1"));
            //subgraph.addEdge(("PROTEIN_1"), ("PROTEIN_2"));
            //subgraph.addEdge(("PROTEIN_2"), ("PROTEIN_1"));
            //subgraph.addEdge(("PROTEIN_1"), ("DISEASE_1"));
            //subgraph.addEdge(("PROTEIN_2"), ("DISEASE_2"));

            // h.addVertex(v3);

            // add edges to create a circuit
            network.addEdge(("DRUG (Chlo)" + "  " + i),
                    ("PROTEIN_2" + "  " + i));
            network.addEdge(("DRUG (Chlo)" + "  " + i),
                    ("DRUG (Trim)" + "  " + i));
            network.addEdge(("DRUG (Trim)" + "  " + i),
                    ("DRUG (Chlo)" + "  " + i));
            //network.addEdge(("DRUG (Trim)" + "  " + i), ("TARGET" + "  " + i));
            //network.addEdge(("DRUG (Chlo)" + "  " + i), ("TARGET" + "  " + i));
            //network.addEdge(("Extra" + "  " + i), ("TARGET" + "  " + i));
            network.addEdge(("DRUG (Chlo)" + "  " + i), ("Extra2" + "  " + i));
            network.addEdge(("Extra" + "  " + i), ("DRUG (Chlo)" + "  " + i));
            network.addEdge(("Extra3" + "  " + i), ("Extra4" + "  " + i));
            network.addEdge(("Extra4" + "  " + i), ("Extra5" + "  " + i));
            network.addEdge(("Extra5" + "  " + i), ("Extra6" + "  " + i));
            network.addEdge(("Extra3" + "  " + i), ("Extra7" + "  " + i));
            network.addEdge(("Extra5" + "  " + i), ("Extra6" + "  " + i));
            network.addEdge(("Extra6" + "  " + i), ("Extra7" + "  " + i));


            if (i >= 1) {
//				network.addEdge(("DRUG" + "  " + i), ("TARGET" + "  " + (i-1)));
//			}
                network.addEdge(("TARGET" + "  " + i),
                        ("TARGET" + "  " + (i - 1)));
                network.addEdge(("Extra" + "  " + i),
                        ("DRUG (Chlo)" + "  " + (i - 1)));
                network.addEdge(("Extra2" + "  " + i),
                        ("DRUG (Chlo)" + "  " + (i - 1)));
                network.addEdge(("Extra2" + "  " + i),
                        ("Extra3" + "  " + (i - 1)));
                network.addEdge(("DRUG (Chlo)" + "  " + i),
                        ("Extra3" + "  " + (i - 1)));

            }
//
        }

        System.out.println(
                "[INFO] DEFINED GRAPH PRODUCED: " + "\n" + network.toString());
        return network;
    }

    public static DirectedGraph<String, DefaultEdge> randomDirectedGraph(
            Set<String> nodes, ArrayList<String> relations) {

        DirectedGraph<String, DefaultEdge> graph = new DefaultDirectedGraph<String, DefaultEdge>(
                DefaultEdge.class);


        for (String node : nodes) {
            graph.addVertex(node);

        }
        for (String rel : relations) {
            if (rel.equals("")) {
            } else {
                String[] local = rel.split(":");
                String source = local[0];
                String target = local[1];
                //System.out.println("sour: " + source + " tar:" + target);
                graph.addEdge(source, target);
            }

        }

        //System.out.println("EDGESET : " + graph.edgeSet().toString());
        //System.out.println(
        //    "Network produced: " + graph.toString());
        //return network2;
        return graph;
    }

    public HashMap<String, Integer> getCCFrequency() {
        HashMap<String, Integer> freq = new HashMap<String, Integer>();

        //get the frequency of each concept class
        int total = 0;
        for (int i = 0; i < conceptInfo.length; i++) {
            String cc = conceptInfo[i][2];
            if (freq.containsKey(cc)) {
                int fre = freq.get(cc);
                fre += 1;
                freq.put(cc, fre);
                total++;

            } else {
                freq.put(cc, 1);
                total++;
            }
        }


        //then work this out as a percentage of the total

        int totalpercentage = 0;
        int count = 0;
        Set<String> ccnames = freq.keySet();
        for (String cc : ccnames) {
            count++;
            int fre = freq.get(cc);
            float percent = (fre / (float) total) * 100;
            int per = (int) Math.floor(percent);

            //some are less than 1; want them present in the graph
            if (per == 0) {
                per = 1;
            }
            //don't want more than 100 percent!!!
            if (count == ccnames.size()) {

                per = 100 - totalpercentage;
            }
            totalpercentage += per;
            freq.put(cc, per);

        }
        // System.out.println(freq.toString());
        return freq;

    }

    public Set<String> getAllCCs() {

        Set<String> allConceptClasses = new HashSet<String>();
        for (int i = 0; i < conceptInfo.length; i++) {
            allConceptClasses.add(conceptInfo[i][2]);
        }

        //System.out.println("All Concept Classes: " + allConceptClasses.toString());
        return allConceptClasses;

    }

    public int getGraphEdgesetSize() {
        return source.edgeSet().size();

    }

    public int getGraphNodeSize() {
        return source.vertexSet().size();

    }

    public Map<String, Set<String>> getRelationTypeRules() {


        Map<String, Integer> relTypesFreq = new HashMap<String, Integer>();
        Map<String, Set<String>> relTypesRules = new HashMap<String, Set<String>>();
        Map<String, Set<String>> relTypesRules2 = new HashMap<String, Set<String>>();

        for (String rel : alllRelations.keySet()) {
            String[] splits = rel.split(":");
            String from = splits[0].trim();
            String to = splits[1].trim();
            String fromCC = getConceptClass(from);
            String toCC = getConceptClass(to);

            String ccRel = fromCC + ":" + toCC;
            String relTypes = alllRelations.get(rel);

            //other way around
            if (!relTypesRules2.containsKey(ccRel)) {
                Set<String> relTypesH = new HashSet<String>();
                if (relTypes.contains(" ")) {
                    String[] split = relTypes.split(" ");
                    for (String rela : split) {
                        relTypesH.add(rela);
                    }
                } else {
                    relTypesH.add(relTypes);
                }
                relTypesRules2.put(ccRel, relTypesH);
            }
            //

            if (relTypesRules2.containsKey(ccRel)) {
                Set<String> relTypesH = relTypesRules2.get(ccRel);
                if (relTypes.contains(" ")) {
                    String[] split = relTypes.split(" ");
                    for (String rela : split) {
                        relTypesH.add(rela);
                    }
                } else {
                    relTypesH.add(relTypes);

                }
                relTypesRules2.put(ccRel, relTypesH);
            }


//            //first two
//
//            if (relTypes.contains(" ")) {
//                String[] split = relTypes.split(" ");
//                for (String rela : split) {
//                    if (!relTypesFreq.containsKey(rela)) {
//                        relTypesFreq.put(rela, 1);
//
//                    }
//                    if (relTypesFreq.containsKey(rela)) {
//                        int count = relTypesFreq.get(rela);
//                        relTypesFreq.put(rela, count + 1);
//                    }
//                    if (!relTypesRules.containsKey(rela)) {
//                        Set<String> local = new HashSet<String>();
//                        local.add(ccRel);
//                        relTypesRules.put(rela, local);
//                    }
//
//                    if (relTypesRules.containsKey(rela)) {
//                        Set<String> local = relTypesRules.get(rela);
//                        local.add(ccRel);
//                        relTypesRules.put(rela, local);
//                    }
//                }
//
//            } else {
//
//                if (!relTypesFreq.containsKey(relTypes)) {
//                    relTypesFreq.put(relTypes, 1);
//
//
//                }
//                if (relTypesFreq.containsKey(relTypes)) {
//
//                    int count = relTypesFreq.get(relTypes);
//                    relTypesFreq.put(relTypes, count + 1);
//
//                }
//
//                if (!relTypesRules.containsKey(relTypes)) {
//                    Set<String> local = new HashSet<String>();
//                    local.add(ccRel);
//                    relTypesRules.put(relTypes, local);
//                }
//
//                if (relTypesRules.containsKey(relTypes)) {
//                    Set<String> local = relTypesRules.get(relTypes);
//                    local.add(ccRel);
//                    relTypesRules.put(relTypes, local);
//                }
//
//            }
//
        }
//        System.out.println(relTypesFreq.toString());
//        System.out.println(relTypesRules.toString());
        return relTypesRules2;

    }

    public String[][] getAverageConnectivityOfEachConceptClass() {
        //get the average percentage of each cc that makes up the graph
        HashMap<String, Integer> info = getCCFrequency();
        //get all the conceptClasses that may be present in th graph
        Set<String> concepts = info.keySet();
        //create a new 2d Array to store the info in
        String[][] connectivity = new String[concepts.size()][5];
        int conceptNumber = 0;
        //for every concept class we have in our graph

        for (String cc : concepts) {
            //HashMap to store conceptClasses of possible to nodes
            HashMap<String, Integer> ccto = new HashMap<String, Integer>();
            Set<String> symmetricalCCs = new HashSet<String>();

            int connectivities = 0;
            int number = 0;
            int symCount = 0;
            int simFreq = 0;
            //go through the semantic info of the graph
            for (int i = 0; i < conceptInfo.length; i++) {
                //if the node = the concept class of interest
                if (conceptInfo[i][2].equals(cc)) {
                    number++;
                    //get all edges
                    Set<DefaultEdge> local = source.outgoingEdgesOf(conceptInfo[i][0]);
                    int outgoing = local.size();
                    connectivities += outgoing;
                    for (DefaultEdge de : local) {

                        String ed = source.getEdgeTarget(de);

                        if (getConceptClass(ed).equals(cc)) {
                            if (source.containsEdge(ed, source.getEdgeSource(de))) {
                                symmetricalCCs.add(cc);
                                simFreq++;
                                symCount++;
                            } else {
                                symCount++;
                            }

                        }


                        if (ccto.containsKey(getConceptClass(ed))) {
                            Integer count = ccto.get(getConceptClass(ed));
                            ccto.put(getConceptClass(ed), count + 1);
                            //if the target node has the same concept class as the target node 
                            //lets see if there is a symmetrical relation


                        } else {
                            ccto.put(getConceptClass(ed), 1);
                        }

                    }
                }
            }

            String rules = "";

            //work out how many of the rels for each concept class go to which concept class


            if (ccto.size() == 0) {
                //we have no relations coming from this conceptclass,
                //no need to calculate the frequency 
            } else if (ccto.size() == 1) {
                //there is only one concept class- nothing to work out
                Set<String> cc2 = ccto.keySet();
                for (String con : cc2) {
                    //don't want rules that will not be used!
                    if (connectivities / number != 0) {
                        rules = con + "=" + Integer.toString(connectivities / number);
                    }
                }

            } else if (ccto.size() > 1) {

                //String[][] inf = new String[ccto.size()][2];

                Set<String> cc3 = ccto.keySet();
                //total number of outgoing rels
                int total = 0;
                for (String concl : cc3) {
                    total = total + ccto.get(concl);

                }

                //now we have the total lets work out the average frequency
                int numberOfrels = connectivities / number;
                int aver = total / numberOfrels;

                String[][] averages = new String[ccto.size()][3];

                int totalAllocated = 0;
                int count = 0;
                for (String concl : cc3) {
                    //total = total + ccto.get(concl);
                    averages[count][0] = concl;
                    averages[count][1] = Double.toString((double) ccto.get(concl) / aver);
                    averages[count][2] = Integer.toString(ccto.get(concl) / aver);
                    //rules = rules + concl + "=" + ccto.get(concl) / aver + ",";
                    totalAllocated += ccto.get(concl) / aver;
                    count++;

                }

                // System.out.println("BEFORE ANY ADDIION: "+Arrays.deepToString(averages));

                int leftWith = (numberOfrels - totalAllocated);

                //allocate to the conceptclasses that have 0 in first
                if (leftWith > 0) {

                    for (int p = 0; p < averages.length; p++) {
                        if (leftWith < 1) {

                            //System.out.println("ALLALLOCATED");
                            break;
                        }
                        if (Integer.parseInt(averages[p][2]) == 0) {
                            //Integer.parseInt(cc);averages[p][1]
                            averages[p][2] = "1";
                            leftWith = leftWith - 1;

                        }

                    }

                }

                // System.out.println(leftWith + "  should equal zero here");
                // System.out.println(Arrays.deepToString(averages));
                //if we still have relation to allocate them to the conceptclass with the highest average(>0.5)
                if (leftWith > 0) {
                    for (int p = 0; p < averages.length; p++) {
                        if (leftWith < 1) {
                            break;
                        }

                        if (Double.parseDouble(averages[p][1]) - Integer.parseInt(averages[p][2]) > 0.5) {
                            averages[p][2] = Integer.toString(Integer.parseInt(averages[p][2]) + 1);
                            leftWith = leftWith - 1;
                        }
                    }

                }

                // System.out.println(leftWith + "  should equal zero here------second round");
                // System.out.println(Arrays.deepToString(averages));


                for (int y = 0; y < averages.length; y++) {
                    if (Integer.parseInt(averages[y][2]) != 0) {

                        rules = rules + averages[y][0] + "=" + averages[y][2] + ",";
                    }


                }
                rules = rules.substring(0, rules.length() - 1);

            }


            connectivity[conceptNumber][0] = cc;
            //System.out.println(cc);
            connectivity[conceptNumber][1] = Integer.toString(info.get(cc));
            //  System.out.println(Integer.toString(info.get(cc)));
            connectivity[conceptNumber][2] = Integer.toString(connectivities / number);
            // System.out.println(Integer.toString(connectivities / number));
            connectivity[conceptNumber][3] = rules;
            // System.out.println(rules);

            // System.out.println("there are>>> "+ number);

            // System.out.println("------------------------------------------");
            if (symmetricalCCs.size() == 0) {
            } else {
                connectivity[conceptNumber][4] = symmetricalCCs.toString().substring(1, symmetricalCCs.toString().length() - 1) + "=" + (double) simFreq / symCount;
            }
            conceptNumber++;


        }
        // System.out.println(Arrays.deepToString(connectivity));
        return connectivity;

    }

    public String getHighestConnectivity() {
        int highest = 0;
        String cc = "";
        String nodename = "";
        for (String node : source.vertexSet()) {
            if (source.edgesOf(node).size() > highest) {
                highest = source.edgesOf(node).size();
                cc = getConceptClass(node);
                nodename = node;
            }

        }

        System.out.println("[INFO]"+source.edgesOf(nodename).toString());

        return highest + " " + cc;

    }

    public String[][] addSemanticNode(String id, String pid, String conceptClass, String name, String[][] newSemInfo) {
        String[][] newdata = new String[newSemInfo.length + 1][4];

        for (int i = 0; i < newSemInfo.length; i++) {
            for (int p = 0; p < 4; p++) {
                newdata[i][p] = newSemInfo[i][p];
            }

        }

        newdata[newSemInfo.length][0] = id;
        newdata[newSemInfo.length][1] = pid;
        newdata[newSemInfo.length][2] = conceptClass;
        newdata[newSemInfo.length][3] = name;

        return newdata;

    }

    public Map<String, String> addSemanticRel(String relFrom, String relTo, String relType, Map<String, String> allRelations) {

        String relation = relFrom + ":" + relTo;
        if (allRelations.containsKey(relation)) {
            String origRelType = allRelations.get(relation);
            String newRels = origRelType + " " + relType;
            allRelations.put(relation, newRels);
        } else {

            allRelations.put(relation, relType);

        }

        return allRelations;

    }

    public String[] getConceptName(String[] local) {
        String[] ConIDs = new String[local.length];
        for (int i = 0; i < ConIDs.length; i++) {
            ConIDs[i] = getConceptName(local[i]);
        }
        return ConIDs;
    }

    public Map<String, String> deleteSemanticRel(String relFrom, String relTo, String relType, Map<String, String> allRelations) {

        String relation = relFrom + ":" + relTo;

        if (allRelations.containsKey(relation)) {
            String relationTypes = allRelations.get(relation);
            //if there is only one reltype associated to the nodes it will ne deleted 
            if (!relationTypes.contains(" ")) {
                allRelations.remove(relation);

            } else {

                String[] split = relationTypes.split(" ");
                String newRels = "";
                for (int h = 0; h < split.length; h++) {
                    if (!split[h].equals(relType)) {
                        newRels = newRels + " " + split[h];
                    }
                }
                newRels.trim();
                allRelations.put(relation, newRels);

            }

        }

        return allRelations;

    }

    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }
}

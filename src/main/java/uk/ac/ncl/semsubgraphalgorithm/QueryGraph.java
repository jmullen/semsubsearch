package uk.ac.ncl.semsubgraphalgorithm;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import uk.ac.ncl.semsubgraphalgorithm.visualisation.GS_Core_Visualisation;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author joemullen
 */
public class QueryGraph extends GraphMethods {

    private SemanticSub semSubType;
    private Map<String, String> queryNODEInfo = new HashMap<String, String>();
    private Map<String, String> queryRELInfo = new HashMap<String, String>();
    private String MOTIFNAME = "";
    private DirectedGraph<String, DefaultEdge> Sub;
    //information about the inferred relation
    private String inferredFROM = "none";
    private String inferredTO = "none";
    private String inferredTYPE = "none";

    public enum SemanticSub {

        CTIT, CTCT, SIMCOMPSSMALL, SIMTARGSSMALL, NEWTARGIND, SIMCOMSIMTAR, NEWINDICATION, SIXSTAR, IDENTIFYMOA, FIVENODE, FIVENODELINEAR, SIXNODE, EIGHTNODE, TEST10ELEMENT, SEVENSPLIT
    }

    public QueryGraph(DirectedGraph<String, DefaultEdge> graph, String motifname, Map<String, String> queryNODEInfo, Map<String, String> queryRELInfo) {
        this.MOTIFNAME = motifname;
        this.Sub = graph;
        this.queryNODEInfo = queryNODEInfo;
        this.queryRELInfo = queryRELInfo;


    }

    public QueryGraph(DirectedGraph<String, DefaultEdge> graph, String motifname, Map<String, String> queryNODEInfo) {
        this.MOTIFNAME = motifname;
        this.Sub = graph;
        this.queryNODEInfo = queryNODEInfo;


    }

    public QueryGraph(SemanticSub sub) {
        this.semSubType = sub;
        this.Sub = new DefaultDirectedGraph<String, DefaultEdge>(
                DefaultEdge.class);

        switch (semSubType) {
            case SIMCOMPSSMALL:
                chlorpromMotif();
                break;

            case CTCT:
                CTCT();
                break;

            case CTIT:
                CTIT();
                break;

            case SIMTARGSSMALL:
                newTargetMotif();
                break;

            case NEWTARGIND:
                newTargetSMALL();
                break;

            case SIMCOMSIMTAR:
                newBothMotif();
                break;

            case NEWINDICATION:
                newIndications();
                break;

            case IDENTIFYMOA:
                identifyMoA();
                break;

            case FIVENODE:
                testFive2();
                break;

            case FIVENODELINEAR:
                testFiveLinear();
                break;
                
            case SIXNODE:
                largeMotif();
                break;

            case EIGHTNODE:
                testForSplitEight();
                break;

            case TEST10ELEMENT:
                tenElements();
                break;
               
            case SIXSTAR:
                sixSTARSplit();
                break;

            case SEVENSPLIT:
                sevenSplit();
                break;

        }

    }

    private void sevenSplit() {

        //addNodes
        addNode("comp_1", "Compound");
        addNode("comp_2", "Compound");
        addNode("tar_1", "Target");
        addNode("tar_2", "Target");
        addNode("ind_1", "Indications");
        addNode("ind_2", "Indications");
        addNode("ind_3", "Indications");


        //addRelations
        addRelation("comp_1", "ind_1", "may_treat");
        addRelation("comp_1", "comp_2", "sim");
        addRelation("comp_2", "comp_1", "sim");
        addRelation("comp_1", "tar_1", "binds_to");
        addRelation("tar_1", "tar_2", "has_similar_sequence");
        addRelation("tar_2", "tar_1", "has_similar_sequence");
        addRelation("tar_2", "ind_3", "disgenet_involved_in");
        addRelation("comp_2", "ind_2", "may_treat");


        //addInferredRelation
        //setInferredRelation("comp_1", "tar_2", "binds_to");

        //setName
        setMotifName("sevenSplitTEST");

    }
    
       private void sixSTARSplit() {

        //addNodes
        addNode("comp_0", "Compound");
        addNode("tar_1", "Target");
        addNode("pro_2", "Protein");
        addNode("pub_3", "Publication");
        addNode("pro_4", "Protein");
        addNode("pro_5", "Protein");
     

        //addRelations
        addRelation("comp_0", "tar_1", "binds_to");       
        addRelation("pro_4", "tar_1", "is_a");
        addRelation("pro_5", "tar_1", "is_a");
        addRelation("pro_2", "tar_1", "has_similar_sequence");       
        addRelation( "tar_1", "pro_4", "is_a");
        addRelation("tar_1", "pro_5",  "is_a");
        addRelation("tar_1", "pro_2", "has_similar_sequence");      
        addRelation("pro_2", "pub_3", "published_in");



        //addInferredRelation
        //setInferredRelation("comp_1", "tar_2", "binds_to");

        //setName
        setMotifName("sixSTAR");

    }

    private void tenElements() {

        //addNodes
        addNode("comp_1", "Compound");
        addNode("comp_2", "Compound");
        addNode("tar_1", "Target");
        addNode("tar_2", "Target");
        addNode("prot", "Protein");

        //addRelations
        addRelation("comp_1", "tar_2", "binds_to");
        addRelation("comp_2", "comp_1", "sim");
        addRelation("comp_2", "tar_1", "binds_to");
        addRelation("tar_1", "prot", "is_a");
        addRelation("tar_2", "prot", "is_a");

        //addInferredRelation
        setInferredRelation("comp_1", "tar_2", "binds_to");

        //setName
        setMotifName("semanticComplexity");

    }

    //produces a sub-graph for the chlorpromazine motif
    private void chlorpromMotif() {

        //addNodes
        addNode("DRUG Chlo", "Compound");
        addNode("DRUG Trim", "Compound");
        addNode("TARGET", "Target");

        //addRelations
        addRelation("DRUG Chlo", "DRUG Trim", "sim");
        addRelation("DRUG Trim", "DRUG Chlo", "sim");
        addRelation("DRUG Trim", "TARGET", "binds_to");

        //addInferredRelation
        setInferredRelation("DRUG Chlo", "TARGET", "binds_to");

        //setName
        setMotifName("CHLOR");

    }

    //produces a sub-graph for the chlorpromazine motif
    private void testFive() {

        //addNodes
        addNode("comp_1", "Compound");
        addNode("comp_2", "Compound");
        addNode("tar_1", "Target");
        addNode("tar_2", "Target");
        addNode("tar_3", "Target");

        //addRelations
        addRelation("comp_1", "comp_2", "sim");
        addRelation("comp_2", "comp_1", "sim");
        addRelation("tar_1", "tar_2", "has_similar_sequence");
        addRelation("tar_2", "tar_1", "has_similar_sequence");
        addRelation("tar_3", "tar_2", "has_similar_sequence");
        addRelation("tar_2", "tar_3", "has_similar_sequence");
        addRelation("comp_2", "tar_1", "binds_to");
        addRelation("comp_2", "tar_3", "binds_to");
        addRelation("comp_2", "tar_2", "binds_to");
        addRelation("comp_1", "tar_3", "binds_to");
        // addRelation("comp_1", "tar_1", "binds_to");
        //addInferredRelation
        setInferredRelation("comp_1", "tar_2", "binds_to");

        //setName
        setMotifName("testFive");

    }

    private void testFive2() {

        //addNodes
        addNode("comp_0", "Compound");
        addNode("comp_1", "Compound");
        addNode("comp_3", "Compound");
        addNode("ind_4", "Indications");
        addNode("ind_2", "Indications");

        //addRelations
        addRelation("comp_3", "comp_0", "sim");
        addRelation("comp_0", "comp_3", "sim");
        addRelation("comp_1", "comp_0", "sim");
        addRelation("comp_0", "comp_1", "sim");
        addRelation("comp_1", "ind_2", "may_treat");
        addRelation("comp_0", "ind_4", "may_treat");



        //setName
        setMotifName("testFive");

    }

    private void testFiveLinear() {

        //addNodes
        addNode("comp_0", "Compound");
        addNode("comp_2", "Compound");
        addNode("comp_3", "Compound");
        addNode("tar_4", "Target");
        addNode("tar_1", "Target");

        //addRelations
        addRelation("comp_3", "comp_2", "sim");
        addRelation("comp_2", "comp_3", "sim");
        addRelation("comp_2", "comp_0", "sim");
        addRelation("comp_0", "comp_2", "sim");
        addRelation("comp_3", "tar_4", "binds_to");
        addRelation("comp_0", "tar_1", "binds_to");



        //setName
        setMotifName("testFiveLinear");

    }

    private void newTargetSMALL() {

        //add nodes
        addNode("DRUG Chlo", "Compound");
        addNode("TARGET", "Target");
        addNode("INDI", "Indications");

        //add relations
        addRelation("DRUG Chlo", "INDI", "may_treat");
        addRelation("TARGET", "INDI", "disgenet_involved_in");

        //set the inferred relation information 
        setInferredRelation("DRUG Chlo", "TARGET", "binds_to");
        //setMotifName
        setMotifName("NEWTargetSMALL");

    }

    public void newBothMotif() {

        //addNodes
        addNode("DRUG_1", "Compound");
        addNode("DRUG_2", "Compound");
        addNode("TARGET_1", "Target");
        addNode("TARGET_2", "Target");

        //add relations
        addRelation("DRUG_1", "TARGET_1", "binds_to");
        addRelation("TARGET_1", "TARGET_2", "has_similar_sequence");
        addRelation("TARGET_2", "TARGET_1", "has_similar_sequence");
        addRelation("DRUG_1", "DRUG_2", "sim");
        addRelation("DRUG_2", "DRUG_1", "sim");

        //set all sub info
        setMotifName("new both");

        //set the inferred relation information
        setInferredRelation("DRUG_2", "TARGET_2", "binds_to");

    }

    private void CTCT() {

        //addNodes
        addNode("DRUG Chlo", "Compound");
        addNode("TARGET", "Target");
        addNode("DRUG Chlo2", "Compound");
        addNode("TARGET2", "Target");

        //add relations
        addRelation("DRUG Chlo", "TARGET", "binds_to");
        addRelation("DRUG Chlo", "TARGET2", "binds_to");
        addRelation("DRUG Chlo2", "TARGET", "binds_to");


        //set the inferred relation information 
        setInferredRelation("DRUG Chlo2", "TARGET2", "binds_to");
        //set all sub info
        setMotifName("CTCT");


    }

    private void CTIT() {

        //addNodes
        addNode("DRUG Chlo", "Compound");
        addNode("TARGET", "Target");
        addNode("IND", "Indications");
        addNode("TARGET2", "Target");

        //addrels
        addRelation("DRUG Chlo", "TARGET", "binds_to");
        addRelation("TARGET", "IND", "disgenet_involved_in");
        addRelation("TARGET2", "IND", "disgenet_involved_in");

        //set subname
        setMotifName("CTIT");

        //set the inferred relation information 
        setInferredRelation("DRUG Chlo", "TARGET2", "binds_to");


    }

    private void newIndications() {

        //add nodes
        addNode("DRUG Chlo", "Compound");
        addNode("DRUG Trim", "Compound");
        addNode("TARGET", "Target");
        addNode("INDI", "Indications");

        //add relations
        addRelation("DRUG Chlo", "DRUG Trim", "sim");
        addRelation("DRUG Trim", "DRUG Chlo", "sim");
        addRelation("DRUG Trim", "TARGET", "binds_to");
        addRelation("DRUG Chlo", "TARGET", "binds_to");
        addRelation("TARGET", "INDI", "is_involved_in");
        addRelation("DRUG Chlo", "INDI", "may_treat");

        //set sub name
        setMotifName("NEWINLARGE");

        //set the inferred relation information
        setInferredRelation("DRUG Trim", "INDI", "may_treat");

    }

    public void identifyMoA() {

        //add nodes
        addNode("DRUG Chlo", "Compound");
        addNode("DRUG Trim", "Compound");
        addNode("TARGET", "Target");
        addNode("INDI", "Indications");

        //add relations
        addRelation("DRUG Chlo", "DRUG Trim", "sim");
        addRelation("DRUG Trim", "DRUG Chlo", "sim");
        addRelation("DRUG Trim", "TARGET", "binds_to");
        addRelation("TARGET", "INDI", "disgenet_invloved_in");
        addRelation("DRUG Chlo", "INDI", "may_treat");
        addRelation("DRUG Trim", "INDI", "may_treat");

        //set all sub info
        setMotifName("MOA");

        //set the inferred relation information
        setInferredRelation("DRUG Chlo", "TARGET", "binds_to");

    }

    public void newTargetMotif() {

        //add nodes
        addNode("DRUG", "Compound");
        addNode("TARGET_1", "Target");
        addNode("TARGET_2", "Target");

        //add relations
        addRelation("DRUG", "TARGET_1", "binds_to");
        addRelation("TARGET_1", "TARGET_2", "has_similar_sequence");
        addRelation("TARGET_2", "TARGET_1", "has_similar_sequence");

        //set all sub info
        setMotifName("NewTARGET");

        //set the inferred relation information
        setInferredRelation("DRUG", "TARGET_2", "binds_to");

    }

    public DirectedGraph<String, DefaultEdge> fiveNodeMotif() {

        DirectedGraph<String, DefaultEdge> subgraph = new DefaultDirectedGraph<String, DefaultEdge>(
                DefaultEdge.class);
        Map<String, String> nodeInfo = new HashMap<String, String>();
        Map<String, String> relInfo = new HashMap<String, String>();

        //add nodes
        subgraph.addVertex("DRUG1");
        nodeInfo.put("DRUG1", "Compound");
        subgraph.addVertex("DRUG2");
        nodeInfo.put("DRUG2", "Compound");
        subgraph.addVertex("TARGET1");
        nodeInfo.put("TARGET1", "Target");
        subgraph.addVertex("TARGET2");
        nodeInfo.put("TARGET2", "Target");
        subgraph.addVertex("INDI");
        nodeInfo.put("INDI", "Protein");

        //add relations
        subgraph.addEdge(("DRUG1"), ("TARGET1"));
        relInfo.put("DRUG1:TARGET1", "binds_to");
        subgraph.addEdge(("TARGET1"), ("TARGET2"));
        relInfo.put("TARGET1:TARGET2", "has_similar_sequence");
        subgraph.addEdge(("TARGET2"), ("TARGET1"));
        relInfo.put("TARGET1:TARGET2", "has_similar_sequence");
        subgraph.addEdge(("DRUG1"), ("DRUG2"));
        relInfo.put("DRUG1:DRUG2", "sim");
        subgraph.addEdge(("DRUG2"), ("DRUG1"));
        relInfo.put("DRUG2:DRUG1", "sim");
        subgraph.addEdge(("TARGET2"), ("INDI"));
        relInfo.put("TARGET2:INDI", "disgenet_involved_in");

        //set all sub info
        setSub(subgraph);
        setQueryNodeInfo(nodeInfo);
        setQueryRelInfo(relInfo);
        setMotifName("five");

        //System.out.println("[INFO] Test Sub Created: " + "\n" + subgraph.toString());
        //System.out.println("[INFO] ----------------------------------------------------------");
        return subgraph;

    }

    public void largeMotif() {

        //add nodes
        addNode("DRUG", "Compound");
        addNode("TARGET", "Target");
        addNode("PROTEIN_1", "Protein");
        addNode("PROTEIN_2", "Protein");
        addNode("DISEASE_1", "Disease");
        addNode("DISEASE_2", "Disease");

        //add relations
        addRelation("DRUG", "TARGET", "binds_to");
        addRelation("TARGET", "PROTEIN_1", "has_similar_sequence");
        addRelation("PROTEIN_1", "PROTEIN_2", "has_similar_sequence");
        addRelation("PROTEIN_2", "PROTEIN_1", "has_similar_sequence");
        addRelation("PROTEIN_1", "DISEASE_1", "disgenet_involved_in");
        addRelation("PROTEIN_2", "DISEASE_2", "disgenet_involved_in");

        //set all sub info
        setMotifName("large");

        //set the inferred relation information
        setInferredRelation("TARGET", "DISEASE_2", "disgenet_involved_in");

    }

    private DirectedGraph<String, DefaultEdge> testForSplitEight() {

        DirectedGraph<String, DefaultEdge> subgraph = new DefaultDirectedGraph<String, DefaultEdge>(
                DefaultEdge.class);
        Map<String, String> nodeInfo = new HashMap<String, String>();
        Map<String, String> relInfo = new HashMap<String, String>();

        //add nodes
        subgraph.addVertex("DRUG Chlo");
        nodeInfo.put("DRUG Chlo", "Compound");
        subgraph.addVertex("DRUG Trim");
        nodeInfo.put("DRUG Trim", "Compound");
        subgraph.addVertex("TARGET");
        nodeInfo.put("TARGET", "Target");
        subgraph.addVertex("INDI");
        nodeInfo.put("INDI", "Indications");
        subgraph.addVertex("Target2");
        nodeInfo.put("Target2", "Target");
        subgraph.addVertex("Indication2");
        nodeInfo.put("Indication2", "Indications");
        subgraph.addVertex("Protein1");
        nodeInfo.put("Protein1", "Protein");
        subgraph.addVertex("Protein2");
        nodeInfo.put("Protein2", "Protein");

        //add edges
        subgraph.addEdge(("DRUG Chlo"), ("DRUG Trim"));
        relInfo.put("DRUG Chlo:DRUG Trim", "similar_to");
        subgraph.addEdge(("DRUG Trim"), ("DRUG Chlo"));
        relInfo.put("DRUG Trim:DRUG Chlo", "similar_to");
        subgraph.addEdge(("DRUG Trim"), ("TARGET"));
        relInfo.put("DRUG Trim:TARGET", "binds_to");
        subgraph.addEdge(("DRUG Chlo"), ("TARGET"));
        relInfo.put("DRUG Chlo:TARGET", "binds_to");
        subgraph.addEdge(("TARGET"), ("INDI"));
        relInfo.put("TARGET:INDI", "binds_to");
        subgraph.addEdge(("DRUG Chlo"), ("INDI"));
        relInfo.put("DRUG Chlo:INDI", "binds_to");
        subgraph.addEdge(("TARGET"), ("Target2"));
        relInfo.put("DRUG Trim:DRUG Chlo", "similar_to");
        subgraph.addEdge(("Target2"), ("TARGET"));
        relInfo.put("DRUG Trim:DRUG Chlo", "similar_to");
        subgraph.addEdge(("Target2"), ("Protein1"));
        relInfo.put("DRUG Trim:DRUG Chlo", "similar_to");
        subgraph.addEdge(("Protein1"), ("Protein2"));
        relInfo.put("DRUG Trim:DRUG Chlo", "similar_to");
        subgraph.addEdge(("Protein2"), ("Protein1"));
        relInfo.put("DRUG Trim:DRUG Chlo", "similar_to");
        subgraph.addEdge(("INDI"), ("Indication2"));
        relInfo.put("DRUG Trim:DRUG Chlo", "similar_to");

        //set all sub info
        setSub(subgraph);
        setQueryNodeInfo(nodeInfo);
        setQueryRelInfo(relInfo);
        setMotifName("NEWINLARGE");

        //set indferred relation
        //this is a large graph- will be split

        System.out.println("[INFO] New_Indication sub created:  " + "\n" + subgraph.toString());
        System.out.println("[INFO] ----------------------------------------------------------");

        return subgraph;

    }

    public void setQueryNodeInfo(Map<String, String> info) {
        this.queryNODEInfo = info;

    }

    public Map<String, String> getQueryNodeInfo() {

        return queryNODEInfo;

    }

    public void setQueryRelInfo(Map<String, String> info) {
        this.queryRELInfo = info;

    }

    public Map<String, String> getQueryRelInfo() {

        return queryRELInfo;

    }

    public String getQueryNodeCC(String node) {
        if (queryNODEInfo.containsKey(node)) {
            return queryNODEInfo.get(node);

        } else {
            return "NOT_AVAILABLE";
        }


    }

    public String getQueryRelRT(String relation) {
        if (queryRELInfo.containsKey(relation)) {
            return queryNODEInfo.get(relation);

        } else {

            return "NOT AVAILABLE";
        }


    }

    @Override
    public String toString() {

        String name = "[Name]" + getMotifName();
        String nodes = "[Nodes]" + queryNODEInfo.toString();
        String edges = "[Edges]" + queryRELInfo.toString();
        String inferredRel = "[InferredRel]" + getInferredFROM() + "/" + getInferredTO() + "/" + getInferredTYPE();

        return name + "\n" + nodes + "\n" + edges + "\n" + inferredRel;

    }

    public void addNode(String name, String conceptClass) {

        Sub.addVertex(name);
        queryNODEInfo.put(name, conceptClass);

    }

    public void addRelation(String from, String to, String relType) {
        Sub.addEdge(from, to);
        queryRELInfo.put(from + ":" + to, relType);
    }

    public String getMotifName() {

        return MOTIFNAME;
    }

    public void setMotifName(String name) {

        this.MOTIFNAME = name;
    }

    public String getInferredFROM() {

        return inferredFROM;
    }

    public void setInferredFROM(String name) {

        this.inferredFROM = name;
    }

    public String getInferredTO() {

        return inferredTO;
    }

    public void setInferredTO(String name) {

        this.inferredTO = name;
    }

    public String getInferredTYPE() {
        return inferredTYPE;
    }

    public void setInferredTYPE(String name) {

        this.inferredTYPE = name;
    }

    public DirectedGraph<String, DefaultEdge> getSub() {
        return Sub;

    }

    public void setSub(DirectedGraph<String, DefaultEdge> sub) {
        this.Sub = sub;

    }

    public void setInferredRelation(String from, String to, String relType) {
        setInferredFROM(from);
        setInferredTO(to);
        setInferredTYPE(relType);
    }

    public void draw() {

        HashMap<String, Set<String>> nodes = new HashMap<String, Set<String>>();
        HashMap<String, Set<String>> edges = new HashMap<String, Set<String>>();
        Set<String> highlight = new HashSet<String>();

        for (String name : queryNODEInfo.keySet()) {
            String type = queryNODEInfo.get(name);
            if (nodes.containsKey(type)) {
                Set<String> local = nodes.get(type);
                local.add(name);
                nodes.put(type, local);
            } else {

                Set<String> local = new HashSet<String>();
                local.add(name);
                nodes.put(type, local);
            }

        }

        for (String name : queryRELInfo.keySet()) {
            String type = queryRELInfo.get(name);
            if (edges.containsKey(type)) {
                Set<String> local = edges.get(type);
                local.add(name);
                edges.put(type, local);
            } else {

                Set<String> local = new HashSet<String>();
                local.add(name);
                edges.put(type, local);
            }

        }

        GS_Core_Visualisation gs = new GS_Core_Visualisation(nodes, edges, getMotifName(), true);
        gs.visualise();

    }
}

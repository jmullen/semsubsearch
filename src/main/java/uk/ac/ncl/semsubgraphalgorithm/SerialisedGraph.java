/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.semsubgraphalgorithm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import uk.ac.ncl.semsubgraphalgorithm.SerialisedGraph;
import uk.ac.ncl.semsubgraphalgorithm.SourceGraph;


/**
 *
 * @author joemullen
 */
public class SerialisedGraph implements java.io.Serializable {
    //private static final long serialVersionUID = 1L;

    private static final long serialVersionUID = 33326404614661473L;
    String summary = "";
    long startTime;
    long endTime;

    public SerialisedGraph() throws IOException {
        super();
    }

    public static void main(String[] args) throws IOException, Exception {
        try {
            SourceGraph s = new SourceGraph(new File("/Users/joemullen/Desktop/OP_tab_exporter/LargePLUSindsPLUSdisgenet/con_listOP_55e0c.tsv"), new File("/Users/joemullen/Desktop/OP_tab_exporter/LargePLUSindsPLUSdisgenet/rel_listOP_55e0c.tsv"));
            //SourceGraph s = new SourceGraph();
            SerialisedGraph sg = new SerialisedGraph();
            sg.SerializeGraph(s, "graph");

        } catch (FileNotFoundException fe) {
            System.out.println(fe);

        }

    }

    public void SerializeGraph(SourceGraph s, String fileName) throws FileNotFoundException, IOException {
        startTime = System.currentTimeMillis();
        try (FileOutputStream f_out = new FileOutputStream(fileName+ ".data")) {
            ObjectOutputStream obj_out = new ObjectOutputStream(f_out);
            obj_out.writeObject(s);
            obj_out.close();
        }

        endTime = System.currentTimeMillis();

        System.out.printf("[INFO] Serialized graph is saved as..."+ fileName );

    }

    public SourceGraph useSerialized(String file) throws FileNotFoundException, IOException, ClassNotFoundException {
        SourceGraph s = null;
        startTime = System.currentTimeMillis();

        try {
            FileInputStream f_in = new FileInputStream(file);
            ObjectInputStream obj_in = new ObjectInputStream(f_in);
            s = (SourceGraph) obj_in.readObject();
            f_in.close();
            obj_in.close();
            endTime = System.currentTimeMillis();
            return s;

        } catch (IOException i) {
            i.printStackTrace();

        }

        return s;

    }

    public String getSummary() {

        return "[INFO] Serialized Graph Parsed in: " + (endTime - startTime) / 1000 + " seconds";
    }
}
